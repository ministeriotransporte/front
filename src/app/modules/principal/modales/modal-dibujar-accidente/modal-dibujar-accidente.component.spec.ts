import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDibujarAccidenteComponent } from './modal-dibujar-accidente.component';

describe('ModalDibujarAccidenteComponent', () => {
  let component: ModalDibujarAccidenteComponent;
  let fixture: ComponentFixture<ModalDibujarAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDibujarAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDibujarAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
