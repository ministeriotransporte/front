import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';

declare let L;

@Component({
  selector: 'app-modal-dibujar-accidente',
  templateUrl: './modal-dibujar-accidente.component.html',
  styleUrls: ['./modal-dibujar-accidente.component.scss']
})
export class ModalDibujarAccidenteComponent implements OnInit {
  @Output() retornoValores = new EventEmitter();

  geojson: any;
  id_proyecto_detalle: number;
  map: any;
  Lgeojson: any;

  geoEditar;

  showMapa: boolean = false;

  USUARIO;
  IDUSUARIO;

  constructor(public modalRef: BsModalRef,) { }

  ngOnInit() {
    L.Icon.Default.prototype.options.imagePath = 'assets/markers/';
    setTimeout(() => {
      this.generarmapa();
    }, 500);


  }

  generarmapa() {
    this.map = L.map('map_').setView([-12.047266299999999, -77.0507234], 5);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    var options = {
      position: 'topleft', // toolbar position, options are 'topleft', 'topright', 'bottomleft', 'bottomright'
      drawMarker: true, // adds button to draw markers
      drawPolyline: true, // adds button to draw a polyline
      drawRectangle: false, // adds button to draw a rectangle
      drawPolygon: false, // adds button to draw a polygon
      drawCircle: false, // adds button to draw a cricle
      cutPolygon: true, // adds button to cut a hole in a polygon
      editMode: true, // adds button to toggle edit mode for all layers
      removalMode: true, // adds a button to remove layers
    };

    this.map.pm.addControls(options);
    this.map.pm.setPathOptions({
      color: '#FB5252',
      fillColor: 'green',
      fillOpacity: 0.9
    });

    //this.CargarShapeDB();
    this.mensaje(this.map);

    this.map.on('click', function(e) {
  });
  }

  mensaje(mapa){
    var lineas=[];
    var puntos=[];

    let MULTILINESTRING="";
    let LINESTRING="";
    let MULTIPOINT="";
    let POINT="";
    this.map.eachLayer(function(layer){
      if (layer instanceof L.Polyline) {
          let geojson = layer.toGeoJSON();
          lineas.push(geojson.geometry.coordinates);
      }
      if (layer instanceof L.Marker) {
        let geojson = layer.toGeoJSON();
        // Push GeoJSON object to collection
        puntos.push(geojson.geometry.coordinates);
      }
    });

    lineas.forEach(function(linea) {
      LINESTRING+="(";
      linea.forEach(function(element) {
          LINESTRING+=element[0]+' '+element[1]+',';

      });
      LINESTRING=LINESTRING.substring(0,LINESTRING.length - 1);
      LINESTRING+="),";
    });
    LINESTRING=LINESTRING.substring(0,LINESTRING.length - 1);
    MULTILINESTRING="@IniMULTILINESTRING("+LINESTRING+")@Fin,";

    if(lineas.length==0){
      MULTILINESTRING="";
    }

    
    puntos.forEach(function(element) {
      POINT+='('+element[0]+' '+element[1]+'),';
    });
    POINT=POINT.substring(0,POINT.length - 1);
    MULTIPOINT="@IniMULTIPOINT("+POINT+")@Fin";
    if(puntos.length==0){
      if(lineas.length>0){
        MULTILINESTRING=MULTILINESTRING.substring(0,MULTILINESTRING.length - 1);
      }
      MULTIPOINT="";
    }
  }

 

 

  guardar(){
    
  }

  closeModal() {
    this.modalRef.hide();
  }

}
