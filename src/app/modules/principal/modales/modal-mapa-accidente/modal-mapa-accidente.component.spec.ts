import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMapaAccidenteComponent } from './modal-mapa-accidente.component';

describe('ModalMapaAccidenteComponent', () => {
  let component: ModalMapaAccidenteComponent;
  let fixture: ComponentFixture<ModalMapaAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMapaAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMapaAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
