import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';

declare let L;

@Component({
  selector: 'app-modal-mapa-accidente',
  templateUrl: './modal-mapa-accidente.component.html',
  styleUrls: ['./modal-mapa-accidente.component.scss']
})
export class ModalMapaAccidenteComponent implements OnInit {

  @Output() retornoValores = new EventEmitter();

  datosAccidente;

  map: any;
  bsModalRef: BsModalRef;
  config;

  geojson: any;
  Lgeojson: any;
  expanded: boolean = false;
  opcionGeo: number = null;

  showMapa: boolean = false;

  constructor(public modalRef: BsModalRef,
    private funciones: Funciones,
    private fs: FacadeService,
    private modalService: BsModalService,
    private fb: FormBuilder) { }

  ngOnInit() {
    L.Icon.Default.prototype.options.imagePath = 'assets/markers/';
    setTimeout(() => {
      
    }, 500);

    this.generarmapa();




    



    this.geojson = {
      markers: {
        myMarker: {
          lat: 41.3825,
          lng: 2.176944,
          message: "Hello from Barcelona!!",
          focus: true,
          draggable: false
        }
     },
      'type': 'Feature',
      'geometry': {
      'type': 'Point',
      'coordinates': [this.datosAccidente.latitud,this.datosAccidente.longitud],
      },
      'properties': {
      'name': 'Accidente  de Transito'
      },
      'crs': {
      'type': 'name',
      'properties': {
        'name': 'urn:ogc:def:crs:EPSG::25830'
        }
      }
      };
  }

  generarmapa() {
    
    this.map = L.map('map_').setView([this.datosAccidente.latitud, this.datosAccidente.longitud], 15);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    L.marker([this.datosAccidente.latitud, this.datosAccidente.longitud]).addTo(this.map)
    //.bindPopup("<b>Código Accidente: </b>"+this.datosAccidente.cod_acc_transito_externo)
    //.openPopup();

    this.map.pm.setPathOptions({
      color: '#FB5252',
      fillColor: 'green',
      fillOpacity: 0.9
    });

    this.AddGeojson();

    //this.map.on('click', e => this.asignar(e));
  }

  AddGeojson(){
    this.showMapa=true;
    this.RemoveGeojson();
    this.Lgeojson=L.geoJSON(this.geojson, {
      onEachFeature: function (feature, layer) {
        layer.myTag = "myGeoJSON"
      }
    });
    this.Lgeojson.addTo(this.map);
    setTimeout(() => {
      this.map.invalidateSize();
      //this.map.fitBounds(this.Lgeojson.getBounds());
    },500);
  }

  RemoveGeojson(){
    let map=this.map
     map.eachLayer( function(layer) {
       if(layer.hasOwnProperty("myTag")){
      if ( layer.myTag &&  layer.myTag === "myGeoJSON") {
            map.removeLayer(layer)
          }
       }
    });
  }

  closeModal() {
    this.modalRef.hide();
  }

}
