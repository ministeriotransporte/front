import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-ficha-accidente',
  templateUrl: './modal-ficha-accidente.component.html',
  styleUrls: ['./modal-ficha-accidente.component.scss']
})
export class ModalFichaAccidenteComponent implements OnInit {
  @Output() retornoValores = new EventEmitter();

  accidenteModel;

  rutaLogo: string = "";

  constructor(public modalRef: BsModalRef) { }

  ngOnInit() {
    let cadena = window.location.href;
        this.rutaLogo = cadena.replace("registro", "");
        this.rutaLogo=this.rutaLogo.replace("registro/","");
       this.rutaLogo = this.rutaLogo + "assets/img/logo-mtc.png"; 
  }

  imprimir(pdf) {
    pdf.saveAs('Ficha.pdf');
    //pdf.saveAs('Ayuda_Memoria_' + this.actual + '.pdf');
  }

}
