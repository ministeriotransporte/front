import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFichaAccidenteComponent } from './modal-ficha-accidente.component';

describe('ModalFichaAccidenteComponent', () => {
  let component: ModalFichaAccidenteComponent;
  let fixture: ComponentFixture<ModalFichaAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFichaAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFichaAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
