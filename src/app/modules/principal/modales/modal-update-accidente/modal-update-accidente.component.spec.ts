import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUpdateAccidenteComponent } from './modal-update-accidente.component';

describe('ModalUpdateAccidenteComponent', () => {
  let component: ModalUpdateAccidenteComponent;
  let fixture: ComponentFixture<ModalUpdateAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalUpdateAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUpdateAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
