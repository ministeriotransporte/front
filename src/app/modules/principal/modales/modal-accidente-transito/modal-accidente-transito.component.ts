import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalDibujarAccidenteComponent } from '../modal-dibujar-accidente/modal-dibujar-accidente.component';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { AccidenteTransito } from 'src/app/modules/shared/models/accidente/accidenteTransito';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { tipoArchivo } from 'src/app/modules/shared/variables-generales/enumeraciones';
import { element } from '../../../../../../node_modules/protractor';


declare let L;

@Component({
  selector: 'app-modal-accidente-transito',
  templateUrl: './modal-accidente-transito.component.html',
  styleUrls: ['./modal-accidente-transito.component.scss']
})
export class ModalAccidenteTransitoComponent implements OnInit {
  @ViewChild('ddldepartamentos', { static: false }) selectTipoDeparatamento: NgSelectComponent;
  @ViewChild('ddlprovincias', { static: false }) selectTipoProvincia: NgSelectComponent;
  @ViewChild('ddldistritos', { static: false }) selectTipoDistrito: NgSelectComponent;
  @ViewChild('ddlTipoClase', { static: false }) selectTipoClase: NgSelectComponent;

  @Output() retornoValores = new EventEmitter();

  isMeridian = false;
  showSpinners = false;
  readonly = true;

  modelRegistroAccidente: AccidenteTransito;

  cambiarEditar: boolean = true;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  listaclaseAccidente = [];
  departamentos = [];
  provincias = [];
  distritos = [];

  usuario;
  datosAccidente;
  ShowOtroTipoClase: boolean = false;

  map: any;
  bsModalRef: BsModalRef;
  config;

  hoursPlaceholder="00"

  nombreArchivo: string = "";
  nombreArchivoOriginal : string = "";

  tipoDocumentoAccidente:number= tipoArchivo.ACCIDENTEIMAGEN;

  fechaActual : any;
  horaActual : any;

  constructor(public modalRef: BsModalRef,
    public funciones: Funciones,
    private fs: FacadeService,
    private modalService: BsModalService,
    private fb: FormBuilder) {
     }

  ngOnInit() {
    this.fechaActual=new Date();
    this.horaActual= this.fechaActual.getHours() +":"+this.fechaActual.getMinutes();
    L.Icon.Default.prototype.options.imagePath = 'assets/markers/';
    this.crearFormGroup();
    setTimeout(() => {
      this.generarmapa();
    }, 500);
    this.usuario = sessionStorage.getItem("Usuario");
    this.cargarControlesIniciales();
    this.fs.accidenteTransitoService.listarDepartamento().subscribe((data: any) => {
      this.departamentos = data;
    });
  }

  generarmapa() {
    this.map = L.map('map_').setView([-12.047266299999999, -77.0507234], 6);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    var options = {
      position: 'topleft', // toolbar position, options are 'topleft', 'topright', 'bottomleft', 'bottomright'
      drawMarker: false, // adds button to draw markers
      drawPolyline: false, // adds button to draw a polyline
      drawRectangle: false, // adds button to draw a rectangle
      drawPolygon: false, // adds button to draw a polygon
      drawCircle: false, // adds button to draw a cricle
      cutPolygon: false, // adds button to cut a hole in a polygon
      editMode: false, // adds button to toggle edit mode for all layers
      removalMode: false, // adds a button to remove layers
    };

    this.map.pm.addControls(options);
    this.map.pm.setPathOptions({
      color: '#FB5252',
      fillColor: 'green',
      fillOpacity: 0.9
    });

    //this.CargarShapeDB();

    this.map.on('click', e => this.asignar(e));
  }

  asignar(evento: any) {
    if (this.theMarker != undefined) {
      this.map.removeLayer(this.theMarker);
    };
     this.theMarker=L.marker([evento.latlng.lat, evento.latlng.lng]).addTo(this.map);
     let group = new L.featureGroup([this.theMarker]);
     this.map.fitBounds(group.getBounds());

    let param = { "latitud": evento.latlng.lat, "longitud": evento.latlng.lng }
    this.fs.accidenteTransitoService.obtenerUbigeoXPuntos(param).subscribe(
      (data:any) => {
        let param = { iddpto: data[0].iddpto };
        setTimeout(() => {
          this.fs.accidenteTransitoService.listarProvincia(param).subscribe((data: any) => {
            this.provincias = data;
          });
          let paramDistrito = { iddpto: data[0].iddpto, idprov: data[0].idprov };
          this.fs.accidenteTransitoService.listarDistrito(paramDistrito).subscribe((data: any) => {
            this.distritos = data;
          });
        }, 500);
        
        this.formGroup.patchValue({
          latitud:evento.latlng.lat,
          longitud:evento.latlng.lng,
          iddpto:data[0].iddpto,
          idprov:data[0].idprov,
          iddist:data[0].iddist
        })
        this.fs.accidenteTransitoService.listarDepartamento().subscribe((data: any) => {
          this.departamentos = data;
        });
        
      }
    );
  }

  geojson: any;
  Lgeojson: any;
  expanded: boolean = false;
  opcionGeo: number = null;

  showMapa: boolean = false;

  theMarker = {};
  Generar(){
    if((this.formGroup.get("latitud").value!="" && this.formGroup.get("latitud").value!=null)  && 
        (this.formGroup.get("longitud").value!="" && this.formGroup.get("longitud").value!=null)){
      let param = { "latitud": this.formGroup.get("latitud").value, "longitud": this.formGroup.get("longitud").value }
    this.fs.accidenteTransitoService.obtenerUbigeoXPuntos(param).subscribe(
      (data:any) => {
        if(data[0].mensaje==""){
          let param = { iddpto: data[0].iddpto };
          setTimeout(() => {
            this.fs.accidenteTransitoService.listarProvincia(param).subscribe((data: any) => {
              this.provincias = data;
            });
            let paramDistrito = { iddpto: data[0].iddpto, idprov: data[0].idprov };
            this.fs.accidenteTransitoService.listarDistrito(paramDistrito).subscribe((data: any) => {
              this.distritos = data;
            });
          }, 500);
          this.formGroup.patchValue({
            iddpto:data[0].iddpto,
            idprov:data[0].idprov,
            iddist:data[0].iddist
          })
          this.fs.accidenteTransitoService.listarDepartamento().subscribe((data: any) => {
            this.departamentos = data;
          });

          if (this.theMarker != undefined) {
            this.map.removeLayer(this.theMarker);
            };
           this.theMarker=L.marker([this.formGroup.get("latitud").value, this.formGroup.get("longitud").value]).addTo(this.map);
           let group = new L.featureGroup([this.theMarker]);
           this.map.fitBounds(group.getBounds());
        }else{
          this.funciones.mensaje("error",data[0].mensaje);
        }
        
        
      }
    );
 /*   L.marker([this.formGroup.get("latitud").value, this.formGroup.get("longitud").value]).addTo(this.map)
   
      this.showMapa=true;
      this.RemoveGeojson();
      this.Lgeojson=L.geoJSON(this.geojson, {
        onEachFeature: function (feature, layer) {
          layer.myTag = "myGeoJSON"
        }
      });
      this.Lgeojson.addTo(this.map);
      setTimeout(() => {
        this.map.invalidateSize();

        //this.map.fitBounds(this.Lgeojson.getBounds());

      },500); */

     // this.map.fitBounds(this.Lgeojson.getBounds())



     

     // this.showMapa = true;
     // this.RemoveGeojson();

     //    //this.Lgeojson.addTo(this.map);
     //     setTimeout(() => {
     //      // this.map.invalidateSize();
     //       console.log()
     //       //this.map.fitBounds(this.Lgeojson.getBounds());
   
     //     },1000);



    }else{
      this.funciones.mensaje("info","Ingrese la Latitud y longitud");
    }
    
  }



/* PRUEBA GPS*/
geoFindMe() {
  const principal = this;
  var output = document.getElementById("out");
  //var latitud1 = document.getElementById("latitud");
  // var latitud1 = document.getElementById("latitud");

  // var longitud1 = document.getElementById("longitud");

  if (!navigator.geolocation) {
    output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
    return;
  }
  function success(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    principal.asignarLatitud(latitude, longitude);
    // output.innerHTML = '<p>Latitud es' + latitude + '<br>Longitud es ' + longitude + '</p>' +
    //  '<p>Prueba ok</p>';
  };
  function error() {
    output.innerHTML = "No devuelve localizacion en el explorador de Internet"; // Unable to retrieve your location";
  };

  output.innerHTML = "<p>Locating…</p>";

  navigator.geolocation.getCurrentPosition(success, error);
}

asignarLatitud(latitud, longitud) {
  this.formGroup.patchValue({
    latitud: latitud,
    longitud: longitud
  })
}
/* PRUEBA GPS FIN*/


  RemoveGeojson(){
    let map=this.map
     map.eachLayer( function(layer) {
       if(layer.hasOwnProperty("myTag")){
      if ( layer.myTag &&  layer.myTag === "myGeoJSON") {
            map.removeLayer(layer)
          }
       }
    });
  }

  cargarControlesIniciales() {
    this.fs.accidenteTransitoService.listarAccidenteTransitoControl().subscribe(
      (data: any) => {
        if (data != null) {
          this.listaclaseAccidente = data;
        }
      });
  }

  validarOtroTipo(evento) {
    if (evento != undefined) {
      if (evento.id_clase_accidente == 12) {
        this.ShowOtroTipoClase = true;
      } else {
        this.ShowOtroTipoClase = false;
        this.formGroup.patchValue({
          otro_clase_accidente: "",
        });
      }
    }
  }
  DepartamentoSeleccionado(evento) {
    if (evento != undefined) {
      let param = { iddpto: evento.iddpto };
      this.fs.accidenteTransitoService.listarProvincia(param).subscribe((data: any) => {
        this.provincias = data;
      });
    }
    else {
      this.formGroup.patchValue({
        idprov: null,
        iddist: null
      })
      this.provincias = [];
      this.distritos = [];
    }
  }
  ProvinciaSeleccionada(evento: any) {
    if (evento != undefined) {
      let param = { iddpto: evento.iddpto, idprov: evento.idprov };
      this.fs.accidenteTransitoService.listarDistrito(param).subscribe((data: any) => {
        this.distritos = data;
      });
    }
    else {
      this.formGroup.patchValue({
        iddist: null
      })
      this.distritos = [];
    }
  }
  crearFormGroup() {
    if (this.datosAccidente == null) {
      this.formGroup = this.fb.group({
        id_accidente_transito: null,
        cod_accidente_transito: "",
        cod_acc_transito_externo: "",
        fecha_registro: null, 
        hora_registro: null,
        id_clase_accidente: null,
        otro_clase_accidente: "",
        direccion: "",
        nombre_documento:"",
        nombre_documento_original:"",
        descripcion: "",
        es_por_mapa: null,
        latitud: "",
        longitud: "",
        iddpto: null,
        idprov: null,
        iddist: null,
        activo: null,
        usuario_creacion: null,
        tipoDato: null,
        detalle_hechos:"",
      })
    } else {
      let param = { iddpto: this.datosAccidente.iddpto };
      this.fs.accidenteTransitoService.listarProvincia(param).subscribe((data: any) => {
        this.provincias = data;
      });
      let paramDistrito = { iddpto: this.datosAccidente.iddpto, idprov: this.datosAccidente.idprov };
      this.fs.accidenteTransitoService.listarDistrito(paramDistrito).subscribe((data: any) => {
        this.distritos = data;
      });

      this.formGroup = this.fb.group({
        id_accidente_transito: this.datosAccidente.id_accidente_transito == null ? 0 : this.datosAccidente.id_accidente_transito,
        cod_accidente_transito: this.datosAccidente.cod_accidente_transito == null ? "" : this.datosAccidente.cod_accidente_transito,
        cod_acc_transito_externo: this.datosAccidente.cod_acc_transito_externo == null ? "" : this.datosAccidente.cod_acc_transito_externo,
        fecha_registro: this.datosAccidente.fecha_registro == null ? "" : new Date(this.datosAccidente.fecha_registro),
        hora_registro: this.datosAccidente.hora_registro == null ? "" : this.datosAccidente.hora_registro,
        id_clase_accidente: this.datosAccidente.id_clase_accidente == null ? null : this.datosAccidente.id_clase_accidente,
        otro_clase_accidente: this.datosAccidente.otro_clase_accidente == null ? "" : this.datosAccidente.otro_clase_accidente,
        direccion: this.datosAccidente.direccion == null ? "" : this.datosAccidente.direccion,
        nombre_documento : this.datosAccidente.nombre_documento == null ? "" : this.datosAccidente.nombre_documento,
        nombre_documento_original : this.datosAccidente.nombre_documento_original == null ? "" : this.datosAccidente.nombre_documento_original,
        descripcion : this.datosAccidente.descripcion == null ? "" : this.datosAccidente.descripcion,

        es_por_mapa: false,
        latitud: this.datosAccidente.latitud == null ? "" : this.datosAccidente.latitud,
        longitud: this.datosAccidente.longitud == null ? "" : this.datosAccidente.longitud,
        iddpto: this.datosAccidente.iddpto == null ? 0 : this.datosAccidente.iddpto,
        idprov: this.datosAccidente.idprov == null ? 0 : this.datosAccidente.idprov,
        iddist: this.datosAccidente.iddist == null ? 0 : this.datosAccidente.iddist,
        detalle_hechos:this.datosAccidente.detalle_hechos == null ? "" : this.datosAccidente.detalle_hechos,
        activo: true,
        usuario_modificacion: this.usuario
      })


      if (this.datosAccidente.id_clase_accidente == 12) {
        this.ShowOtroTipoClase = true;
      } else {
        this.ShowOtroTipoClase = false;
      }
    }
  }

  fileChangeEventImagen(evento: any) {
    if (evento.uploaded != null) {
      this.nombreArchivo = JSON.parse(evento.uploaded._body).file;
      this.nombreArchivoOriginal= evento.nombreArchivo;
      this.formGroup.patchValue({
        nombre_documento: JSON.parse(evento.uploaded._body).file,
        nombre_documento_original: evento.nombreArchivo
      })

    }
  }

  generarCodigo(): string {
    let codigo: string = "";
    let dia = this.formGroup.get("fecha_registro").value.getDate();
    let mes = this.formGroup.get("fecha_registro").value.getMonth() + 1
    let anio = this.formGroup.get("fecha_registro").value.getFullYear();
    let hora = this.formGroup.get("hora_registro").value.getHours();
    let minutos = this.formGroup.get("hora_registro").value.getMinutes();
    let latitud = this.formGroup.get("latitud").value;
    let longitud = this.formGroup.get("longitud").value;
    return anio + "_" + mes + "_" + dia + "_" + hora + "_" + minutos + "_" + latitud + "_" + longitud;
  }

  registroAccidente() {
    if (this.Validar()) {
      this.asignarValoresAccidente();
      if (this.datosAccidente == null) {
        this.bMostrar = true;
        //this.modelRegistroAccidente.cod_accidente_transito = this.generarCodigo();
        let paramInsert={
          "id_accidente_transito":this.modelRegistroAccidente.id_accidente_transito,
          "cod_accidente_transito":this.modelRegistroAccidente.cod_accidente_transito,
          "cod_acc_transito_externo":this.modelRegistroAccidente.cod_acc_transito_externo,
          "fecha_registro":this.modelRegistroAccidente.fecha_registro,
          "hora_registro":this.modelRegistroAccidente.hora_registro,
          "id_clase_accidente":this.modelRegistroAccidente.id_clase_accidente,
          "otro_clase_accidente":this.modelRegistroAccidente.otro_clase_accidente,
          "direccion":this.modelRegistroAccidente.direccion,
          "es_por_mapa":this.modelRegistroAccidente.es_por_mapa,
          "latitud":this.modelRegistroAccidente.latitud,
          "longitud":this.modelRegistroAccidente.longitud,
          "iddpto":this.modelRegistroAccidente.iddpto,
          "idprov":this.modelRegistroAccidente.idprov,
          "iddist":this.modelRegistroAccidente.iddist,
          "detalle_hechos":this.modelRegistroAccidente.detalle_hechos,
          "activo":this.modelRegistroAccidente.activo,
          "usuario_creacion":this.modelRegistroAccidente.usuario_creacion,
          "archivos":[{
            "id_accidente_transito_archivo":0,
            "nombre_documento":this.modelRegistroAccidente.nombre_documento,
            "nombre_documento_original":this.modelRegistroAccidente.nombre_documento_original,
            "descripcion":this.modelRegistroAccidente.descripcion,
            "activo":true,
            "usuario_creacion":this.modelRegistroAccidente.usuario_creacion,
          }]

        }
        this.fs.accidenteTransitoService.insertarAccidenteTransito(paramInsert).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.accidenteTransitoService.modificarAccidenteTransito(this.modelRegistroAccidente).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }
  }

  asignarValoresAccidente() {
    let modelRegistroAccidenteEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroAccidente = new AccidenteTransito();
    this.modelRegistroAccidente.id_accidente_transito = modelRegistroAccidenteEnvio.id_accidente_transito == null ? 0 : modelRegistroAccidenteEnvio.id_accidente_transito;
    this.modelRegistroAccidente.cod_accidente_transito = modelRegistroAccidenteEnvio.cod_accidente_transito == null ? "" : modelRegistroAccidenteEnvio.cod_accidente_transito;
    this.modelRegistroAccidente.cod_acc_transito_externo = modelRegistroAccidenteEnvio.cod_acc_transito_externo == null ? "" : modelRegistroAccidenteEnvio.cod_acc_transito_externo;
    this.modelRegistroAccidente.fecha_registro = modelRegistroAccidenteEnvio.fecha_registro == null ? "" : new Date(modelRegistroAccidenteEnvio.fecha_registro);
    this.modelRegistroAccidente.hora_registro = modelRegistroAccidenteEnvio.hora_registro == null ? "" : modelRegistroAccidenteEnvio.hora_registro;
    this.modelRegistroAccidente.id_clase_accidente = modelRegistroAccidenteEnvio.id_clase_accidente == null ? null : modelRegistroAccidenteEnvio.id_clase_accidente;
    this.modelRegistroAccidente.otro_clase_accidente = modelRegistroAccidenteEnvio.otro_clase_accidente == null ? "" : modelRegistroAccidenteEnvio.otro_clase_accidente;
    this.modelRegistroAccidente.direccion = modelRegistroAccidenteEnvio.direccion == null ? "" : modelRegistroAccidenteEnvio.direccion;
    this.modelRegistroAccidente.nombre_documento = modelRegistroAccidenteEnvio.nombre_documento == null ? "" : modelRegistroAccidenteEnvio.nombre_documento;
    this.modelRegistroAccidente.nombre_documento_original = modelRegistroAccidenteEnvio.nombre_documento_original == null ? "" : modelRegistroAccidenteEnvio.nombre_documento_original;
    this.modelRegistroAccidente.descripcion = modelRegistroAccidenteEnvio.descripcion == null ? "" : modelRegistroAccidenteEnvio.descripcion;
    this.modelRegistroAccidente.es_por_mapa = modelRegistroAccidenteEnvio.es_por_mapa == null ? false : modelRegistroAccidenteEnvio.es_por_mapa;
    this.modelRegistroAccidente.latitud = modelRegistroAccidenteEnvio.latitud == null ? "" : modelRegistroAccidenteEnvio.latitud;
    this.modelRegistroAccidente.longitud = modelRegistroAccidenteEnvio.longitud == null ? "" : modelRegistroAccidenteEnvio.longitud;
    this.modelRegistroAccidente.iddpto = modelRegistroAccidenteEnvio.iddpto == null ? 0 : modelRegistroAccidenteEnvio.iddpto;
    this.modelRegistroAccidente.idprov = modelRegistroAccidenteEnvio.idprov == null ? 0 : modelRegistroAccidenteEnvio.idprov;
    this.modelRegistroAccidente.iddist = modelRegistroAccidenteEnvio.iddist == null ? 0 : modelRegistroAccidenteEnvio.iddist;
    this.modelRegistroAccidente.detalle_hechos = modelRegistroAccidenteEnvio.detalle_hechos == null ? "" : modelRegistroAccidenteEnvio.detalle_hechos;
    this.modelRegistroAccidente.activo = true;

    this.modelRegistroAccidente.usuario_creacion = this.usuario;
    this.modelRegistroAccidente.usuario_modificacion = this.usuario;

    //Fin
  }

  Validar(): boolean {
    let accidente = Object.assign({}, this.formGroup.value);
    /*if (accidente.cod_accidente_transito == null || accidente.cod_accidente_transito == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Código de Accidente</b>", true, () => {
        document.getElementById("cod_accidente_transito").focus();
      });
      return false;
    }*/
    if (accidente.iddpto == null || accidente.iddpto == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Departamento</b>", true, () => {
        this.selectTipoDeparatamento.focus();
      });
      return false;
    }
    if (accidente.idprov == null || accidente.idprov == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Provincia</b>", true, () => {
        this.selectTipoProvincia.focus();
      });
      return false;
    }
    if (accidente.iddist == null || accidente.iddist == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Distrito</b>", true, () => {
        this.selectTipoDistrito.focus();
      });
      return false;
    }
    if (accidente.fecha_registro == null || accidente.fecha_registro == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Fecha</b>", true, () => {
        document.getElementById("fecha_registro").focus();
      });
      return false;
    }
    if (accidente.hora_registro == null || accidente.hora_registro == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Hora</b>", true, () => {
        document.getElementById("hora_registro").focus();
      });
      return false;
    }
    // if (accidente.id_clase_accidente == null || accidente.id_clase_accidente == 0) {
    //   this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Clase del Accidente</b>", true, () => {
    //     this.selectTipoClase.focus();
    //   });
    //   return false;
    // }
    if (accidente.id_clase_accidente == 12 && (accidente.otro_clase_accidente == null || accidente.otro_clase_accidente == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>La descripción del Otro Tipo de Clase</b>", true, () => {
        document.getElementById("otro_clase_accidente").focus();
      });
      return false;
    }
    // if (accidente.detalle_hechos == null || accidente.detalle_hechos == "") {
    //   this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Detalle del Accidente</b>", true, () => {
    //     document.getElementById("detalle_hechos").focus();
    //   });
    //   return false;
    // }
    // if (accidente.nombre_documento == null || accidente.nombre_documento == "") {
    //   this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>foto del accidente</b>", true, () => {
    //     document.getElementById("fileInsertImagen").focus();
    //   });
    //   return false;
    // }
    // if (accidente.descripcion == null || accidente.descripcion == "") {
    //   this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>Descripción de la imagen</b>", true, () => {
    //     document.getElementById("descripcion").focus();
    //   });
    //   return false;
    // }
    if (accidente.latitud == null || accidente.latitud == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>Latitud</b>", true, () => {
        document.getElementById("latitud").focus();
      });
      return false;
    }
    if (accidente.longitud == null || accidente.longitud == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>Longitud</b>", true, () => {
        document.getElementById("longitud").focus();
      });
      return false;
    }
    return true;
  }

  closeModal() {
    this.modalRef.hide();
  }


  envento(item) {
  }

}
