import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAccidenteTransitoComponent } from './modal-accidente-transito.component';

describe('ModalAccidenteTransitoComponent', () => {
  let component: ModalAccidenteTransitoComponent;
  let fixture: ComponentFixture<ModalAccidenteTransitoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAccidenteTransitoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAccidenteTransitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
