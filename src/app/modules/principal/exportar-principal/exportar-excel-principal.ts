import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
//import * as fs from 'file-saver';
import { saveAs } from 'file-saver';
import { Funciones } from '../../shared/functions/funciones';
import * as logoFile from '../../../../assets/img/mtc-logo.js';
import { DatePipe } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class ExcelService {

    constructor(private datePipe: DatePipe, public funciones: Funciones) {
    }

    generateExcel(dataAccidente,datamovil,dataPerson, ptitle, arrCabeceraPrimeraFila,arrCabeceraPrimeraFilaVehiculo,arrDatosCabeceraPersona, arrFiltros) {
        //Excel Title, Header, Data
        const title = ptitle;
        const headerPrimeraFila = arrCabeceraPrimeraFila;
        //const datosCabecera = arrDatosCabecera;
        const data = dataAccidente;
        const filtros = arrFiltros;

        const dataVehiculo = datamovil;
        const headerPrimerFilaVehiculo =arrCabeceraPrimeraFilaVehiculo;

        const dataPersona = dataPerson;
        const headerPrimerFilaPersona =arrDatosCabeceraPersona;

        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet('Reporte de Accidente de Transito');


        //Create workbook and worksheet para vehiculo
        let workbookVehiculo = new Workbook();
        let worksheetVehiculo = workbook.addWorksheet('Vehículos');

        //Create workbook and worksheet para persona
        let workbookPersona = new Workbook();
        let worksheetPersona = workbook.addWorksheet('Persona');

        //Add Image
        let logo = workbook.addImage({
            base64: logoFile.logoBase64,
            extension: 'png',
        });
        worksheet.addImage(logo, 'A1:B2');
        //Add Row and formatting
        let titleRow = worksheet.addRow([title]);
        titleRow.font = { name: 'Segoe UI', family: 4, size: 16, bold: true }
        titleRow.alignment = { horizontal: 'center', vertical: 'middle' }
        worksheet.getCell("A1").value = "";
        worksheet.getCell("B1").value = title;

        worksheet.addRow([]);
        worksheet.mergeCells('B1:I2');


        worksheet.addRow([]);
        
        //Fecha y Hora de Descarga
        worksheet.getCell("H4").value = "fecha de descarga:";
        worksheet.getCell("H4").font = { name: 'Segoe UI', bold: true, size: 12 };
        worksheet.getCell("H4").alignment = { horizontal: "right" };
        worksheet.getCell("I4").value = this.funciones.ObtenerFechaDescarga();
        worksheet.getCell("I4").font = { name: 'Segoe UI', size: 9 };

        let rowFiltros = worksheet.addRow(filtros);
        worksheet.getCell("A5").alignment = { horizontal: "right" };
        worksheet.getCell("C5").alignment = { horizontal: "right" };
        worksheet.getCell("E5").alignment = { horizontal: "right" };
        worksheet.getCell("A5").font = { name: 'Segoe UI', bold: true, size: 10 };
        worksheet.getCell("C5").font = { name: 'Segoe UI', bold: true, size: 10 };
        worksheet.getCell("E5").font = { name: 'Segoe UI', bold: true, size: 10 };
        //Add Header Row
        let headerRowPrimeraFila = worksheet.addRow(headerPrimeraFila);

        headerRowPrimeraFila.eachCell((cell, number) => {
            cell.font = { name: 'Segoe UI', color: { argb: 'FFFFFFFF' }, bold: true, size: 9 }
            cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '039BE5' },
                bgColor: { argb: '039BE5' }
            }
        });
        worksheet.getColumn(1).width = 50;
        worksheet.getColumn(2).width = 20;
        worksheet.getColumn(3).width = 20;
        worksheet.getColumn(4).width = 20;
        worksheet.getColumn(5).width = 20;
        worksheet.getColumn(6).width = 20;
        worksheet.getColumn(7).width = 20;
        worksheet.getColumn(8).width = 20;
        worksheet.getColumn(9).width = 20;
        worksheet.getColumn(10).width = 20;

        // Add Data and Conditional Formatting

        //let dataDetalle = worksheet.addRow(data);
        let dataPintar = [];
        data.forEach((d, number) => {
            dataPintar = [];
            let detProducto = "";
            let detPrecios = "";
            let detFecha = "";
            


            dataPintar.push(d.cod_accidente_transito,d.cod_acc_transito_externo,d.fecha_registro, d.hora_registro, d.latitud, d.longitud, d.claseaccidente,d.direccion, d.cantidad_fallecido,d.cantidad_lesionado);
            let row: any;
            row = worksheet.addRow(dataPintar);
            //row.eachCell((cell, number) => {
            row.eachCell({ includeEmpty: true }, function(cell, number) {

                //this.alinearDatosCabecera(number, cell, worksheet);
                if(number == 5 || number == 6){
                    cell.alignment = { horizontal: 'left', vertical: 'middle', wrapText: true }
                }else{
                    cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true }
                }
                
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            });
        });
        worksheet.addRow([]);

        ///inicio para el vehiculo

        let headerRowPrimeraFilaVehiculo = worksheetVehiculo.addRow(headerPrimerFilaVehiculo);

        headerRowPrimeraFilaVehiculo.eachCell((cell, number) => {
            cell.font = { name: 'Segoe UI', color: { argb: 'FFFFFFFF' }, bold: true, size: 9 }
            cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '039BE5' },
                bgColor: { argb: '039BE5' }
            }
        });
        worksheetVehiculo.getColumn(1).width = 50;
        worksheetVehiculo.getColumn(2).width = 20;
        worksheetVehiculo.getColumn(3).width = 20;
        worksheetVehiculo.getColumn(4).width = 20;
        worksheetVehiculo.getColumn(5).width = 20;
        worksheetVehiculo.getColumn(6).width = 20;
        worksheetVehiculo.getColumn(7).width = 20;
        worksheetVehiculo.getColumn(8).width = 20;
        worksheetVehiculo.getColumn(9).width = 20;
        worksheetVehiculo.getColumn(10).width = 20;
        worksheetVehiculo.getColumn(11).width = 20;
        worksheetVehiculo.getColumn(12).width = 20;

        let dataPintarVehiculo = [];
        if(dataVehiculo!=null){
            dataVehiculo.forEach((d, number) => {
                dataPintarVehiculo = [];
                let detProducto = "";
                let detPrecios = "";
                let detFecha = "";
                
    
    
                dataPintarVehiculo.push(d.cod_accidente_transito,d.cod_acc_transito_externo,d.situacionvehiculo, d.placa_vehiculo_involucrado, d.placa_remolque, d.tipovehiculo, d.estadosoat, d.companiaseguro,d.estadocitv, d.modalidadtransporte, d.estadomodtransporte, d.tarjetacirculacion);
                let row: any;
                row = worksheetVehiculo.addRow(dataPintarVehiculo);
               // row.eachCell((cell, number) => {
                row.eachCell({ includeEmpty: true }, function(cell, number) {
    
                    //this.alinearDatosCabecera(number, cell, worksheet);
                    if(number == 5 || number == 6){
                        cell.alignment = { horizontal: 'left', vertical: 'middle', wrapText: true }
                    }else{
                        cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true }
                    }
                    
                    cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
                });
            });
        }
        
        worksheetVehiculo.addRow([]);

        ///fin para el vehiculo


         ///inicio para la persona

         let headerRowPrimeraFilaPersona = worksheetPersona.addRow(headerPrimerFilaPersona);

         headerRowPrimeraFilaPersona.eachCell((cell, number) => {
             cell.font = { name: 'Segoe UI', color: { argb: 'FFFFFFFF' }, bold: true, size: 9 }
             cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true }
             cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
             cell.fill = {
                 type: 'pattern',
                 pattern: 'solid',
                 fgColor: { argb: '039BE5' },
                 bgColor: { argb: '039BE5' }
             }
         });
         worksheetPersona.getColumn(1).width = 50;
         worksheetPersona.getColumn(2).width = 20;
         worksheetPersona.getColumn(3).width = 20;
         worksheetPersona.getColumn(4).width = 20;
         worksheetPersona.getColumn(5).width = 20;
         worksheetPersona.getColumn(6).width = 20;
         worksheetPersona.getColumn(7).width = 20;
         worksheetPersona.getColumn(8).width = 20;
         worksheetPersona.getColumn(9).width = 20;
         worksheetPersona.getColumn(10).width = 20;
 
         let dataPintarPersona = [];
         if(dataPersona!=null){
            dataPersona.forEach((d, number) => {
                dataPintarPersona = [];
                 let detProducto = "";
                 let detPrecios = "";
                 let detFecha = "";
                 
                 dataPintarPersona.push(d.cod_accidente_transito,d.cod_acc_transito_externo,d.tipopersonaaccidente, d.tipodocumento, d.dni,d.nombre,d.apellido_paterno,d.apellido_materno, d.estadoinicial,d.placa_vehiculo_involucrado);
                 let row: any;
                 row = worksheetPersona.addRow(dataPintarPersona);
                 row.eachCell({ includeEmpty: true }, function(cell, number) {
                     //this.alinearDatosCabecera(number, cell, worksheet);
                     if(number == 5 || number == 6){
                         cell.alignment = { horizontal: 'left', vertical: 'middle', wrapText: true }
                     }else{
                         cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true }
                     }
                     
                     cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
                 });
             });
         }
         
         worksheetPersona.addRow([]);
 
         ///fin para el vehiculo

        //Footer Row
        let footerRow = worksheet.addRow(['MINISTERIO DE TRANSPORTES - OFICINA GENERAL DE TECNOLOGÍA DE LA INFORMACIÓN - SISTEMA DE ACCIDENTE DE TRÁNSITO']);
        footerRow.font = { name: 'Segoe UI', family: 4, size: 8, bold: true }
        footerRow.alignment = { horizontal: "center" }
        //Merge Cells
        worksheet.mergeCells(`A${footerRow.number}:L${footerRow.number}`);

        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data:any) => {
            let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            saveAs.saveAs(blob, 'Reporte_Accidente_' + this.funciones.obtenerFechaDescargaGuardarArchivo() + '.xlsx');
        });

    }

    asignarRowsColsPan(indice, celda: any, worksheet: any, header: any) {
        let colspan = 0;
        let rowspan = 0;
        let color = "";
        header.forEach(element => {
            if (element.columna == indice) {
                colspan = element.colspan;
                rowspan = element.rowspan;
                color = (element.color != null ? element.color : "");
                //if(celda.address != this.ConvertToLetter(celda.col + colspan) + (celda.row + rowspan).toString()){
                worksheet.mergeCells(celda.address + ":" + this.ConvertToLetter(celda.col + colspan) + (celda.row + rowspan).toString());
                //}

                if (color != "") {
                    celda.fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'FF' + color }
                        ,
                        bgColor: { argb: 'FFFF9980' }
                    }
                } else {
                    celda.fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'FF009DDB' }
                        ,
                        bgColor: { argb: 'FFFF9980' }
                    }
                }
            }
        });
    }

    alinearDetalle(indice, celda: any, numColumns) {
        if (indice > 2) {
            if (indice == numColumns.length - 1) {
                celda.alignment = { horizontal: 'left', vertical: 'middle', wrapText: "pre" }
            } else if (indice == numColumns.length - 2) {
                celda.alignment = { horizontal: 'left', vertical: 'middle', wrapText: "pre" }
            } else if (indice == numColumns.length - 3) {
                celda.alignment = { horizontal: 'left', vertical: 'middle', wrapText: "pre" }
            } else if (indice == numColumns.length - 4) {
                celda.alignment = { horizontal: 'left', vertical: 'middle', wrapText: "pre" }
            } else {
                celda.alignment = { horizontal: 'center', vertical: 'middle', wrapText: "pre" }
            }
        } else {
            celda.alignment = { horizontal: 'left', vertical: 'middle', wrapText: "pre" }
        }

    }

    alinearDatosCabecera(indice, celda: any, worksheet: any) {
        let bold: boolean = false;
        if (indice % 2 > 0) {
            bold = true;
            celda.font = { name: 'Segoe UI', size: 9, bold: bold }
        } else {
            worksheet.mergeCells(celda.address + ":" + this.ConvertToLetter(celda.col + 4) + celda.row.toString());
            celda.font = { name: 'Segoe UI', size: 10, bold: bold }
        }
        celda.alignment = { horizontal: 'left', vertical: 'middle', wrapText: "pre" }

    }

    ConvertToLetter(columnNumber) {
        var dividend = columnNumber;
        var columnName = "";
        var modulo;

        while (dividend > 0) {
            modulo = (dividend - 1) % 26;
            columnName = String.fromCharCode(65 + modulo).toString() + columnName;
            dividend = parseInt(((dividend - modulo) / 26).toString());
        }
        return columnName;
    }
}