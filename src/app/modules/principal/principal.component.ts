import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FiltrosRegistroAccidente } from '../shared/models/principal/filtroRegistros';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalAccidenteTransitoComponent } from './modales/modal-accidente-transito/modal-accidente-transito.component';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from '../shared/functions/funciones';
import { FacadeService } from '../shared/patrons/facade.service';
import { ModalUpdateAccidenteComponent } from './modales/modal-update-accidente/modal-update-accidente.component';
import { ModalMapaAccidenteComponent } from './modales/modal-mapa-accidente/modal-mapa-accidente.component';
import { ExcelService } from './exportar-principal/exportar-excel-principal';
import { ModalFichaAccidenteComponent } from './modales/modal-ficha-accidente/modal-ficha-accidente.component';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  beFiltrosRegistroAccidente: FiltrosRegistroAccidente;

  totalRegistros : number=0;

  arregloAccidentes = [];
  claseAccidente = [];

  codigoAccidente:string=""
  idClaseAccidente:number=null;

  paginaActiva: number = 0;
  numPaginasMostrar: number = 10;

  config;
  bsModalRef: BsModalRef;

  usuario;
  mostrarOrdenamientoCodAccidente: boolean = false;
  mostrarOrdenamientoFecha: boolean = false;
  mostrarOrdenamientoClase: boolean = false;
  constructor(private fs:FacadeService,
              private modalService: BsModalService,
              private router: Router,
              private ruta:ParametrosGeneralesServiceService,
              private funciones : Funciones,
              private excelService : ExcelService
              ) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.beFiltrosRegistroAccidente = new FiltrosRegistroAccidente();
    this.cargarControlesIniciales();
    this.listarConsultaPrincipal(0);
  }

  cargarControlesIniciales() {
    this.fs.PrincipalService.listarCombos().subscribe(
      (data: any) => {
        if (data != null) {
          this.claseAccidente = data;
        }
      });
  }

  limpiarFiltros(){
    this.beFiltrosRegistroAccidente.cod_accidente_transito="";
    this.beFiltrosRegistroAccidente.cod_acc_transito_externo="";
    this.beFiltrosRegistroAccidente.id_clase_accidente=0;
    this.beFiltrosRegistroAccidente.fecha_inicial=null;
    this.beFiltrosRegistroAccidente.fecha_final=null;
    this.listarConsultaPrincipal(0);
  }

  listarConsultaPrincipal(skip?: number){
    this.beFiltrosRegistroAccidente.num_pagina = skip;
    let param={
      "cod_accidente_transito":"", 
      "cod_acc_transito_externo":"",
      "fecha_inicial":null,
      "fecha_final":null,
      "id_clase_accidente":0,
      "otros_clase_accidente":"",
      "skip":this.beFiltrosRegistroAccidente.num_pagina,
      "take":10,
      "indice": this.beFiltrosRegistroAccidente.indice,
      "orden": this.beFiltrosRegistroAccidente.orden
    }
    this.fs.PrincipalService.listarPrincipal(param).subscribe(
      (data:any)=>{
        if(data.listaaccidentetransito!=null){
          this.arregloAccidentes=data.listaaccidentetransito;
          this.totalRegistros=data.cantidad
        }else{
          this.arregloAccidentes=[];
          this.totalRegistros=0;
        }
      }
    )
  }

  buscarAccidente(){
    let param={
      "cod_accidente_transito":this.beFiltrosRegistroAccidente.cod_accidente_transito == null ? "" : this.beFiltrosRegistroAccidente.cod_accidente_transito,
      "cod_acc_transito_externo":this.beFiltrosRegistroAccidente.cod_acc_transito_externo == null ? "" : this.beFiltrosRegistroAccidente.cod_acc_transito_externo,
      "fecha_inicial":this.beFiltrosRegistroAccidente.fecha_inicial == null ? null : this.beFiltrosRegistroAccidente.fecha_inicial,
      "fecha_final":this.beFiltrosRegistroAccidente.fecha_final == null ? null : this.beFiltrosRegistroAccidente.fecha_final,
      "id_clase_accidente":this.beFiltrosRegistroAccidente.id_clase_accidente == null ? 0 : this.beFiltrosRegistroAccidente.id_clase_accidente,
      "otros_clase_accidente":"",
      "skip":this.beFiltrosRegistroAccidente.num_pagina,
      "take":this.beFiltrosRegistroAccidente.num_filas,
      "indice": this.beFiltrosRegistroAccidente.indice,
      "orden": this.beFiltrosRegistroAccidente.orden
    }

    this.fs.PrincipalService.listarPrincipal(param).subscribe(
      (data:any)=>{
        if(data.listaaccidentetransito!=null){
          this.arregloAccidentes=data.listaaccidentetransito;
          this.totalRegistros=data.cantidad
        }else{
          this.arregloAccidentes=[];
          this.totalRegistros=0;
        }
      }
    )
  }

  modalNuevoRegistroAccidente(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-accidente',
      initialState: {
        datosAccidente:null,
        accion: 1
      }
    };
    this.bsModalRef = this.modalService.show(ModalAccidenteTransitoComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarConsultaPrincipal();
      }
    );
  }

  editarAccidente(accidente){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-accidente',
      initialState: {
        datosAccidente:accidente,
        ACCION:2,
      }
    };
    this.bsModalRef = this.modalService.show(ModalUpdateAccidenteComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarConsultaPrincipal();
      }
    );
  }

  verMapa(accidente){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-accidente',
      initialState: {
        datosAccidente:accidente,
      }
    };
    this.bsModalRef = this.modalService.show(ModalMapaAccidenteComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarConsultaPrincipal();
      }
    );
  }

  openEditarAccidente(accidente){
    if(accidente.direccion=="" || accidente.detalle_hechos=="" ||  accidente.id_clase_accidente==null){
      this.funciones.mensaje("info","Completar los datos del accidente (clase, dirección y detalle)");
      return false;
    }else{
      this.ruta.setPaso("p_envio_codigo_accidente",accidente.cod_accidente_transito);
      this.ruta.setPaso("p_envio_id_accidente_transito",accidente.id_accidente_transito);
      this.ruta.setPaso("p_fecha_registro_accidente_transito",accidente.fecha_registro);
      this.ruta.setPaso("p_hora_registro_accidente_transito",accidente.hora_registro);
      this.ruta.setPaso("p_envio_codigo_Accidente_externo",accidente.cod_acc_transito_externo);
      this.router.navigate(['registro/registro-accidente']);
    }
    
  }

  anularAccidente(accidente){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let param={"id_accidente_transito":accidente.id_accidente_transito,"usuario_eliminacion":this.usuario}
        this.fs.accidenteTransitoService.anularAccidenteTransito(param).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarConsultaPrincipal();
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  exportarReportePrincipalGeneral() {
    let param={
      "cod_accidente_transito":this.beFiltrosRegistroAccidente.cod_accidente_transito == null ? "" : this.beFiltrosRegistroAccidente.cod_accidente_transito,
      "cod_acc_transito_externo": this.beFiltrosRegistroAccidente.cod_acc_transito_externo == null ? "" : this.beFiltrosRegistroAccidente.cod_acc_transito_externo,
      "fecha_inicial":this.beFiltrosRegistroAccidente.fecha_inicial == null ? null : this.beFiltrosRegistroAccidente.fecha_inicial,
      "fecha_final":this.beFiltrosRegistroAccidente.fecha_final == null ? null : this.beFiltrosRegistroAccidente.fecha_final,
      "id_clase_accidente":this.beFiltrosRegistroAccidente.id_clase_accidente == null ? 0 : this.beFiltrosRegistroAccidente.id_clase_accidente,
      "otros_clase_accidente":"",
    }
    this.fs.PrincipalService.listarAccidenteTransitoRepExcel(param).subscribe(
      (data:any) => {
        //if (data != "" && data != null) {
          if(data != null){
            let arrDatos;
            let arrVehiculos;
            let arrPersona;
            
            arrDatos = data.listaaccidentetransito as any;
            arrVehiculos = data.listavehiculoinvolucrado as any;
            arrPersona = data.listapersona as any;
            let arrDatosCabecera = [];
            let arrDatosCabeceraVeviculo = [];
            let arrDatosCabeceraPersona = [];
            arrDatosCabecera.push(
                'CÓDIGO INTERNO ACCIDENTE'
              , 'CÓDIGO ACCIDENTE'
              , 'FECHA DEL ACCIDENTE'
              , 'HORA DEL ACCIDENTE'
              , 'COORDENADAS NORTE LATITUD'
              , 'COORDENADAS UTM ESTE LONGITUD'
              , 'CLASE DEL ACCIDENTE'
              , 'DIRECCIÓN'
              , 'CANTIDAD DE FALLECIDOS'
              , 'CANTIDAD DE LESIONADOS'
            );

            arrDatosCabeceraVeviculo.push(
              'CÓDIGO INTERNO ACCIDENTE'
            , 'CÓDIGO ACCIDENTE'
            , 'SITUACIÓN VEHICULO'
            , 'NÚMERO DE PLACA'
            , 'NÚN. PLACA REMOLQUE'
            , 'CLASE DEL VEHICULO'
            , 'ESTADO SOAT'
            , 'COMPAÑIA SEGURO'
            , 'ESTADO CITV'
            , 'MODALIDAD TRANSPORTE'
            , 'ESTADO MODALIDAD'
            , 'TARJETA CIRCULACIÓN'
          );
          arrDatosCabeceraPersona.push(
            'CÓDIGO INTERNO ACCIDENTE'
          , 'CÓDIGO ACCIDENTE'
          , 'TIPO' 
          , 'TIPO DOCUMENTO' 
          , 'DNI'
          , 'NOMBRES'
          , 'APELLIDO PARTENO'
          , 'APELLIDO MATERNO'
          , 'ESTADO INICIAL'
          , 'NÚMERO DE PLACA'
        );
  
            let arrFiltros = [];
            //arrFiltros.push('Fecha Inicio:',this.beFiltrosRegistroAccidente.fecha_inicial,'Fecha Fin:',this.beFiltrosRegistroAccidente.fecha_final,'Código del Accidente:',this.beFiltrosRegistroAccidente.codigo_accidente);

            this.excelService.generateExcel(arrDatos,arrVehiculos,arrPersona, "Reporte de Registro de Accidente de Tránsito", arrDatosCabecera,arrDatosCabeceraVeviculo,arrDatosCabeceraPersona,arrFiltros);
          }else{
            this.funciones.mensaje("info", "No existe información a exportar.");
          }
          
        //} else {
         // this.funciones.mensaje("info", "No existe información a exportar.");
        //}
      }
    )
  }


  
  resumen:any;
  verFicha(accidente){
    let param={"id_accidente_transito":accidente.id_accidente_transito};
    this.fs.PrincipalService.listarAccidenteTransitoFicha(param).subscribe(
      (data:any)=>{
        const initialState = {
          accidenteModel: data.listaaccidentetransito[0],
        };
    
        this.config = {
          ignoreBackdropClick: true,
          keyboard: false,
          class: "modal-registrar-accidente",
          initialState: initialState
    
        };
        this.bsModalRef = this.modalService.show(ModalFichaAccidenteComponent, this.config);
        this.bsModalRef.content.retornoValores.subscribe(
          data => {
            this.listarConsultaPrincipal();
          }
        )
      }
    )
  }

  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.beFiltrosRegistroAccidente.num_filas);
    //this.numero_Pagina = this.paginaActiva;
    //this.listarConsultaPrincipal();
    this.listarConsultaPrincipal((pagina.page * this.numPaginasMostrar) - this.numPaginasMostrar);
  }
  ordenarBusqueda(indice: number, orden: number){
    if (indice== 1 && orden == 1) {//ascendente
      this.mostrarOrdenamientoCodAccidente = true; //ascendente
      this.mostrarOrdenamientoFecha = false; //descendente
      this.mostrarOrdenamientoClase = false; //descendente
    } else if(indice== 1 && orden == 0) {
      this.mostrarOrdenamientoCodAccidente = false;
      this.mostrarOrdenamientoFecha = false; //descendente
      this.mostrarOrdenamientoClase = false; //descendente
    } else if(indice== 2 && orden == 1){
      this.mostrarOrdenamientoFecha = true;
      this.mostrarOrdenamientoCodAccidente = false;
      this.mostrarOrdenamientoClase = false;
    } else if(indice== 2 && orden == 0){
      this.mostrarOrdenamientoFecha = false;
      this.mostrarOrdenamientoCodAccidente = false;
      this.mostrarOrdenamientoClase = false;
    } else if(indice== 3 && orden == 1) {
      this.mostrarOrdenamientoClase = true;
      this.mostrarOrdenamientoCodAccidente = false;
      this.mostrarOrdenamientoFecha = false;
    } else{
      this.mostrarOrdenamientoClase = false;
      this.mostrarOrdenamientoCodAccidente = false;
      this.mostrarOrdenamientoFecha = false;
    }

    this.beFiltrosRegistroAccidente.indice = indice;
    this.beFiltrosRegistroAccidente.orden = orden;
    this.listarConsultaPrincipal(this.beFiltrosRegistroAccidente.num_pagina);
  }
}
