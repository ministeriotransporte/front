import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalComponent } from './principal.component';
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PlantillaModule } from '../shared/plantilla/plantilla.module';
import { ModalAccidenteTransitoComponent } from './modales/modal-accidente-transito/modal-accidente-transito.component';
import { ModalDibujarAccidenteComponent } from './modales/modal-dibujar-accidente/modal-dibujar-accidente.component';
import { ModalUpdateAccidenteComponent } from './modales/modal-update-accidente/modal-update-accidente.component';
import { ModalMapaAccidenteComponent } from './modales/modal-mapa-accidente/modal-mapa-accidente.component';
import { ModalFichaAccidenteComponent } from './modales/modal-ficha-accidente/modal-ficha-accidente.component';


const routes:Routes=[
  {
    path:'',
    component:PrincipalComponent
  }
];
@NgModule({
  declarations: [PrincipalComponent, ModalAccidenteTransitoComponent, ModalDibujarAccidenteComponent, ModalUpdateAccidenteComponent, ModalMapaAccidenteComponent, ModalFichaAccidenteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    PlantillaModule,
    ModalModule.forRoot()
    
    
  ],
  entryComponents:[
    ModalAccidenteTransitoComponent,
    ModalDibujarAccidenteComponent,
    ModalUpdateAccidenteComponent,
    ModalMapaAccidenteComponent,
    ModalFichaAccidenteComponent
  ]
})
export class PrincipalModule { }
