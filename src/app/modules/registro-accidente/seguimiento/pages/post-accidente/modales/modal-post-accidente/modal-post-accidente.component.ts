import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { isNullOrUndefined } from 'util';
import { tipoArchivo } from 'src/app/modules/shared/variables-generales/enumeraciones';
import { detalleDocumentoSeguimiento } from 'src/app/modules/shared/models/accidente/detalleDocumentoSeguimiento';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-modal-post-accidente',
  templateUrl: './modal-post-accidente.component.html',
  styleUrls: ['./modal-post-accidente.component.scss']
})
export class ModalPostAccidenteComponent implements OnInit {


  @Output() retornoValores = new EventEmitter();

  datosSeguimientoInvolucrado
  tipoDocumentoGeneral:number= tipoArchivo.POSTSEGUIMIENTO;

  ACCION: number = null;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  usuario;

  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.crearFormGroup();
  }

  crearFormGroup(){
    if(this.datosSeguimientoInvolucrado==null){
      this.formGroup = this.fb.group({ 
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_seguimiento_post_accidente:null,
        dni_fiscal:null,
        nombre_fiscal:null,
        apellido_pat_fiscal:null,
        apellido_mat_fiscal:null,
        activo:true,
        descripcion:null,
        archivos:[]

      })
    }else{
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_seguimiento_post_accidente:this.datosSeguimientoInvolucrado.id_seguimiento_post_accidente == null ? 0 : this.datosSeguimientoInvolucrado.id_seguimiento_post_accidente,
        dni_fiscal:this.datosSeguimientoInvolucrado.dni_fiscal == null ? "" : this.datosSeguimientoInvolucrado.dni_fiscal,
        nombre_fiscal:this.datosSeguimientoInvolucrado.nombre_fiscal == null ? "" : this.datosSeguimientoInvolucrado.nombre_fiscal,
        apellido_pat_fiscal:this.datosSeguimientoInvolucrado.apellido_pat_fiscal == null ? "" : this.datosSeguimientoInvolucrado.apellido_pat_fiscal,
        apellido_mat_fiscal:this.datosSeguimientoInvolucrado.apellido_mat_fiscal == null ? "" : this.datosSeguimientoInvolucrado.apellido_mat_fiscal,
        activo:true,
        descripcion:null,
        archivos:[]
      })
      if (this.datosSeguimientoInvolucrado.archivos != null) {
        for (let i = 0; i <= this.datosSeguimientoInvolucrado.archivos.length - 1; i++) {
          this.listArchivosSeleccionados.push({ "id_seg_post_accidente_archivo": this.datosSeguimientoInvolucrado.archivos[i].id_seg_post_accidente_archivo,
                                                "id_seguimiento_post_accidente": this.datosSeguimientoInvolucrado.archivos[i].id_seguimiento_post_accidente,
                                                "nombre_documento": this.datosSeguimientoInvolucrado.archivos[i].nombre_documento,
                                                "descripcion": this.datosSeguimientoInvolucrado.archivos[i].descripcion,
                                                "activo": true,
                                                "nombre_documento_original": this.datosSeguimientoInvolucrado.archivos[i].nombre_documento_original,
                                                 "usuario_modificacion": this.usuario });
        }
      }

    }
  }

  arrArchivosEnvio = [];
  contador: number = 0;
  listArchivosSeleccionados = [];
  listArchivosSeleccionadosTemporal = [];
  listaArchivosEliminados = [];
  nombreArchivo: string = "";
  nombreArchivoOriginal : string = "";
  fileChangeEventDocumentosGenerales(evento: any) {
    if (evento.uploaded != null) {
      this.nombreArchivo = JSON.parse(evento.uploaded._body).file;
      this.nombreArchivoOriginal= evento.nombreArchivo;
      this.formGroup.patchValue({
        nombre_archivo: JSON.parse(evento.uploaded._body).file
      })
    }
  }

  AgregarArchivo(){
    if (this.listArchivosSeleccionadosTemporal != null && this.listArchivosSeleccionadosTemporal != undefined && (this.nombreArchivo!= null && this.nombreArchivo!="" ) && this.formGroup.get("descripcion").value!=null) {
          
      //if(this.listArchivosSeleccionadosTemporal.indexOf(parseInt(this.primeraAutorizacionPeriodicoForm.get("id_tipo_archivo_perfil_primera_autorizacion_periodico").value)) == -1){
        let InputSalida: HTMLInputElement = document.getElementsByName("fileInsertDocumentosGenerales")[0] as HTMLInputElement;

        this.listArchivosSeleccionados.push({"nombre_documento": this.nombreArchivo,
        "descripcion":this.formGroup.get("descripcion").value,
        "id_seg_post_accidente_archivo":0,"id_seguimiento_post_accidente":0,
        "activo":true,
        "nombre_documento_original":this.nombreArchivoOriginal, "usuario_creacion":this.usuario});
        this.listArchivosSeleccionadosTemporal.push(this.nombreArchivo);
        this.nombreArchivo="";
        this.nombreArchivoOriginal="";
        this.formGroup.patchValue({
          descripcion:null,
        })
        InputSalida.value = "";
    }else{
      this.funciones.mensaje("info","Ingrese el archivo y la descripción del documento");
    }
  }

  eliminarArchivoSeleccionadoDocumentos(aSeleccionado) {
    this.funciones.alertaRetorno("question", "<strong>¿Está seguro de eliminar el archivo?</strong>", "", true, (respuesta) => {
      if (respuesta.value) {
        if (this.listArchivosSeleccionados != null && this.listArchivosSeleccionados != undefined) {
          for (let i = 0; i < this.listArchivosSeleccionados.length; i++) {
            if (this.listArchivosSeleccionados[i].nombre_documento == aSeleccionado.nombre_documento) {
              this.listaArchivosEliminados.push(
                {
                  id_seg_post_accidente_archivo: aSeleccionado.id_seg_post_accidente_archivo,
                  id_seguimiento_post_accidente: aSeleccionado.id_seguimiento_post_accidente,
                  nombre_documento: aSeleccionado.nombre_documento,
                  activo: false,
                  descripcion : aSeleccionado.descripcion,
                  nombre_documento_original : aSeleccionado.nombre_documento_original,
                  usuario_modificacion: this.usuario
                });
    
              this.listArchivosSeleccionados.splice(i, 1);
              this.listArchivosSeleccionadosTemporal.splice(i, 1);
    
              let index = this.arrArchivosEnvio.findIndex(x => x.nombre_documento === aSeleccionado.nombre_documento);
              this.arrArchivosEnvio.splice(index, 1);
            }
          }
        }
      }
    });

    
  }

  validarDNI() {
    const valDni = this.formGroup.get('dni_fiscal').value;
    if (!isNullOrUndefined(valDni) && valDni.length == 8) {
      //this.validarInformacionReniec();
    } else {
      this.formGroup.patchValue({
        nombre_fiscal: null,
        apellido_pat_fiscal: null,
        apellido_mat_fiscal: null,

      });
      //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
      //imagen.src = '';
    }
  }

  validarInformacionReniec() {
    let valDni = this.formGroup.get('dni_fiscal').value;
    let cant = 0;
    if (valDni == '' || valDni == null) {
      document.getElementById('dni_fiscal').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de Dni a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionReniec(valDni).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.formGroup.patchValue({
                nombre_fiscal: response.strnombres,
                apellido_pat_fiscal: response.strapellidopaterno,
                apellido_mat_fiscal: response.strapellidomaterno,
              });
              //this.foto = response.strfoto;
              //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = response.strfoto;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
              this.formGroup.patchValue({
                nombre_fiscal: null,
                apellido_pat_fiscal: null,
                apellido_mat_fiscal: null,
              });
              //let imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = '';
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
            this.formGroup.patchValue({
              nombre_fiscal: null,
              apellido_pat_fiscal: null,
              apellido_mat_fiscal: null,
            });
          }
        );
      } else {
        this.funciones.mensaje('info', 'El DNI ingresado ya se encuentra registrado en el sistema.');
      }
    }
  }

  registrarPostSeguimiento(){
    if (this.Validar()) {
      //this.asignarValoreConductor();
      if (this.datosSeguimientoInvolucrado == null) {
        this.bMostrar = true;
        this.formGroup.patchValue({
          archivos:this.listArchivosSeleccionados
         })
        this.fs.postSeguimientoService.insertarSeguimientoAccidente(this.formGroup.value).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.asignarDetalle();
        this.formGroup.patchValue({
          archivos:this.modelDetalleDocumentoEnvio,
        })
        this.fs.postSeguimientoService.modificarSeguimientoAccidente(this.formGroup.value).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }
  }

  Validar(): boolean {
    let seguimiento = Object.assign({}, this.formGroup.value);
    if (seguimiento.dni_fiscal == null || seguimiento.dni_fiscal == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>DNI</b>", true, () => {
        document.getElementById("dni_fiscal").focus();
      });
      return false;
    }
    if (seguimiento.nombre_fiscal == null || seguimiento.nombre_fiscal == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Nombre del fiscal</b>", true, () => {
        document.getElementById("nombre_fiscal").focus();
      });
      return false;
    }
    if (seguimiento.apellido_pat_fiscal == null || seguimiento.apellido_pat_fiscal == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Paterno del fiscal</b>", true, () => {
        document.getElementById("apellido_pat_fiscal").focus();
      });
      return false;
    }
    if (seguimiento.apellido_mat_fiscal == null || seguimiento.apellido_mat_fiscal == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Materno del fiscal</b>", true, () => {
        document.getElementById("apellido_mat_fiscal").focus();
      });
      return false;
    }
    return true;
  }

  modelDetalleDocumentoEnvio: Array<detalleDocumentoSeguimiento>;
  asignarDetalle() {
    this.modelDetalleDocumentoEnvio = new Array<detalleDocumentoSeguimiento>();
    this.listArchivosSeleccionados.forEach(element => {
      if (element.id_seg_post_accidente_archivo == 0) {
        this.modelDetalleDocumentoEnvio.push({
          id_seg_post_accidente_archivo: 0,
          id_seguimiento_post_accidente:0,
          nombre_documento: element.nombre_documento,
          activo: true,
          descripcion:element.descripcion,
          nombre_documento_original: element.nombre_documento_original,
          usuario_modificacion: this.usuario,
        });
      } else {
        this.modelDetalleDocumentoEnvio.push({
          id_seg_post_accidente_archivo: element.id_seg_post_accidente_archivo,
          id_seguimiento_post_accidente: element.id_seguimiento_post_accidente,
          nombre_documento: element.nombre_documento,
          activo: true,
          descripcion : element.descripcion,
          nombre_documento_original: element.nombre_documento_original,
          usuario_modificacion: this.usuario,
        });
      }
    });

    this.listaArchivosEliminados.forEach(q => {
      if (q.id_seg_post_accidente_archivo > 0) {
        this.modelDetalleDocumentoEnvio.push({
          id_seg_post_accidente_archivo: q.id_seg_post_accidente_archivo,
          id_seguimiento_post_accidente: q.id_seguimiento_post_accidente,
          nombre_documento: q.nombre_documento,
          activo: q.activo,
          descripcion : q.descripcion,
          nombre_documento_original : q.nombre_documento_original,
          usuario_modificacion: this.usuario
        });
      }

    });
  }

  closeModal(){
    this.modalRef.hide();
  }

}
