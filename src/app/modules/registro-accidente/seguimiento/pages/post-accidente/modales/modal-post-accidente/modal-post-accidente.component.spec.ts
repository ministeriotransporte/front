import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPostAccidenteComponent } from './modal-post-accidente.component';

describe('ModalPostAccidenteComponent', () => {
  let component: ModalPostAccidenteComponent;
  let fixture: ComponentFixture<ModalPostAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPostAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPostAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
