import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostAccidenteComponent } from './post-accidente.component';

describe('PostAccidenteComponent', () => {
  let component: PostAccidenteComponent;
  let fixture: ComponentFixture<PostAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
