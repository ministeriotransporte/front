import { Component, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalPostAccidenteComponent } from './modales/modal-post-accidente/modal-post-accidente.component';

@Component({
  selector: 'app-post-accidente',
  templateUrl: './post-accidente.component.html',
  styleUrls: ['./post-accidente.component.scss']
})
export class PostAccidenteComponent implements OnInit {

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  modalRefArchivos: BsModalRef;

  @Output() retornoValores = new EventEmitter();

  listaSeguimiento:any[];
  totalRegistros:number=0;

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarSeguimiento(this.numPaginasMostrar, this.numero_Pagina);
  }

  listarSeguimiento(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.postSeguimientoService.listarSeguimientoAccidente(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaSeguimiento=data.listaseguimientoaccidente;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaSeguimiento=[]
        }
        
      }
    )
  }

  arregloArchivos= []
  verArchivos(template: TemplateRef<any>,item){
    this.arregloArchivos=item.archivos;
        this.config = {
          ignoreBackdropClick: true,
          keyboard: false,
          class: 'modal-registrar-vehiculo',
        };
        this.modalRefArchivos = this.modalService.show(template, this.config);
  }

  openModalPostSeguimiento(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:1,
        datosSeguimientoInvolucrado:null,
      }
    };
    this.bsModalRef = this.modalService.show(ModalPostAccidenteComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarSeguimiento(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  openEditarSeguimiento(seguimiento){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:2,
        datosSeguimientoInvolucrado:seguimiento,
      }
    };
    this.bsModalRef = this.modalService.show(ModalPostAccidenteComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarSeguimiento(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  anularAtencionPrimaria(seguimiento){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_seguimiento_post_accidente":seguimiento.id_seguimiento_post_accidente,"usuario_eliminacion":this.usuario};
        this.fs.postSeguimientoService.anularSeguimientoAccidente(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarSeguimiento(this.numPaginasMostrar, this.numero_Pagina);
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarSeguimiento(this.numPaginasMostrar, this.numero_Pagina);
  }

}
