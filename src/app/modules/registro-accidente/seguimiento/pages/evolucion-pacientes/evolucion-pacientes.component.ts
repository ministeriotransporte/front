import { Component, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalEvolucionPacientesComponent } from './modales/modal-evolucion-pacientes/modal-evolucion-pacientes.component';

@Component({
  selector: 'app-evolucion-pacientes',
  templateUrl: './evolucion-pacientes.component.html',
  styleUrls: ['./evolucion-pacientes.component.scss']
})
export class EvolucionPacientesComponent implements OnInit {

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  listaPacientesLesionados:any[];
  totalRegistros:number=0;

  @Output() retornoValores = new EventEmitter();

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
  }

  listarAtencionPrimariaDetalle(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.postSeguimientoService.listarAtencionPrimariaDetalle(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaPacientesLesionados=data.listaatencionprimaria;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaPacientesLesionados=[]
        }
        
      }
    )
  }

  openEditarEvolucion(paciente){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:2,
        datosEvolucionPaciente:paciente,
      }
    };
    this.bsModalRef = this.modalService.show(ModalEvolucionPacientesComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
  }

}
