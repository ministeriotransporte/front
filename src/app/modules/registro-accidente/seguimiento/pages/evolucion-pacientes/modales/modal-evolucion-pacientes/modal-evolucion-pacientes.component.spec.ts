import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEvolucionPacientesComponent } from './modal-evolucion-pacientes.component';

describe('ModalEvolucionPacientesComponent', () => {
  let component: ModalEvolucionPacientesComponent;
  let fixture: ComponentFixture<ModalEvolucionPacientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEvolucionPacientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEvolucionPacientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
