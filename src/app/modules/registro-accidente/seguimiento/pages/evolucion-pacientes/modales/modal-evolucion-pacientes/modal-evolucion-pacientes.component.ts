import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { NgSelectComponent } from '@ng-select/ng-select';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { EvolucionPaciente } from 'src/app/modules/shared/models/accidente/evolucionPaciente';

@Component({
  selector: 'app-modal-evolucion-pacientes',
  templateUrl: './modal-evolucion-pacientes.component.html',
  styleUrls: ['./modal-evolucion-pacientes.component.scss']
})
export class ModalEvolucionPacientesComponent implements OnInit {
  @ViewChild('ddlTipoSituacionLesionado', { static: false }) selectSituacion: NgSelectComponent;
  @ViewChild('ddlEstadoFinal', { static: false }) selectEstadoFinal: NgSelectComponent;
  

  listaSituacionLesionado:any[];
  listaEstadoFinal:any[];
  listaEstadoFinalTemporal:any[];

  modelRegistroEvolucionPaciente: EvolucionPaciente;

  @Output() retornoValores = new EventEmitter();
  datosEvolucionPaciente;

  ACCION: number = null;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  fechaRegistroAccidente:any;

  usuario;
  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {

    this.fechaRegistroAccidente=new Date(this.ruta.getRastro().accidente.p_fecha_registro_accidente_transito);
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarSeguimientoAccidenteControl();
    this.crearFormGroup();
  }

  crearFormGroup(){
    if(this.datosEvolucionPaciente==null){
      this.formGroup = this.fb.group({
        id_atencion_primaria_detalle:null,
        id_atencion_primaria:null,
        ipress:null,
        lesionado:"",
        estadoinicialatencion:"",
        id_situacion_lesionado:null,
        id_estado_final_atencion:null,
        fecha_egreso_hospital:null,
        hora_egreso_hospital: null,
        lugar_derivacion: "",
        activo:null,
      })
    }else{
      this.formGroup = this.fb.group({
        id_atencion_primaria_detalle:this.datosEvolucionPaciente.id_atencion_primaria_detalle == null ? 0 : this.datosEvolucionPaciente.id_atencion_primaria_detalle,
        id_atencion_primaria:this.datosEvolucionPaciente.id_atencion_primaria == null ? 0 : this.datosEvolucionPaciente.id_atencion_primaria,
        ipress:this.datosEvolucionPaciente.ipress == null ? 0 : this.datosEvolucionPaciente.ipress,
        lesionado:this.datosEvolucionPaciente.nombre_lesionado + " " + this.datosEvolucionPaciente.apellido_pat_lesionado + " " + this.datosEvolucionPaciente.apellido_mat_lesionado,
        estadoinicialatencion:this.datosEvolucionPaciente.estadoinicialatencion == null ? "" : this.datosEvolucionPaciente.estadoinicialatencion,
        id_situacion_lesionado: this.datosEvolucionPaciente.id_situacion_lesionado == null ? null : this.datosEvolucionPaciente.id_situacion_lesionado,
        id_estado_final_atencion: this.datosEvolucionPaciente.id_estado_final_atencion == null ? null : this.datosEvolucionPaciente.id_estado_final_atencion,
        fecha_egreso_hospital:this.datosEvolucionPaciente.fecha_egreso_hospital == null ? null : new Date(this.datosEvolucionPaciente.fecha_egreso_hospital),
        hora_egreso_hospital: this.datosEvolucionPaciente.hora_egreso_hospital == null ? null : this.datosEvolucionPaciente.hora_egreso_hospital,
        lugar_derivacion : this.datosEvolucionPaciente.lugar_derivacion == null ? "" : this.datosEvolucionPaciente.lugar_derivacion,
        
      })
      this.formGroup.controls["ipress"].disable();
      this.formGroup.controls["lesionado"].disable();
      this.formGroup.controls["estadoinicialatencion"].disable();
    }

  }

  listarSeguimientoAccidenteControl(){
    this.fs.postSeguimientoService.listarSeguimientoAccidenteControl().subscribe(
      (data:any)=>{
        this.listaSituacionLesionado=data.situacionlesionado;
        this.listaEstadoFinal=data.estadofinalatencion;
        this.listaEstadoFinalTemporal=data.estadofinalatencion;
      }
    )
  }

  Grabar(){
    if (this.Validar()) {
      this.asignarValoreSituacionVia();
      if (this.datosEvolucionPaciente == null) {
        this.bMostrar = true;
        this.fs.postSeguimientoService.insertarAtencionPrimariaDetalle(this.modelRegistroEvolucionPaciente).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.postSeguimientoService.modificarAtencionPrimariaDetalle(this.modelRegistroEvolucionPaciente).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }
  }

  ValidarEstadoFinal(evento){
    let arrayTemporal=this.listaEstadoFinalTemporal;
    this.formGroup.patchValue({
      id_estado_final_atencion:null,
    })
    if(evento!=undefined){
      if(evento.id_situacion_lesionado==3){
        this.listaEstadoFinal=arrayTemporal.filter(x=>x.id_estado_final_atencion==3);
      }else if(evento.id_situacion_lesionado==2){
        this.listaEstadoFinal=arrayTemporal.filter(x=>x.id_estado_final_atencion==1);
      }else{
        this.listaEstadoFinal=arrayTemporal;
      }
    }else{
      this.listaEstadoFinal=arrayTemporal;
    }
  }

  Validar(): boolean {
    let evolucion = Object.assign({}, this.formGroup.value);
    
    if (evolucion.id_situacion_lesionado == null || evolucion.id_situacion_lesionado == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Situación del Lesionado</b>", true, () => {
        this.selectSituacion.focus();
      });
      return false;
    }
    if (evolucion.id_estado_final_atencion == null || evolucion.id_estado_final_atencion == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado Final del Lesionado</b>", true, () => {
        this.selectEstadoFinal.focus();
      });
      return false;
    }
    if ((evolucion.fecha_egreso_hospital == null || evolucion.fecha_egreso_hospital=="")  ) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Fecha de Egreso Hospital</b>", true, () => {
        document.getElementById("fecha_egreso_hospital").focus();
      });
      return false;
    }
    if (evolucion.hora_egreso_hospital == null || evolucion.hora_egreso_hospital==""  ) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Hora de Egreso Hospital</b>", true, () => {
        document.getElementById("hora_egreso_hospital").focus();
      });
      return false;
    }
    //  if (evolucion.lugar_derivacion == null || evolucion.lugar_derivacion==""  ) {
    //   this.funciones.alertaSimple("info", "", "Por favor ingrese el <br><b>Lugar de derivación</b>", true, () => {
    //      document.getElementById("lugar_derivacion").focus();
    //    });
    //    return false;
    //  }
    return true;
  }

  asignarValoreSituacionVia(){
    let modelEvolucionPaciente = Object.assign({}, this.formGroup.value);
    this.modelRegistroEvolucionPaciente = new EvolucionPaciente();

    this.modelRegistroEvolucionPaciente.id_atencion_primaria_detalle = modelEvolucionPaciente.id_atencion_primaria_detalle == null ? 0 : modelEvolucionPaciente.id_atencion_primaria_detalle;
    this.modelRegistroEvolucionPaciente.id_atencion_primaria = modelEvolucionPaciente.id_atencion_primaria == null ? 0 : modelEvolucionPaciente.id_atencion_primaria;
    this.modelRegistroEvolucionPaciente.id_situacion_lesionado = modelEvolucionPaciente.id_situacion_lesionado == null ? 0 : modelEvolucionPaciente.id_situacion_lesionado;
    this.modelRegistroEvolucionPaciente.id_estado_final_atencion = modelEvolucionPaciente.id_estado_final_atencion == null ? 0 : modelEvolucionPaciente.id_estado_final_atencion;
    this.modelRegistroEvolucionPaciente.fecha_egreso_hospital = modelEvolucionPaciente.fecha_egreso_hospital == null ? null : modelEvolucionPaciente.fecha_egreso_hospital;
    this.modelRegistroEvolucionPaciente.hora_egreso_hospital = modelEvolucionPaciente.hora_egreso_hospital == null ? null : modelEvolucionPaciente.hora_egreso_hospital;
    this.modelRegistroEvolucionPaciente.lugar_derivacion = modelEvolucionPaciente.lugar_derivacion == null ? "" : modelEvolucionPaciente.lugar_derivacion;

    this.modelRegistroEvolucionPaciente.activo = true;
    this.modelRegistroEvolucionPaciente.usuario_creacion = this.usuario;
    this.modelRegistroEvolucionPaciente.usuario_modificacion = this.usuario;
  }

  closeModal(){
    this.modalRef.hide();
  }

}
