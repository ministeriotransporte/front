import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvolucionPacientesComponent } from './evolucion-pacientes.component';

describe('EvolucionPacientesComponent', () => {
  let component: EvolucionPacientesComponent;
  let fixture: ComponentFixture<EvolucionPacientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvolucionPacientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvolucionPacientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
