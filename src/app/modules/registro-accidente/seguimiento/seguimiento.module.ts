import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SeguimientoComponent } from './seguimiento.component';
import { SharedModule } from '../../shared/shared.module';
import { PostAccidenteComponent } from './pages/post-accidente/post-accidente.component';
import { ModalPostAccidenteComponent } from './pages/post-accidente/modales/modal-post-accidente/modal-post-accidente.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { EvolucionPacientesComponent } from './pages/evolucion-pacientes/evolucion-pacientes.component';
import { ModalEvolucionPacientesComponent } from './pages/evolucion-pacientes/modales/modal-evolucion-pacientes/modal-evolucion-pacientes.component';

const routes: Routes = [{ path: '', component: SeguimientoComponent }];

@NgModule({
  declarations: [SeguimientoComponent, PostAccidenteComponent, ModalPostAccidenteComponent, EvolucionPacientesComponent, ModalEvolucionPacientesComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalPostAccidenteComponent,
    ModalEvolucionPacientesComponent
  ]
})
export class SeguimientoModule { }
