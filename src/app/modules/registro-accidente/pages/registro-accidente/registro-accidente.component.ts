import { Component, OnInit } from '@angular/core';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';

@Component({
  selector: 'app-registro-accidente',
  templateUrl: './registro-accidente.component.html',
  styleUrls: ['./registro-accidente.component.scss']
})
export class RegistroAccidenteComponent implements OnInit {

  codigoAccidente:string="";

  constructor( private ruta:ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.codigoAccidente=this.ruta.getRastro().accidente.p_envio_codigo_Accidente_externo; 
  }

}
