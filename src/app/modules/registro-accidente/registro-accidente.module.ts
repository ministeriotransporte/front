import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegistroAccidenteComponent } from './pages/registro-accidente/registro-accidente.component';
import { SharedModule } from '../shared/shared.module';
import { PlantillaModule } from '../shared/plantilla/plantilla.module';

const routes: Routes = [
  { path: '', redirectTo: '/registro/registro-accidente/detalle-accidente', pathMatch: 'full' },
  {
  path: '', component: RegistroAccidenteComponent, children: [

    // detalle accidente
    {
      path: 'detalle-accidente',
      loadChildren: () => import("./detalle-accidente/detalle-accidente.module").then(x => x.DetalleAccidenteModule)
    },
    // SITUACION DE LA VIA
    {
      path: 'situacion-via',
      loadChildren: () => import("./situacion-via/situacion-via.module").then(x => x.SituacionViaModule)
    },
    // primera atencion
    {
      path: 'primera-atencion',
      loadChildren: () => import("./primera-atencion/primera-atencion.module").then(x => x.PrimeraAtencionModule)
    },
    // segunda atencion
    {
      path: 'segunda-atencion',
      loadChildren: () => import("./segunda-atencion/segunda-atencion.module").then(x => x.SegundaAtencionModule)
    },
    // seguimiento
    {
      path: 'seguimiento',
      loadChildren: () => import("./seguimiento/seguimiento.module").then(x => x.SeguimientoModule)
    },
    {
      path: 'furat',
      loadChildren: () => import("./furat/furat.module").then(x => x.FuratModule)
    },






  ]
}];



@NgModule({
  declarations: [RegistroAccidenteComponent],
  imports: [
    CommonModule,
    SharedModule,
    PlantillaModule,
    RouterModule.forChild(routes)
  ]
})
export class RegistroAccidenteModule { }
