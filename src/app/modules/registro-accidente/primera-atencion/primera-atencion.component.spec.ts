import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeraAtencionComponent } from './primera-atencion.component';

describe('PrimeraAtencionComponent', () => {
  let component: PrimeraAtencionComponent;
  let fixture: ComponentFixture<PrimeraAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeraAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeraAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
