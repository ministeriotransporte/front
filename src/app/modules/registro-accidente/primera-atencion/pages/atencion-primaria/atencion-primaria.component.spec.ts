import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtencionPrimariaComponent } from './atencion-primaria.component';

describe('AtencionPrimariaComponent', () => {
  let component: AtencionPrimariaComponent;
  let fixture: ComponentFixture<AtencionPrimariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtencionPrimariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtencionPrimariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
