import { Component, OnInit, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { isNullOrUndefined } from 'util';
import { PrimeraAtencion, Persona } from 'src/app/modules/shared/models/accidente/primeraAtencion';

@Component({
  selector: 'app-modal-atencion-primaria',
  templateUrl: './modal-atencion-primaria.component.html',
  styleUrls: ['./modal-atencion-primaria.component.scss']
})
export class ModalAtencionPrimariaComponent implements OnInit {
  @ViewChild('ddlTipoUnidad', { static: false }) selectTipoUnidad: NgSelectComponent; 
  @ViewChild('ddllTipoDocumento', { static: false }) selectTipoDocumento: NgSelectComponent;
  @ViewChild('ddlTipoDestino', { static: false }) selectTipoDestino: NgSelectComponent; 
  @ViewChild('ddlEstadoInicial', { static: false }) selectEstadoInicial: NgSelectComponent; 

  @Output() retornoValores = new EventEmitter();
  listaUnidadAtecion : any[];
  listaTipoDocumento : any[];
  listaEstadoInicial : any[];
  listaIpress : any[];

  modelRegistroPrimeraAtencion: PrimeraAtencion;

  datosPrimeraAtencionInvolucrado;
  usuario;
  ACCION: number = null;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  tipoDocumentoCadena:string="DNI RESPONSABLE";
  numCantidad:number=8;

  modalReflesionados: BsModalRef;

  showMostrarDerivacion: boolean=false;

  fechaRegistroAccidente:any;
  horaRegistroAtencion:"";


  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private modalService: BsModalService,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.fechaRegistroAccidente=new Date(this.ruta.getRastro().accidente.p_fecha_registro_accidente_transito);
    //this.horaRegistroAtencion= this.ruta.getRastro().accidente.p_hora_registro_accidente_transito;
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarAtencionPrimariaControl();
    this.crearFormGroup();
    this.formGroup.controls["nombreLesionado"].disable();
  }

  listarAtencionPrimariaControl(){
    this.fs.primeraAtencionService.listarAtencionPrimariaControl().subscribe(
      (data:any)=>{
          this.listaUnidadAtecion=data.unidadatencion;
          this.listaTipoDocumento=data.tipodocumento;
          this.listaEstadoInicial=data.estadoinicialatencion;
          this.listaIpress= data.ipress;

      }
    )
  }

  crearFormGroup(){
    if(this.datosPrimeraAtencionInvolucrado==null){
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_atencion_primaria:null,
        id_tipo_unidad_atencion:null,
        id_tipo_documento:1,
        dni_responsable:null,
        nombre_responsable:null,
        apellido_pat_responsable:null,
        apellido_mat_responsable:null,
        nombreLesionado:null,
        id_persona_accidente:null,
        id_ipress:null,
        fecha_registro:null,
        hora_registro:null,
        id_estado_inicial_atencion:null,
        //id_estado_final_atencion:null,
        //lugar_derivacion:null,
        telefono:null,
        persona:{
          id_tipo_persona_accidente:null,
          id_persona_accidente:null,
          id_peaton:null,
          id_pasajero_vehiculo:null,
          id_conductor_vehiculo:null,
          id_accidente_transito:null,
          id_ocupante_vehiculo:null,
          activo:true,
          usuario_creacion:null,
        }
      })
    }else{
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_atencion_primaria:this.datosPrimeraAtencionInvolucrado.id_atencion_primaria == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_atencion_primaria,
        id_tipo_unidad_atencion:this.datosPrimeraAtencionInvolucrado.id_tipo_unidad_atencion == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_tipo_unidad_atencion,
        id_tipo_documento:this.datosPrimeraAtencionInvolucrado.id_tipo_documento == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_tipo_documento,
        dni_responsable:this.datosPrimeraAtencionInvolucrado.dni_responsable == null ? "" : this.datosPrimeraAtencionInvolucrado.dni_responsable,
        nombre_responsable:this.datosPrimeraAtencionInvolucrado.nombre_responsable == null ? "" : this.datosPrimeraAtencionInvolucrado.nombre_responsable,
        apellido_pat_responsable:this.datosPrimeraAtencionInvolucrado.apellido_pat_responsable == null ? "" : this.datosPrimeraAtencionInvolucrado.apellido_pat_responsable,
        apellido_mat_responsable:this.datosPrimeraAtencionInvolucrado.apellido_mat_responsable == null ? "" : this.datosPrimeraAtencionInvolucrado.apellido_mat_responsable,
        nombreLesionado:this.datosPrimeraAtencionInvolucrado.nombre_lesionado + " " + this.datosPrimeraAtencionInvolucrado.apellido_pat_lesionado + " "+ this.datosPrimeraAtencionInvolucrado.apellido_mat_lesionado,
        id_persona_accidente:this.datosPrimeraAtencionInvolucrado.id_persona_accidente == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_persona_accidente,
        id_ipress:this.datosPrimeraAtencionInvolucrado.id_ipress == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_ipress,
        fecha_registro: this.datosPrimeraAtencionInvolucrado.fecha_registro == null ? "" : new Date(this.datosPrimeraAtencionInvolucrado.fecha_registro),
        hora_registro: this.datosPrimeraAtencionInvolucrado.hora_registro == null ? "" : this.datosPrimeraAtencionInvolucrado.hora_registro,
        id_estado_inicial_atencion:this.datosPrimeraAtencionInvolucrado.id_estado_inicial_atencion == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_estado_inicial_atencion,
        //id_estado_final_atencion:this.datosPrimeraAtencionInvolucrado.id_estado_final_atencion == null ? null : this.datosPrimeraAtencionInvolucrado.id_estado_final_atencion,
        //lugar_derivacion:this.datosPrimeraAtencionInvolucrado.lugar_derivacion == null ? "" : this.datosPrimeraAtencionInvolucrado.lugar_derivacion,
        telefono:this.datosPrimeraAtencionInvolucrado.telefono == null ? "" : this.datosPrimeraAtencionInvolucrado.telefono,
        persona:{
          id_tipo_persona_accidente:this.datosPrimeraAtencionInvolucrado.id_tipo_persona_accidente == null ? null : this.datosPrimeraAtencionInvolucrado.id_tipo_persona_accidente,
          id_persona_accidente:this.datosPrimeraAtencionInvolucrado.id_persona_accidente == null ? 0 : this.datosPrimeraAtencionInvolucrado.id_persona_accidente,
          id_peaton:this.datosPrimeraAtencionInvolucrado.id_peaton == null ? null : this.datosPrimeraAtencionInvolucrado.id_peaton,
          id_pasajero_vehiculo:this.datosPrimeraAtencionInvolucrado.id_pasajero_vehiculo == null ? null : this.datosPrimeraAtencionInvolucrado.id_pasajero_vehiculo,
          id_conductor_vehiculo:this.datosPrimeraAtencionInvolucrado.id_conductor_vehiculo == null ? null : this.datosPrimeraAtencionInvolucrado.id_conductor_vehiculo,
          id_ocupante_vehiculo:this.datosPrimeraAtencionInvolucrado.id_ocupante_vehiculo == null ? null : this.datosPrimeraAtencionInvolucrado.id_ocupante_vehiculo,
          id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
          activo:true,
        }
        
      })
      if(this.datosPrimeraAtencionInvolucrado.id_tipo_documento==2){
        this.tipoDocumentoCadena="Carnet de Extranjería Responsable";
        this.numCantidad=12;
      }else{
        this.tipoDocumentoCadena="DNI RESPONSABLE";
        this.numCantidad=8;
      }
      /*if(this.datosPrimeraAtencionInvolucrado.id_estado_final_atencion==3){
        this.showMostrarDerivacion=true;
      }else{
        this.showMostrarDerivacion=false;
      }*/
    }

    
  }

  validarHoraRegistro(evento,fechaRegistro){
    let fechaAtencion=new Date(evento);
    let fechaAccidente=new Date(this.fechaRegistroAccidente)
    if(fechaAtencion>fechaAccidente){
      this.horaRegistroAtencion= null;
    }else{
      this.horaRegistroAtencion= this.ruta.getRastro().accidente.p_hora_registro_accidente_transito;
    }
  }

  validarDocumento(evento){
    if(evento!=undefined){
      if(evento.id_tipo_documento==2){
        this.tipoDocumentoCadena="Carnet de Extranjería Responsable";
        this.numCantidad=12;
      }else{
        this.tipoDocumentoCadena="DNI RESPONSABLE";
        this.numCantidad=8;
      }
    }else{

    }
  }

  // validarLugarDerivacion(evento){
  //   if(evento!=undefined){
  //     if(evento.id_estado_final_atencion==3){
  //       this.showMostrarDerivacion=true;
  //     }else{
  //       this.showMostrarDerivacion=false;
  //       this.formGroup.patchValue({
  //         lugar_derivacion:"",
  //       });
  //     }
  //   }
  // }

  validarDNI() {
    const valDni = this.formGroup.get('dni_responsable').value;
    if (!isNullOrUndefined(valDni) && valDni.length == 8) {
      //this.validarInformacionReniec();
    } else {
      this.formGroup.patchValue({
        nombre_responsable: null,
        apellido_pat_responsable: null,
        apellido_mat_responsable: null,

      });
      //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
      //imagen.src = '';
    }
  }

  validarInformacionReniec() {
    let valDni = this.formGroup.get('dni_responsable').value;
    let cant = 0;
    if (valDni == '' || valDni == null) {
      document.getElementById('dni_responsable').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de Dni a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionReniec(valDni).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.formGroup.patchValue({
                nombre_responsable: response.strnombres,
                apellido_pat_responsable: response.strapellidopaterno,
                apellido_mat_responsable: response.strapellidomaterno,
              });
              //this.foto = response.strfoto;
              //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = response.strfoto;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
              this.formGroup.patchValue({
                nombre_responsable: null,
                apellido_pat_responsable: null,
                apellido_mat_responsable: null,
              });
              //let imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = '';
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
            this.formGroup.patchValue({
              nombre_responsable: null,
              apellido_pat_responsable: null,
              apellido_mat_responsable: null,
            });
          }
        );
      } else {
        this.funciones.mensaje('info', 'El DNI ingresado ya se encuentra registrado en el sistema.');
      }
    }
  }

  registrarPrimeraAtencion(){
    this.formGroup.controls["nombreLesionado"].enable();
    if (this.Validar()) {
      this.asignarValoresPrimeraAtencion();
      if (this.datosPrimeraAtencionInvolucrado == null) {
        this.bMostrar = true;
        this.fs.primeraAtencionService.insertarAtencionPrimaria(this.modelRegistroPrimeraAtencion).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.primeraAtencionService.modificarAtencionPrimaria(this.modelRegistroPrimeraAtencion).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }else{
      this.formGroup.controls["nombreLesionado"].disable();
    }
  }

  Validar(): boolean {
    let primeraAtencion = Object.assign({}, this.formGroup.value);
    if (primeraAtencion.id_tipo_unidad_atencion == null || primeraAtencion.id_tipo_unidad_atencion == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Unidad</b>", true, () => {
        this.selectTipoUnidad.focus();
      });
      return false;
    }
    if (primeraAtencion.id_tipo_documento == null || primeraAtencion.id_tipo_documento == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Tipo Documento</b>", true, () => {
        this.selectTipoDocumento.focus();
      });
      return false;
    }
    if (primeraAtencion.dni_responsable == null || primeraAtencion.dni_responsable == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Documento</b>", true, () => {
        document.getElementById("dni_responsable").focus();
      });
      return false;
    }
    if (primeraAtencion.nombre_responsable == null || primeraAtencion.nombre_responsable == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Nombre del responsable</b>", true, () => {
        document.getElementById("nombre_responsable").focus();
      });
      return false;
    }
    if (primeraAtencion.apellido_pat_responsable == null || primeraAtencion.apellido_pat_responsable == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Paterno del responsable</b>", true, () => {
        document.getElementById("apellido_pat_responsable").focus();
      });
      return false;
    }
    if (primeraAtencion.apellido_mat_responsable == null || primeraAtencion.apellido_mat_responsable == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Materno del responsable</b>", true, () => {
        document.getElementById("apellido_mat_responsable").focus();
      });
      return false;
    }
    // if (primeraAtencion.telefono == null || primeraAtencion.telefono == "") {
    //   this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Teléfono</b>", true, () => {
    //     document.getElementById("telefono").focus();
    //   });
    //   return false;
    // }
    if (primeraAtencion.nombreLesionado == null || primeraAtencion.nombreLesionado == "") {
      this.funciones.alertaSimple("info", "", "Por favor seleccione a la  <b>Persona Lesionada</b>", true, () => {
        document.getElementById("nombreLesionado").focus();
      });
      return false;
    }
    if (primeraAtencion.id_ipress == null || primeraAtencion.id_ipress == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Destino del Paciente</b>", true, () => {
        this.selectTipoDestino.focus();
      });
      return false;
    }
    if (primeraAtencion.fecha_registro == null || primeraAtencion.fecha_registro == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Fecha</b>", true, () => {
        document.getElementById("fecha_registro").focus();
      });
      return false;
    }
    if (primeraAtencion.hora_registro == null || primeraAtencion.hora_registro == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Hora</b>", true, () => {
        document.getElementById("hora_registro").focus();
      });
      return false;
    }
    if (primeraAtencion.id_estado_inicial_atencion == null || primeraAtencion.id_estado_inicial_atencion == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado Inicial</b>", true, () => {
        this.selectEstadoInicial.focus();
      });
      return false;
    }
    /*if (primeraAtencion.id_estado_final_atencion == null || primeraAtencion.id_estado_final_atencion == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado Final</b>", true, () => {
        this.selectEstadoFinal.focus();
      });
      return false;
    }*/
    /*if (primeraAtencion.lugar_derivacion == null || primeraAtencion.lugar_derivacion == "") {
      this.funciones.alertaSimple("info", "", "Por favor seleccione a la  <b>Lugar derivación</b>", true, () => {
        document.getElementById("lugar_derivacion").focus();
      });
      return false;
    }*/

    return true;
  }

  asignarValoresPrimeraAtencion(){
    let modelRegistroPrimeraAtencionEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroPrimeraAtencion = new PrimeraAtencion();
    this.modelRegistroPrimeraAtencion.persona = new Persona();

    this.modelRegistroPrimeraAtencion.id_accidente_transito = modelRegistroPrimeraAtencionEnvio.id_accidente_transito == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_accidente_transito;
    this.modelRegistroPrimeraAtencion.id_atencion_primaria = modelRegistroPrimeraAtencionEnvio.id_atencion_primaria == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_atencion_primaria;
    this.modelRegistroPrimeraAtencion.id_tipo_unidad_atencion = modelRegistroPrimeraAtencionEnvio.id_tipo_unidad_atencion == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_tipo_unidad_atencion;
    this.modelRegistroPrimeraAtencion.id_tipo_documento = modelRegistroPrimeraAtencionEnvio.id_tipo_documento == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_tipo_documento;
    
    
    this.modelRegistroPrimeraAtencion.dni_responsable = modelRegistroPrimeraAtencionEnvio.dni_responsable == null ? "" :modelRegistroPrimeraAtencionEnvio.dni_responsable;
    this.modelRegistroPrimeraAtencion.nombre_responsable = modelRegistroPrimeraAtencionEnvio.nombre_responsable == null ? 0 : modelRegistroPrimeraAtencionEnvio.nombre_responsable;
    this.modelRegistroPrimeraAtencion.apellido_pat_responsable = modelRegistroPrimeraAtencionEnvio.apellido_pat_responsable == null ? 0 : modelRegistroPrimeraAtencionEnvio.apellido_pat_responsable;
    this.modelRegistroPrimeraAtencion.apellido_mat_responsable = modelRegistroPrimeraAtencionEnvio.apellido_mat_responsable == null ? 0 : modelRegistroPrimeraAtencionEnvio.apellido_mat_responsable;
    this.modelRegistroPrimeraAtencion.id_persona_accidente = modelRegistroPrimeraAtencionEnvio.id_persona_accidente == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_persona_accidente;
    this.modelRegistroPrimeraAtencion.id_ipress = modelRegistroPrimeraAtencionEnvio.id_ipress == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_ipress;
    this.modelRegistroPrimeraAtencion.fecha_registro = modelRegistroPrimeraAtencionEnvio.fecha_registro == null ? null : new Date(modelRegistroPrimeraAtencionEnvio.fecha_registro);
    this.modelRegistroPrimeraAtencion.hora_registro = modelRegistroPrimeraAtencionEnvio.hora_registro == null ? null : modelRegistroPrimeraAtencionEnvio.hora_registro;
    this.modelRegistroPrimeraAtencion.id_estado_inicial_atencion = modelRegistroPrimeraAtencionEnvio.id_estado_inicial_atencion == null ? 0 : modelRegistroPrimeraAtencionEnvio.id_estado_inicial_atencion;
    //this.modelRegistroPrimeraAtencion.id_estado_final_atencion = modelRegistroPrimeraAtencionEnvio.id_estado_final_atencion == null ? null : modelRegistroPrimeraAtencionEnvio.id_estado_final_atencion;
    this.modelRegistroPrimeraAtencion.activo = true;
    //this.modelRegistroPrimeraAtencion.lugar_derivacion = modelRegistroPrimeraAtencionEnvio.lugar_derivacion == null ? "" : modelRegistroPrimeraAtencionEnvio.lugar_derivacion;
    this.modelRegistroPrimeraAtencion.telefono = modelRegistroPrimeraAtencionEnvio.telefono == null ? "" : modelRegistroPrimeraAtencionEnvio.telefono;
    this.modelRegistroPrimeraAtencion.activo = true;
    this.modelRegistroPrimeraAtencion.usuario_creacion = this.usuario;
    this.modelRegistroPrimeraAtencion.usuario_modificacion = this.usuario;

    this.modelRegistroPrimeraAtencion.persona.id_tipo_persona_accidente = modelRegistroPrimeraAtencionEnvio.persona.id_tipo_persona_accidente == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_tipo_persona_accidente
    this.modelRegistroPrimeraAtencion.persona.id_persona_accidente = modelRegistroPrimeraAtencionEnvio.persona.id_persona_accidente == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_persona_accidente;
    this.modelRegistroPrimeraAtencion.persona.id_peaton = modelRegistroPrimeraAtencionEnvio.persona.id_peaton == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_peaton;
    this.modelRegistroPrimeraAtencion.persona.id_pasajero_vehiculo = modelRegistroPrimeraAtencionEnvio.persona.id_pasajero_vehiculo == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_pasajero_vehiculo;
    this.modelRegistroPrimeraAtencion.persona.id_conductor_vehiculo = modelRegistroPrimeraAtencionEnvio.persona.id_conductor_vehiculo == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_conductor_vehiculo;
    this.modelRegistroPrimeraAtencion.persona.id_ocupante_vehiculo = modelRegistroPrimeraAtencionEnvio.persona.id_ocupante_vehiculo == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_ocupante_vehiculo;
    this.modelRegistroPrimeraAtencion.persona.id_accidente_transito = modelRegistroPrimeraAtencionEnvio.persona.id_accidente_transito == null ? null : modelRegistroPrimeraAtencionEnvio.persona.id_accidente_transito;
    this.modelRegistroPrimeraAtencion.persona.activo=true,
    this.modelRegistroPrimeraAtencion.persona.usuario_creacion=this.usuario;
    this.modelRegistroPrimeraAtencion.persona.usuario_modificacion= this.usuario;

 
  }

  arreglolesionados= []
  config;
  BuscarLesionado(template: TemplateRef<any>){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.primeraAtencionService.listarLesionado(param).subscribe(
      (data:any)=>{
        this.arreglolesionados=data.listalesionadosinvolucrados;
        this.config = {
          ignoreBackdropClick: true,
          keyboard: false,
          class: 'modal-registrar-vehiculo',
        };
        this.modalReflesionados = this.modalService.show(template, this.config);
      }
    )
  }
  persona : any;
  AsignarLesionado(lesionado){
    this.formGroup.patchValue({
      nombreLesionado:lesionado.nombre + " " + lesionado.apellido_paterno + " " + lesionado.apellido_materno,
      persona:{
        id_tipo_persona_accidente:lesionado.id_tipo_persona_accidente,
        id_persona_accidente: this.datosPrimeraAtencionInvolucrado == null ? lesionado.id_persona_accidente : this.datosPrimeraAtencionInvolucrado.id_persona_accidente,
        id_peaton:lesionado.id_peaton,
        id_pasajero_vehiculo:lesionado.id_pasajero_vehiculo,
        id_conductor_vehiculo:lesionado.id_conductor_vehiculo,
        id_accidente_transito:lesionado.id_accidente_transito,
        activo:true,
        usuario_creacion:this.usuario,
        usuario_modificacion:this.usuario

      }
    })
    this.modalReflesionados.hide();
  }

  closeModal(){
    this.modalRef.hide();
  }

}
