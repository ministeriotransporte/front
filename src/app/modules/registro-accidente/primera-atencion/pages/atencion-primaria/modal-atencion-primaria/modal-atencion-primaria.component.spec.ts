import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAtencionPrimariaComponent } from './modal-atencion-primaria.component';

describe('ModalAtencionPrimariaComponent', () => {
  let component: ModalAtencionPrimariaComponent;
  let fixture: ComponentFixture<ModalAtencionPrimariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAtencionPrimariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAtencionPrimariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
