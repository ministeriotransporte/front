import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalAtencionPrimariaComponent } from './modal-atencion-primaria/modal-atencion-primaria.component';

@Component({
  selector: 'app-atencion-primaria',
  templateUrl: './atencion-primaria.component.html',
  styleUrls: ['./atencion-primaria.component.scss']
})
export class AtencionPrimariaComponent implements OnInit {

  listaPrimeraAtencion:any[];
  totalRegistros:number=0;

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarAtencionPrimaria(this.numPaginasMostrar,this.numero_Pagina);
  }

  listarAtencionPrimaria(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.primeraAtencionService.listarAtencionPrimaria(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaPrimeraAtencion=data.listaatencionprimaria;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaPrimeraAtencion=[]
        }
        
      }
    )

  }

  openModalAtencionPrimaria(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosPrimeraAtencionInvolucrado:null,
        ACCION:1,
      }
    };
    this.bsModalRef = this.modalService.show(ModalAtencionPrimariaComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionPrimaria(this.numPaginasMostrar,this.numero_Pagina);
      }
    );
  }

  openEditarAtencionPrimaria(atencion){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosPrimeraAtencionInvolucrado: atencion,
        ACCION:2,
      }
    };
    this.bsModalRef = this.modalService.show(ModalAtencionPrimariaComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionPrimaria(this.numPaginasMostrar,this.numero_Pagina);
      }
    );
  }

  anularAtencionPrimaria(atencion){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_atencion_primaria":atencion.id_atencion_primaria,"usuario_eliminacion":this.usuario};
        this.fs.primeraAtencionService.anularAtencionPrimaria(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarAtencionPrimaria(this.numPaginasMostrar,this.numero_Pagina);
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarAtencionPrimaria(this.numPaginasMostrar,this.numero_Pagina);
  }

}
