import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PrimeraAtencionComponent } from './primera-atencion.component';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AtencionPrimariaComponent } from './pages/atencion-primaria/atencion-primaria.component';
import { ModalAtencionPrimariaComponent } from './pages/atencion-primaria/modal-atencion-primaria/modal-atencion-primaria.component';

const routes: Routes = [{ path: '', component: PrimeraAtencionComponent }];


@NgModule({
  declarations: [PrimeraAtencionComponent, AtencionPrimariaComponent, ModalAtencionPrimariaComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalAtencionPrimariaComponent
  ]
  
})
export class PrimeraAtencionModule { }
