import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FuratComponent } from './furat.component';
import { AccidentesFuratComponent } from './pages/accidentes-furat/accidentes-furat.component';
import { VehiculosFuratComponent } from './pages/vehiculos-furat/vehiculos-furat.component';
import { PersonasFuratComponent } from './pages/personas-furat/personas-furat.component';
import { ModalAccidenteFuratComponent } from './pages/accidentes-furat/modales/modal-accidente-furat/modal-accidente-furat.component';
import { ModalVehiculoFuratComponent } from './pages/vehiculos-furat/modales/modal-vehiculo-furat/modal-vehiculo-furat.component';
import { ModalPersonasFuratComponent } from './pages/personas-furat/modales/modal-personas-furat/modal-personas-furat.component';

const routes: Routes = [{ path: '', component: FuratComponent }];


@NgModule({
  declarations: [FuratComponent, AccidentesFuratComponent, VehiculosFuratComponent, PersonasFuratComponent, ModalAccidenteFuratComponent, ModalVehiculoFuratComponent, ModalPersonasFuratComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalAccidenteFuratComponent,
    ModalVehiculoFuratComponent,
    ModalPersonasFuratComponent
  ]
})
export class FuratModule { }
