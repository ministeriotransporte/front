import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalAccidenteFuratComponent } from './modales/modal-accidente-furat/modal-accidente-furat.component';

@Component({
  selector: 'app-accidentes-furat',
  templateUrl: './accidentes-furat.component.html',
  styleUrls: ['./accidentes-furat.component.scss']
})
export class AccidentesFuratComponent implements OnInit {
  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  listaAccidenteFurat:any[];
  totalRegistros:number=0;

  @Output() retornoValores = new EventEmitter();
  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
  }

  listarAtencionPrimariaDetalle(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.accidenteTransitoFuratService.obtenerAccidenteTransitoFurat(param).subscribe(
      (data:any)=>{
        if(data){
          this.listaAccidenteFurat=data.listaaccidentetransito;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaAccidenteFurat=[]
        }
        
      }
    )
  }

  openModalAccidenteFurat(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosAccidenteFurat:null,
        ACCION:1,
      }
    };
    this.bsModalRef = this.modalService.show(ModalAccidenteFuratComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  openEditarEvolucion(accidenteFurat){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:2,
        datosAccidenteFurat:accidenteFurat,
      }
    };
    this.bsModalRef = this.modalService.show(ModalAccidenteFuratComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarAtencionPrimariaDetalle(this.numPaginasMostrar, this.numero_Pagina);
  }

}
