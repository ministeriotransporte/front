import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccidentesFuratComponent } from './accidentes-furat.component';

describe('AccidentesFuratComponent', () => {
  let component: AccidentesFuratComponent;
  let fixture: ComponentFixture<AccidentesFuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccidentesFuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccidentesFuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
