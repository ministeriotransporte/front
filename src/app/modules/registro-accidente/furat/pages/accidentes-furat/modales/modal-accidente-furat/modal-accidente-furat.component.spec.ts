import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAccidenteFuratComponent } from './modal-accidente-furat.component';

describe('ModalAccidenteFuratComponent', () => {
  let component: ModalAccidenteFuratComponent;
  let fixture: ComponentFixture<ModalAccidenteFuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAccidenteFuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAccidenteFuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
