import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { NgSelectComponent } from '@ng-select/ng-select';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { allResolved } from 'q';
@Component({
  selector: 'app-modal-accidente-furat',
  templateUrl: './modal-accidente-furat.component.html',
  styleUrls: ['./modal-accidente-furat.component.scss']
})
export class ModalAccidenteFuratComponent implements OnInit {

  @ViewChild('tipoComisaria', { static: false }) selectTipoComisaria: NgSelectComponent;
  @ViewChild('tipoGrado', { static: false }) selectTipoGrado: NgSelectComponent;

  @ViewChild('tipoZonificacion', { static: false }) selectTipoZonificacion: NgSelectComponent;
  @ViewChild('tipoCaracteristica', { static: false }) selectTipoCaracteristica: NgSelectComponent;
  @ViewChild('tipoPerfilLongitudinal', { static: false }) selectTipoPerfilLongitudinal: NgSelectComponent;
  @ViewChild('TipoCodigoCarretera', { static: false }) selectTipoCodigoCarretera: NgSelectComponent;

  @ViewChild('tipoClaseAccidente', { static: false }) selectTipoClaseAccidente: NgSelectComponent;

  @ViewChild('tipoSuperficieCalzada', { static: false }) selectTipoSuperficieCalzada: NgSelectComponent;
  @ViewChild('TipoCondicionClimatologica', { static: false }) selectCondicionClimatologica: NgSelectComponent;

  @ViewChild('tipoClasificacionVertical', { static: false }) selectClasificacionVertical: NgSelectComponent;

  @ViewChild('tipoCausaPrincipal', { static: false }) selectCausaPrincipal: NgSelectComponent;
  @ViewChild('tipoCausaEspecifica', { static: false }) selectCausaEspecifica: NgSelectComponent;

  ACCION: number = null;
  datosAccidenteFurat;

  formGroup: FormGroup;

  listaCaracteristicaVia : any[];
  listaCausaPrincipal : any[];
  listaClaseAccidente : any[];
  listaCodigoCarretera : any[];
  listaComisaria : any[];
  listaCondicionClimatologica : any[];
  listaConsecuenciaAccidente : any[];
  listaGradoPolicia: any[];
  listaPerfilLongitudinal : any[];
  listaSenialVertical : any[];
  listaSuperficieCalzada : any[];
  listaTipoVia : any[];
  listaTipoZona : any[];
  listaZonificacion : any[];
  listaCausaEspecifica : any[];

  departamentos = [];
  provincias = [];
  distritos = [];

  vTipoCorresponde: boolean = false;
  vTipoSenialVertical : boolean = false;


  showOtraCaracteristica : boolean=false;
  showObservacionPerfil : boolean=false;
  showObservacionSuperficieCalzada : boolean=false;
  showCodigoCarretera : boolean=false;
  showPerfilLongitudinal : boolean=false;
  showNumCarriles : boolean=false;
  showNombreSenial : boolean=false;
  showNombreSenialHorizontal : boolean=false;
  showCausaEspecifica : boolean=false;
  showSecuencia:boolean=false;
  showOtroTipoVia:boolean=false;
  showSenialVertical:boolean=false;
  showClasificacion:boolean=false;

  usuario;

  OBJETO =
  {
      "id_accidente_transito_furat":null,
      "id_accidente_transito":null,
      "id_comisaria":null,
      "cod_registro_accidente":null,
      "dni_policia":null,
      "nombre_policia":null,
      "apellido_paterno":null,
      "apellido_materno":null,
      "id_grado_policia":null,
      "cip_policia":null,
      "telefono_policia":null,

      "fecha_registro":null,
      "hora_registro":null,

      "id_clase_accidente":null,
      "otro_clase_accidente":null,

      "cantidad_fallecidos":null,
      "cantidad_lesionados":null,
      "cantidad_vehiculo_danado":null,

      "iddpto":null,
      "idprov":null,
      "iddist":null,
      "id_tipo_zona":null,
      "id_tipo_via":null,
      "otro_tipo_via":null,
      "es_ciclovia":null,
      "direccion":null,
      "referencia":null,

      "km_k":null,
      "km_m":null,

      "id_codigo_carretera":null,

      "latitud":null,
      "longitud":null,

      "id_condicion_climatologica":null,

      "numero_vehiculos":null,
      "numero_conductores":null,
      "numero_peatones":null,
      "numero_pasajeros":null,
      "numero_ocupantes":null,

      "id_zonificacion": null,
      "id_caracteristica_via": null,
      "otro_caracteristica_via": null,
      "id_perfil_longitudinal": null,
      "observacion_perfil": null,
      "numero_carril":null,
      "numero_sendero":null,
      "id_superficie_calzada":null,
      "observacion_superficie":null,

      "tiene_senializacion":null,
      "senial_vertical":null,
      "id_clasificacion_senial_vertical":null,
      "nombre_senial_vertical":null,
      "senial_horizontal":null,
      "nombre_senial_horizontal":null,
      "velocidad_maxima":null,
      "velocidad_minima":null,

      "id_causa_principal_accidente":null,
      "id_causa_especifica_accidente":null,
      "otro_causa_accidente":null,

      "observaciones":null,
      "activo":true,
      "usuario_creacion": sessionStorage.getItem("Usuario"),
      "consecuencias":[]



  };

  listConsecuenciasSeleccionados = [];


  @Output() retornoValores = new EventEmitter();
  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.OBJETO.id_accidente_transito=this.datosAccidenteFurat.id_accidente_transito
    this.listarConductorVehiculoControl();
    //this.crearFormGroup();
    this.fs.accidenteTransitoFuratService.listarDepartamento().subscribe((data: any) => {
      this.departamentos = data;
    });

    if(this.ACCION==2){
      this.Setear();
    }
  }

  setearConsecuencias(){
    if(this.datosAccidenteFurat.consecuencias!=null){
      for (let i = 0; i <= this.datosAccidenteFurat.consecuencias.length - 1; i++) {
        this.listConsecuenciasSeleccionados.push({
          "id_consecuencia_accidente_det":this.datosAccidenteFurat.consecuencias[i].id_detalle,
          "id_consecuencia_accidente":this.datosAccidenteFurat.consecuencias[i].id_consecuencia_accidente,
          "id_accidente_transito_furat":this.datosAccidenteFurat.consecuencias[i].id_accidente_transito_furat,
          "activo":true,
          "usuario_creacion":sessionStorage.getItem("Usuario"),
          "usuario_modificacion":sessionStorage.getItem("Usuario"),
          });
      }
    }
  }

  Setear() {
    let param = { iddpto: this.datosAccidenteFurat.iddpto };
        this.fs.accidenteTransitoFuratService.listarProvincia(param).subscribe((data: any) => {
          this.provincias = data;
        });

        let paramDistrito = { iddpto: this.datosAccidenteFurat.iddpto, idprov: this.datosAccidenteFurat.idprov };
        this.fs.accidenteTransitoFuratService.listarDistrito(paramDistrito).subscribe((data: any) => {
          this.distritos = data;
        });
    this.OBJETO.id_accidente_transito_furat = this.datosAccidenteFurat.id_accidente_transito_furat == null ? 0 : this.datosAccidenteFurat.id_accidente_transito_furat;
    this.OBJETO.id_comisaria = this.datosAccidenteFurat.id_comisaria == null ? null : this.datosAccidenteFurat.id_comisaria;
    this.OBJETO.dni_policia = this.datosAccidenteFurat.dni_policia == null ? "": this.datosAccidenteFurat.dni_policia;
    this.OBJETO.nombre_policia = this.datosAccidenteFurat.nombre_policia == null ? "" : this.datosAccidenteFurat.nombre_policia;
    this.OBJETO.apellido_paterno = this.datosAccidenteFurat.apellido_paterno == null ? "" : this.datosAccidenteFurat.apellido_paterno;
    this.OBJETO.apellido_materno = this.datosAccidenteFurat.apellido_materno == null ? "" : this.datosAccidenteFurat.apellido_materno;
    this.OBJETO.id_grado_policia = this.datosAccidenteFurat.id_grado_policia == null ? null : this.datosAccidenteFurat.id_grado_policia;
    this.OBJETO.cip_policia = this.datosAccidenteFurat.cip_policia == null ? "" : this.datosAccidenteFurat.cip_policia;
    this.OBJETO.telefono_policia = this.datosAccidenteFurat.telefono_policia == null ? "" : this.datosAccidenteFurat.telefono_policia;

    this.OBJETO.fecha_registro = (this.datosAccidenteFurat.fecha_registro != null) ? new Date(this.datosAccidenteFurat.fecha_registro) : null;
    this.OBJETO.hora_registro= this.datosAccidenteFurat.hora_registro == null ? "" : this.datosAccidenteFurat.hora_registro,
    this.OBJETO.id_clase_accidente = this.datosAccidenteFurat.id_clase_accidente;
    this.OBJETO.otro_clase_accidente= this.datosAccidenteFurat.otro_clase_accidente == null ? "" : this.datosAccidenteFurat.otro_clase_accidente,
    this.OBJETO.consecuencias=this.datosAccidenteFurat.consecuencias == null ? [] : this.datosAccidenteFurat.consecuencias;
    this.OBJETO.cantidad_fallecidos = this.datosAccidenteFurat.cantidad_fallecidos == null ? null : this.datosAccidenteFurat.cantidad_fallecidos;
    this.OBJETO.cantidad_lesionados = this.datosAccidenteFurat.cantidad_lesionados == null ? null : this.datosAccidenteFurat.cantidad_lesionados;
    this.OBJETO.cantidad_vehiculo_danado = this.datosAccidenteFurat.cantidad_vehiculo_danado == null ? null : this.datosAccidenteFurat.cantidad_vehiculo_danado;

    this.OBJETO.iddpto = this.datosAccidenteFurat.iddpto == null ? null : this.datosAccidenteFurat.iddpto;
    this.OBJETO.idprov = this.datosAccidenteFurat.idprov == null ? null : this.datosAccidenteFurat.idprov;
    this.OBJETO.iddist = this.datosAccidenteFurat.iddist == null ? null : this.datosAccidenteFurat.iddist;

    this.OBJETO.id_tipo_zona = this.datosAccidenteFurat.id_tipo_zona == null ? null : this.datosAccidenteFurat.id_tipo_zona;
    this.OBJETO.otro_tipo_via = this.datosAccidenteFurat.otro_tipo_via == null ? "" : this.datosAccidenteFurat.otro_tipo_via;
    this.OBJETO.id_tipo_via = this.datosAccidenteFurat.id_tipo_via == null ? null : this.datosAccidenteFurat.id_tipo_via;
    this.OBJETO.es_ciclovia = this.datosAccidenteFurat.es_ciclovia == null ? null : this.datosAccidenteFurat.es_ciclovia;
    this.OBJETO.direccion = this.datosAccidenteFurat.direccion == null ? "" : this.datosAccidenteFurat.direccion;
    this.OBJETO.referencia = this.datosAccidenteFurat.referencia == null ? "" : this.datosAccidenteFurat.referencia;

    this.OBJETO.km_k = this.datosAccidenteFurat.km_k == null ? "" : this.datosAccidenteFurat.km_k;
    this.OBJETO.km_m = this.datosAccidenteFurat.km_m == null ? "" : this.datosAccidenteFurat.km_m;

    this.OBJETO.id_codigo_carretera = this.datosAccidenteFurat.id_codigo_carretera == null ? null : this.datosAccidenteFurat.id_codigo_carretera;

    this.OBJETO.latitud = this.datosAccidenteFurat.latitud == null ? "" : this.datosAccidenteFurat.latitud;
    this.OBJETO.longitud = this.datosAccidenteFurat.longitud == null ? "" : this.datosAccidenteFurat.longitud;
    this.OBJETO.id_condicion_climatologica = this.datosAccidenteFurat.id_condicion_climatologica == null ? null : this.datosAccidenteFurat.id_condicion_climatologica;

    this.OBJETO.numero_vehiculos = this.datosAccidenteFurat.numero_vehiculos == null ? null : this.datosAccidenteFurat.numero_vehiculos;
    this.OBJETO.numero_conductores = this.datosAccidenteFurat.numero_conductores == null ? null : this.datosAccidenteFurat.numero_conductores;
    this.OBJETO.numero_peatones = this.datosAccidenteFurat.numero_peatones == null ? null : this.datosAccidenteFurat.numero_peatones;
    this.OBJETO.numero_pasajeros = this.datosAccidenteFurat.numero_pasajeros == null ? null : this.datosAccidenteFurat.numero_pasajeros;
    this.OBJETO.numero_ocupantes = this.datosAccidenteFurat.numero_ocupantes == null ? null : this.datosAccidenteFurat.numero_ocupantes;

    this.OBJETO.id_zonificacion = this.datosAccidenteFurat.id_zonificacion == null ? null : this.datosAccidenteFurat.id_zonificacion;
    this.OBJETO.id_caracteristica_via = this.datosAccidenteFurat.id_caracteristica_via == null ? null : this.datosAccidenteFurat.id_caracteristica_via;
    this.OBJETO.otro_caracteristica_via = this.datosAccidenteFurat.otro_caracteristica_via == null ? "" : this.datosAccidenteFurat.otro_caracteristica_via;
    this.OBJETO.id_perfil_longitudinal = this.datosAccidenteFurat.id_perfil_longitudinal == null ? null : this.datosAccidenteFurat.id_perfil_longitudinal;
    this.OBJETO.observacion_perfil = this.datosAccidenteFurat.observacion_perfil == null ? "" : this.datosAccidenteFurat.observacion_perfil;
    this.OBJETO.numero_carril = this.datosAccidenteFurat.numero_carril == null ? null : this.datosAccidenteFurat.numero_carril;
    this.OBJETO.numero_sendero = this.datosAccidenteFurat.numero_sendero == null ? null : this.datosAccidenteFurat.numero_sendero;
    this.OBJETO.id_superficie_calzada = this.datosAccidenteFurat.id_superficie_calzada == null ? null : this.datosAccidenteFurat.id_superficie_calzada;
    this.OBJETO.observacion_superficie = this.datosAccidenteFurat.observacion_superficie == null ? "" : this.datosAccidenteFurat.observacion_superficie;

    this.OBJETO.tiene_senializacion = this.datosAccidenteFurat.tiene_senializacion == null ? null : this.datosAccidenteFurat.tiene_senializacion;
    this.OBJETO.senial_vertical = this.datosAccidenteFurat.senial_vertical == null ? null : this.datosAccidenteFurat.senial_vertical;
    this.OBJETO.id_clasificacion_senial_vertical = this.datosAccidenteFurat.id_clasificacion_senial_vertical == null ? null : this.datosAccidenteFurat.id_clasificacion_senial_vertical;
    this.OBJETO.nombre_senial_vertical = this.datosAccidenteFurat.nombre_senial_vertical == null ? "" : this.datosAccidenteFurat.nombre_senial_vertical;
    this.OBJETO.senial_horizontal = this.datosAccidenteFurat.senial_horizontal == null ? null : this.datosAccidenteFurat.senial_horizontal;
    this.OBJETO.nombre_senial_horizontal = this.datosAccidenteFurat.nombre_senial_horizontal == null ? "" : this.datosAccidenteFurat.nombre_senial_horizontal;
    this.OBJETO.velocidad_maxima = this.datosAccidenteFurat.velocidad_maxima == null ? null : this.datosAccidenteFurat.velocidad_maxima;
    this.OBJETO.velocidad_minima = this.datosAccidenteFurat.velocidad_minima == null ? null : this.datosAccidenteFurat.velocidad_minima;

    this.OBJETO.id_causa_principal_accidente = this.datosAccidenteFurat.id_causa_principal_accidente == null ? null : this.datosAccidenteFurat.id_causa_principal_accidente;
    this.OBJETO.id_causa_especifica_accidente = this.datosAccidenteFurat.id_causa_especifica_accidente == null ? null : this.datosAccidenteFurat.id_causa_especifica_accidente;
    this.OBJETO.otro_causa_accidente = this.datosAccidenteFurat.otro_causa_accidente == null ? "" : this.datosAccidenteFurat.otro_causa_accidente;

    this.OBJETO.observaciones = this.datosAccidenteFurat.observaciones == null ? "" : this.datosAccidenteFurat.observaciones;
    this.showCodigoCarretera=this.datosAccidenteFurat.id_tipo_via==2 ? true : false;
    this.showOtraCaracteristica=this.datosAccidenteFurat.id_caracteristica_via==9 ? true: false;
    if(this.datosAccidenteFurat.id_caracteristica_via==1){
      this.showPerfilLongitudinal=true;
      this.showNumCarriles=true;
    }else{
      this.showPerfilLongitudinal=false;
      this.showNumCarriles=false;
    }


    this.showObservacionPerfil=this.datosAccidenteFurat.id_perfil_longitudinal==9 ? true:false;
    this.showObservacionSuperficieCalzada=this.datosAccidenteFurat.id_superficie_calzada== 8 ? true:false;
    this.showNombreSenial=this.datosAccidenteFurat.id_clasificacion_senial_vertical ? true:false;

    if(this.datosAccidenteFurat.id_causa_principal_accidente!=6 && this.datosAccidenteFurat.id_causa_principal_accidente!=null){
      let param={"id_causa_principal_accidente":this.datosAccidenteFurat.id_causa_principal_accidente}
        this.fs.accidenteTransitoFuratService.listarCausaSecundaria(param).subscribe((data: any) => {
          this.listaCausaEspecifica = data;
        });
    }

    this.showSecuencia=this.datosAccidenteFurat.id_clase_accidente==11? true : false;
    this.showSenialVertical=this.datosAccidenteFurat.tiene_senializacion==true ? true : false;

    this.showNombreSenialHorizontal=this.datosAccidenteFurat.senial_horizontal==true ? true:false;
    this.showCausaEspecifica=this.datosAccidenteFurat.id_causa_principal_accidente==1 ? true:false;
    this.showOtroTipoVia = this.datosAccidenteFurat.id_tipo_via == 9 ? true:false;

    this.showClasificacion = this.datosAccidenteFurat.senial_vertical==true ? true:false;




  }

  listarConductorVehiculoControl(){
    this.fs.accidenteTransitoFuratService.listarAccidenteFuratControl().subscribe(
      (data:any)=>{
        this.listaCaracteristicaVia=data.caracteristicavia;
        this.listaCausaPrincipal=data.causaprincipal;
        this.listaClaseAccidente = data.claseaccidente;
        this.listaCodigoCarretera = data.codigocarretera;
        this.listaComisaria = data.comisaria;
        this.listaCondicionClimatologica=data.condicionclimatologica;
        this.listaConsecuenciaAccidente = data.consecuenciaaccidente;

        this.listaConsecuenciaAccidente.forEach((algo:any)=>{

          if(this.datosAccidenteFurat.consecuencias==null){
            algo.seleccionado=false;
          }else{
            let valor =this.datosAccidenteFurat.consecuencias.filter(x=>x.id_consecuencia_accidente==algo.id_consecuencia_accidente).length;
            algo.seleccionado=valor>0? true:false
            algo.id_detalle=valor>0 ? this.datosAccidenteFurat.consecuencias.filter(x=>x.id_consecuencia_accidente==algo.id_consecuencia_accidente)[0].id_consecuencia_accidente_det :0

          }


        });
        this.listaGradoPolicia = data.gradopolicia;
        this.listaPerfilLongitudinal=data.perfillongitudinal;
        this.listaSenialVertical = data.senialvertical;
        this.listaSuperficieCalzada = data.superficiecalzada;
        this.listaTipoVia = data.tipovia;
        this.listaTipoZona = data.tipozona;
        this.listaZonificacion = data.zonificacion;
      }
    )


  }

  seleccionarConsecuencia(evento,estado){
    // this.OBJETO.consecuencias.push({
    //   "id_consecuencia_accidente_det":0,
    //   "id_consecuencia_accidente":evento,
    //   "id_accidente_transito_furat":this.datosAccidenteFurat.id_accidente_transito_furat==null ? 0 :this.datosAccidenteFurat.id_accidente_transito_furat,
    //   "activo":true,
    //   "usuario_creacion":sessionStorage.getItem("Usuario"),
    //   "usuario_modificacion":sessionStorage.getItem("Usuario"),
    // })
  }

  validarOtraCaracteristica(evento){
    if(evento!=undefined){
      if(evento.id_caracteristica_via==9){
        this.showOtraCaracteristica=true;
      }else{
        this.showOtraCaracteristica=false;
        this.OBJETO.otro_caracteristica_via="";
      }

      if(evento.id_caracteristica_via==1){
        this.showPerfilLongitudinal=true;
        this.showNumCarriles=true;
      }else{
        this.showPerfilLongitudinal=false;
        this.showNumCarriles=false;
        this.OBJETO.id_perfil_longitudinal=null;
        this.OBJETO.numero_carril=null;
        this.OBJETO.numero_sendero=null;
      }
    }else{
      this.showOtraCaracteristica=false;
      this.OBJETO.otro_caracteristica_via="";
      this.showNumCarriles=false;
      this.OBJETO.id_perfil_longitudinal=null;
      this.OBJETO.numero_carril=null;
      this.OBJETO.numero_sendero=null;
    }
  }

  validarObservacionPerfil(evento){
    if(evento!=undefined){
      if(evento.id_perfil_longitudinal==9){
        this.showObservacionPerfil=true;
      }else{
        this.showObservacionPerfil=false;
        this.OBJETO.observacion_perfil="";
      }
    }else{
      this.showObservacionPerfil=false;
      this.OBJETO.observacion_perfil="";
    }
  }

  validarObservacionSuperficie(evento){
    if(evento!=undefined){
      if(evento.id_superficie_calzada==8){
        this.showObservacionSuperficieCalzada=true;
      }else{
        this.showObservacionSuperficieCalzada=false;
        this.OBJETO.observacion_superficie="";
      }
    }else{
        this.showObservacionSuperficieCalzada=false;
        this.OBJETO.observacion_superficie="";
    }
  }

  validarSenialVertical(evento){
    if(evento){
      this.showSenialVertical=true;
    }else{
      this.showSenialVertical=false;
      this.OBJETO.senial_vertical=null;
    }
  }

  validarClasificacion(evento){
    if(evento){
      this.showClasificacion=true;
    }else{
      this.showClasificacion=false;
      this.OBJETO.id_clasificacion_senial_vertical=null;
      this.OBJETO.nombre_senial_vertical=null;
    }
  }


  seleccionarSenialHorizontal(evento,valor){
    if (valor) {
      this.showNombreSenialHorizontal =true
    } else {
      this.showNombreSenialHorizontal =false;
      this.OBJETO.nombre_senial_horizontal="";
    }
  }

  validarClasificacionVertical(evento){
    if(evento!=undefined){
      this.showNombreSenial=true;
    }else{
      this.showNombreSenial=false;
      this.OBJETO.nombre_senial_vertical=null;
    }

  }

  seleccionarCausaEspecifica(evento){
    if(evento!=undefined){
      if(evento.id_causa_principal_accidente!=6){
        this.showCausaEspecifica=true;
        let param={"id_causa_principal_accidente":evento.id_causa_principal_accidente}
        this.fs.accidenteTransitoFuratService.listarCausaSecundaria(param).subscribe((data: any) => {
          this.listaCausaEspecifica = data;
        });
      }else{
        this.showCausaEspecifica=false;
        this.OBJETO.id_causa_especifica_accidente=null;
      }
    }else{
        this.showCausaEspecifica=false;
        this.OBJETO.id_causa_especifica_accidente=null;
    }
  }

  validarInformacionReniec() {
    let valDni = this.OBJETO.dni_policia;
    let cant = 0;
    if (valDni == '' || valDni == null) {
      document.getElementById('dni_policia').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de Dni a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionReniec(valDni).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.OBJETO.nombre_policia=response.strnombres;
              this.OBJETO.apellido_paterno=response.strapellidopaterno;
              this.OBJETO.apellido_materno=response.strapellidomaterno;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
              this.OBJETO.nombre_policia=null;
              this.OBJETO.apellido_paterno=null;
              this.OBJETO.apellido_materno=null;
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
            this.OBJETO.nombre_policia=null;
            this.OBJETO.apellido_paterno=null;
            this.OBJETO.apellido_materno=null;
          }
        );
      } else {
        this.funciones.mensaje('info', 'El DNI ingresado ya se encuentra registrado en el sistema.');
      }
    }
  }

  Grabar(){
    this.listaConsecuenciaAccidente.forEach(element => {
      if(this.datosAccidenteFurat.consecuencias==null){
        if(element.seleccionado){
          this.OBJETO.consecuencias.push({
            "id_consecuencia_accidente_det":0,
            "id_consecuencia_accidente":element.id_consecuencia_accidente,
            "id_accidente_transito_furat":this.datosAccidenteFurat.id_accidente_transito_furat==null ? 0 :this.datosAccidenteFurat.id_accidente_transito_furat,
            "activo":true,
            "usuario_creacion":sessionStorage.getItem("Usuario"),
            "usuario_modificacion":sessionStorage.getItem("Usuario"),
          })
        }
      }else{
        // console.log("elementt",element.id_detalle);
        // if(element.id_detalle>0){
        //   this.OBJETO.consecuencias.push({
        //     "id_consecuencia_accidente_det":element.id_detalle,
        //     "id_consecuencia_accidente":element.id_consecuencia_accidente,
        //     "id_accidente_transito_furat":this.datosAccidenteFurat.id_accidente_transito_furat==null ? 0 :this.datosAccidenteFurat.id_accidente_transito_furat,
        //     "activo":element.seleccionado,
        //     "usuario_creacion":sessionStorage.getItem("Usuario"),
        //     "usuario_modificacion":sessionStorage.getItem("Usuario"),
        //   })
        // }else{
        //   if(element.seleccionado){
        //     this.OBJETO.consecuencias.push({
        //       "id_consecuencia_accidente_det":0,
        //       "id_consecuencia_accidente":element.id_consecuencia_accidente,
        //       "id_accidente_transito_furat":this.datosAccidenteFurat.id_accidente_transito_furat==null ? 0 :this.datosAccidenteFurat.id_accidente_transito_furat,
        //       "activo":element.seleccionado,
        //       "usuario_creacion":sessionStorage.getItem("Usuario"),
        //       "usuario_modificacion":sessionStorage.getItem("Usuario"),
        //     })
        //   }

        // }
      }
    });

    if (this.Validar()) {

      //this.asignarValoreSituacionVia();
      let param={
        "id_accidente_transito_furat":this.OBJETO.id_accidente_transito_furat,
        "id_accidente_transito":this.OBJETO.id_accidente_transito,
        "id_comisaria":this.OBJETO.id_comisaria,
        "cod_registro_accidente":this.OBJETO.cod_registro_accidente,
        "dni_policia":this.OBJETO.dni_policia,
        "nombre_policia":this.OBJETO.nombre_policia,
        "apellido_paterno":this.OBJETO.apellido_paterno,
        "apellido_materno":this.OBJETO.apellido_materno,
        "id_grado_policia":this.OBJETO.id_grado_policia,
        "cip_policia":this.OBJETO.cip_policia,
        "telefono_policia":this.OBJETO.telefono_policia,
        "cantidad_fallecidos":Number(this.OBJETO.cantidad_fallecidos),
        "cantidad_lesionados":Number(this.OBJETO.cantidad_lesionados),
        "cantidad_vehiculo_danado":Number(this.OBJETO.cantidad_vehiculo_danado),
        "es_ciclovia":Boolean(this.OBJETO.es_ciclovia),
        "km_k":this.OBJETO.km_k,
        "km_m":this.OBJETO.km_m,
        "id_condicion_climatologica":this.OBJETO.id_condicion_climatologica,
        "numero_vehiculos":Number(this.OBJETO.numero_vehiculos),
        "numero_conductores":Number(this.OBJETO.numero_conductores),
        "numero_peatones":Number(this.OBJETO.numero_peatones),
        "numero_pasajeros":Number(this.OBJETO.numero_pasajeros),
        "numero_ocupantes":Number(this.OBJETO.numero_ocupantes),
        "id_zonificacion":this.OBJETO.id_zonificacion,
        "id_caracteristica_via":this.OBJETO.id_caracteristica_via,
        "otro_caracteristica_via":this.OBJETO.otro_caracteristica_via,
        "id_perfil_longitudinal":this.OBJETO.id_perfil_longitudinal,
        "observacion_perfil":this.OBJETO.observacion_perfil,
        "numero_carril":Number(this.OBJETO.numero_carril), // this.OBJETO.numero_carril!=null ? Number(this.OBJETO.numero_carril) : this.OBJETO.numero_carril,
        "numero_sendero":Number(this.OBJETO.numero_sendero), // this.OBJETO.numero_sendero!=null ? Number(this.OBJETO.numero_sendero) : this.OBJETO.numero_sendero,
        "id_superficie_calzada":this.OBJETO.id_superficie_calzada,
        "observacion_superficie":this.OBJETO.observacion_superficie,
        "tiene_senializacion":Boolean(this.OBJETO.tiene_senializacion),
        "senial_vertical":this.OBJETO.senial_vertical==null ? null: Boolean(this.OBJETO.senial_vertical),
        "id_clasificacion_senial_vertical":this.OBJETO.id_clasificacion_senial_vertical,
        "nombre_senial_vertical":this.OBJETO.nombre_senial_vertical,
        "senial_horizontal":Boolean(this.OBJETO.senial_horizontal),
        "nombre_senial_horizontal":this.OBJETO.nombre_senial_horizontal,
        "velocidad_maxima":this.castToFloat(this.OBJETO.velocidad_maxima),
        "velocidad_minima":this.castToFloat(this.OBJETO.velocidad_minima),
        "id_causa_principal_accidente":this.OBJETO.id_causa_principal_accidente,
        "id_causa_especifica_accidente":this.OBJETO.id_causa_especifica_accidente,
        "otro_causa_accidente":this.OBJETO.otro_causa_accidente,
        "observaciones":this.OBJETO.observaciones,
        "id_codigo_carretera":this.OBJETO.id_codigo_carretera,
        "activo":true,
        "usuario_creacion":this.usuario,
        "usuario_modificacion":sessionStorage.getItem("Usuario"),
        "consecuencias":this.OBJETO.consecuencias
      }
      if (this.datosAccidenteFurat.id_accidente_transito_furat == null) {
        //this.bMostrar = true;
        this.fs.accidenteTransitoFuratService.insertarAccidenteTransitoFurat(param).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            //this.bMostrar = false;
          });
      } else {
        this.fs.accidenteTransitoFuratService.modificarAccidenteTransitoFurat(param).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            //this.bMostrar = false;
          });
      }
    }
  }

  castToFloat(monto: number | string): number {
    if (monto == 0 || monto == "") {
      return 0;
    }
    let valueWithReplace = monto.toString().replace(/,/g, "");
    let value = Number.parseFloat(valueWithReplace);
    return value;
  }

  Validar(): boolean {
    let accidenteFurat = this.OBJETO;
    if (accidenteFurat.id_comisaria == null || accidenteFurat.id_comisaria == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Comisaria</b>", true, () => {
        this.selectTipoComisaria.focus();
      });
      return false;
    }
    if (accidenteFurat.dni_policia == null || accidenteFurat.dni_policia == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>DNI del Policia</b>", true, () => {
        document.getElementById("dni_policia").focus();
      });
      return false;
    }
    if (accidenteFurat.nombre_policia == null || accidenteFurat.nombre_policia == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>Nombre del Policia</b>", true, () => {
        document.getElementById("nombre_policia").focus();
      });
      return false;
    }
    if (accidenteFurat.apellido_paterno == null || accidenteFurat.apellido_paterno == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>apellido paterno del policia</b>", true, () => {
        document.getElementById("apellido_paterno").focus();
      });
      return false;
    }
    if (accidenteFurat.apellido_materno == null || accidenteFurat.apellido_materno == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>apellido materno del policia</b>", true, () => {
        document.getElementById("apellido_materno").focus();
      });
      return false;
    }
    if (accidenteFurat.id_grado_policia == null || accidenteFurat.id_grado_policia == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Grado del Policia</b>", true, () => {
        this.selectTipoGrado.focus();
      });
      return false;
    }
    if (accidenteFurat.cip_policia == null || accidenteFurat.cip_policia == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>N° CIP del policia</b>", true, () => {
        document.getElementById("cip_policia").focus();
      });
      return false;
    }
    if (accidenteFurat.telefono_policia == null || accidenteFurat.telefono_policia == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>Telefono del policia</b>", true, () => {
        document.getElementById("telefono_policia").focus();
      });
      return false;
    }
    if (accidenteFurat.cantidad_fallecidos == null || accidenteFurat.cantidad_fallecidos == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Cantidad Fallecidos</b>", true, () => {
        document.getElementById("cantidad_fallecidos").focus();
      });
      return false;
    }
    if (accidenteFurat.cantidad_lesionados == null || accidenteFurat.cantidad_lesionados == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Cantidad Lesionados</b>", true, () => {
        document.getElementById("cantidad_lesionados").focus();
      });
      return false;
    }
    if (accidenteFurat.cantidad_vehiculo_danado == null || accidenteFurat.cantidad_vehiculo_danado == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Cantidad de vehículos dañados</b>", true, () => {
        document.getElementById("cantidad_vehiculo_danado").focus();
      });
      return false;
    }
    if (accidenteFurat.es_ciclovia == null ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Ciclovia</b>", true, () => {
        //document.getElementById("cantidad_vehiculo_danado").focus();
      });
      return false;
    }
    if (accidenteFurat.id_tipo_via==2 && (accidenteFurat.km_k == null || accidenteFurat.km_k == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <br><b>kilometro</b>", true, () => {
        document.getElementById("km_k").focus();
      });
      return false;
    }
    if (accidenteFurat.id_tipo_via==2 && (accidenteFurat.km_m == null || accidenteFurat.km_m == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <br><b>kilometro</b>", true, () => {
        document.getElementById("km_m").focus();
      });
      return false;
    }
    if (accidenteFurat.id_tipo_via==2 && (accidenteFurat.id_codigo_carretera == null || accidenteFurat.id_codigo_carretera == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Código Carretera</b>", true, () => {
        this.selectTipoCodigoCarretera.focus();
      });
      return false;
    }
    if (accidenteFurat.id_condicion_climatologica == null || accidenteFurat.id_condicion_climatologica == 0) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Condición Climatologica</b>", true, () => {
        this.selectCondicionClimatologica.focus();
      });
      return false;
    }
    if (accidenteFurat.numero_vehiculos == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Número de Vehículos</b>", true, () => {
        document.getElementById("numero_vehiculos").focus();
      });
      return false;
    }
    if (accidenteFurat.numero_conductores == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>Número de Conductores</b>", true, () => {
        document.getElementById("numero_conductores").focus();
      });
      return false;
    }
    if (accidenteFurat.numero_peatones == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>Números de peatones</b>", true, () => {
        document.getElementById("numero_peatones").focus();
      });
      return false;
    }
    if (accidenteFurat.numero_pasajeros == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>Número de Pasajeros</b>", true, () => {
        document.getElementById("numero_pasajeros").focus();
      });
      return false;
    }
    if (accidenteFurat.numero_ocupantes == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el<br><b>Número de Ocupantes</b>", true, () => {
        document.getElementById("numero_ocupantes").focus();
      });
      return false;
    }
    if (accidenteFurat.id_zonificacion == null || accidenteFurat.id_zonificacion == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Zonificación</b>", true, () => {
        this.selectTipoZonificacion.focus();
      });
      return false;
    }
    if (accidenteFurat.id_caracteristica_via == null || accidenteFurat.id_caracteristica_via == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Caracteristica</b>", true, () => {
        this.selectTipoCaracteristica.focus();
      });
      return false;
    }
    if (accidenteFurat.id_caracteristica_via==9 && (accidenteFurat.otro_caracteristica_via == null || accidenteFurat.otro_caracteristica_via == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la<br><b>Otra Caracteristica</b>", true, () => {
        document.getElementById("otro_caracteristica_via").focus();
      });
      return false;
    }
    if (accidenteFurat.id_caracteristica_via==1 && (accidenteFurat.id_perfil_longitudinal == null || accidenteFurat.id_perfil_longitudinal == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Perfil Longitudinal de la Vía</b>", true, () => {
        this.selectTipoPerfilLongitudinal.focus();
      });
      return false;
    }
    if (accidenteFurat.id_caracteristica_via==1 && (accidenteFurat.numero_carril == null || accidenteFurat.numero_carril == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la<br><b>Número de Carril</b>", true, () => {
        document.getElementById("numero_carril").focus();
      });
      return false;
    }
    if (accidenteFurat.id_caracteristica_via==1 && (accidenteFurat.numero_sendero == null || accidenteFurat.numero_sendero == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese<br><b>Número de Senderos</b>", true, () => {
        document.getElementById("numero_sendero").focus();
      });
      return false;
    }
    if (accidenteFurat.id_superficie_calzada == null || accidenteFurat.id_superficie_calzada == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Superficie de la Calzada</b>", true, () => {
        this.selectTipoSuperficieCalzada.focus();
      });
      return false;
    }

    if (accidenteFurat.tiene_senializacion == null ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>¿Existe alguna señalización próxima o en el lugar del accidente?</b>", true, () => {
        document.getElementById("siSenializacion").focus();
      });
      return false;
    }
    if (accidenteFurat.tiene_senializacion==true && accidenteFurat.senial_vertical == null ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>¿Existe alguna señal vertical?</b>", true, () => {
        document.getElementById("siSenialVertical").focus();
      });
      return false;
    }
    if (accidenteFurat.senial_vertical == true && (accidenteFurat.id_clasificacion_senial_vertical == null || accidenteFurat.id_clasificacion_senial_vertical == 0) ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Clasificación</b>", true, () => {
        this.selectClasificacionVertical.focus();
      });
      return false;
    }
    if (accidenteFurat.senial_vertical == true && (accidenteFurat.nombre_senial_vertical == null || accidenteFurat.nombre_senial_vertical == "") ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Nombre Señal</b>", true, () => {
        document.getElementById("nombre_senial_vertical").focus();
      });
      return false;
    }
    if (accidenteFurat.tiene_senializacion==true && (accidenteFurat.senial_horizontal == null ) ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>¿Existe alguna señal horizontal?</b>", true, () => {
        document.getElementById("siSenialHorizontal").focus();
      });
      return false;
    }
    if (accidenteFurat.senial_horizontal==true && (accidenteFurat.nombre_senial_horizontal == null || accidenteFurat.nombre_senial_horizontal == "") ) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Nombre señal horizontal</b>", true, () => {
        document.getElementById("nombre_senial_horizontal").focus();
      });
      return false;
    }


    if (accidenteFurat.id_causa_principal_accidente == null || accidenteFurat.id_causa_principal_accidente == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Causa Principal del Accidente</b>", true, () => {
        this.selectCausaPrincipal.focus();
      });
      return false;
    }
    if (accidenteFurat.id_causa_principal_accidente!=6 && (accidenteFurat.id_causa_especifica_accidente == null || accidenteFurat.id_causa_especifica_accidente == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la<br><b>Causa Especifica</b>", true, () => {
        this.selectCausaEspecifica.focus();
      });
      return false;
    }
    return true;
  }

  closeModal(){
    this.modalRef.hide();
    this.retornoValores.emit(0);
  }

}
