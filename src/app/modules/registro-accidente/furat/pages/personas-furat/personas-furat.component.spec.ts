import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasFuratComponent } from './personas-furat.component';

describe('PersonasFuratComponent', () => {
  let component: PersonasFuratComponent;
  let fixture: ComponentFixture<PersonasFuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonasFuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasFuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
