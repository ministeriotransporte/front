import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPersonasFuratComponent } from './modal-personas-furat.component';

describe('ModalPersonasFuratComponent', () => {
  let component: ModalPersonasFuratComponent;
  let fixture: ComponentFixture<ModalPersonasFuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPersonasFuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPersonasFuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
