import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { NgSelectComponent } from '@ng-select/ng-select';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';

@Component({
  selector: 'app-modal-personas-furat',
  templateUrl: './modal-personas-furat.component.html',
  styleUrls: ['./modal-personas-furat.component.scss']
})
export class ModalPersonasFuratComponent implements OnInit {

  @ViewChild('tipoPersonaInvolucrada', { static: false }) selectTipoPersonaInvolucrada: NgSelectComponent;
  @ViewChild('tipoGravedad', { static: false }) selectTipoGravedad: NgSelectComponent;
  @ViewChild('tipoLugarAtencion', { static: false }) selectLugarAtencion: NgSelectComponent;
  @ViewChild('tipoLugarDefuncion', { static: false }) selectLugarDefuncion: NgSelectComponent;
  @ViewChild('tipoNacionalidad', { static: false }) selectTipoNacionalidad: NgSelectComponent;
  @ViewChild('tipoClaseCategoria', { static: false }) selectTipoClaseCategoria: NgSelectComponent; tipoOrganismoEmisor
  @ViewChild('tipoOrganismoEmisor', { static: false }) selectOrganismoEmisor: NgSelectComponent;
  @ViewChild('tipoLugarDosaje', { static: false }) selectLugarDosaje: NgSelectComponent;
  @ViewChild('tipoRazonNoDosaje', { static: false }) selectRazonNoDosaje: NgSelectComponent;
  @ViewChild('tipoSexo', { static: false }) selectTipoSexo: NgSelectComponent;
  @ViewChild('tipoDocumento', { static: false }) selectTipoDocumento: NgSelectComponent;
  @ViewChild('situacion', { static: false }) selectSituacion: NgSelectComponent;
  @ViewChild('horaDosajeCualitativo', { static: false }) selectHoradosaje: NgSelectComponent;


  listaTipoPersonaInvolucrada: any[];
  listaEstadoGravedad: any[];
  listaLugarAtencion: any[];
  listaLugarDefuncion: any[];
  listaNacionalidad: any[];
  listaClaseCategoria: any[];
  listaOrganismoEmisor: any[];
  listaLugarDosaje: any[];
  listaRazonNoDosaje: any[];
  listaModalidadTransporte: any[];
  listaTipoDocumento: any[];
  listaSexo: any[];
  showotraLugarAtencion: boolean = false;
  showotraNacionalidad: boolean = false;
  showOtroPais: boolean = false;
  showOtroEmisionLicencia: boolean = false;
  showOtroOrganismoEmisor: boolean = false;
  showOtroLugarDosaje: boolean = false;
  showOtroRazonDosaje: boolean = false;
  showLicencia: boolean = false;
  showDosajeCualitativo: boolean = false;
  showDosajeCuantitativoNo: boolean = false;
  showDosajeCuantitativoSi: boolean = false;
  datosPersonaFurat;
  resultado_dosaje_cualitativo: boolean = true;
  ACCION: number = null;
  id_peaton;
  OBJETO2={
    "id_tipo_persona_accidente": null,
    "id_tipo_documento": null,
    "id_tipo_sexo": null,
    "id_situacion_pea_pas_ocu": null,
    "dni": null,

    "edad_aprox":null,




  };
  OBJETO = {
    "otra_nacionalidad": null,
    "otro_emision_doc": null,
    "id_nacionalidad": null,
    "id_persona_accidente_furat": null,
    "id_persona_accidente": null,
    "id_tipo_persona_accidente_furat": null,
    "id_estado_gravedad": null,
    "id_lugar_atencion": null,
    "otro_lugar_atencion": null,
    "id_lugar_defuncion": null,
    "otro_lugar_defuncion": null,

    "emision_doc_peru": null,



    "tiene_licencia": null,
    "numero_licencia": null,
    "id_clase_categoria": null,
    "esta_vigente": null,
    "emision_licencia_peru": null,
    "otro_emision_licencia": null,
    "id_organismo_emisor": null,
    "otro_organismo_emisor": null,
    "aparece_snc": null,
    "dosaje_cualitativo": null,
    "hora_dosaje_cualitativo": null,
    "resultado_dosaje_cualitativo": false,
    "id_lugar_dosaje": null,
    "otro_lugar_dosaje": null,
    "dosaje_cuantitativo": null,
    "id_razon_dosaje_cuantitativo": null,
    "otro_razon_dosaje_cuantitativo": null,
    "hora_dosaje_cuantitativo": null,
    "resultado_dosaje_cuantitativo":"",
    "observaciones":null,
    'id_peaton':null,
    "activo":true,
    "id_conductor_vehiculo":null,
    "id_pasajero_vehiculo":null,
    "id_ocupante_vehiculo":null,
    "cod_persona_accidente":null,
    "usuario_creacion":parseInt(sessionStorage.getItem("IdUsuario")),






    /*




    */









  }

  @Output() retornoValores = new EventEmitter();
  constructor(private fs: FacadeService,
    private fb: FormBuilder,
    public funciones: Funciones,
    public modalRef: BsModalRef,
    private ruta: ParametrosGeneralesServiceService) { }

  ngOnInit() {

    this.listarPersonaFuratControl();
    if (this.ACCION == 2) {
      this.setear();
      this.validarDosajeEtilicoCuantitativo(this.OBJETO.dosaje_cuantitativo);
      this.validarLicencia(this.OBJETO.tiene_licencia);
      this.validarDosajeEtilicoCualitativo(this.OBJETO.dosaje_cualitativo);
      this.validarDosajeEtilicoCuantitativo(this.OBJETO.dosaje_cuantitativo);
      this.envento(this.OBJETO.emision_doc_peru);
      this.validarRazon(this.OBJETO.id_razon_dosaje_cuantitativo);

    }
  }

  listarPersonaFuratControl() {
    this.fs.personaFuratService.listarPersonaFuratControl().subscribe(
      (data: any) => {
        this.listaEstadoGravedad = data.ambitoservicio;
        this.listaTipoPersonaInvolucrada = data.tipopersona;
        this.listaEstadoGravedad = data.gravedad;
        this.listaLugarAtencion = data.lugaratencion;
        this.listaLugarDefuncion = data.lugardefuncion;
        this.listaModalidadTransporte = data.situacion;
        this.listaTipoDocumento = data.tipodocumento;
        this.listaNacionalidad = data.nacionalidad;
        this.listaSexo = data.tiposexo;
        this.listaClaseCategoria = data.clasecategoria;
        this.listaOrganismoEmisor = data.organismoemisor;
        this.listaLugarDosaje = data.lugardosaje;
        this.listaRazonNoDosaje = data.razondosajecuantitativo;
      }
    )
  }

  setear() {
    this.OBJETO.id_persona_accidente_furat = this.datosPersonaFurat.id_persona_accidente_furat == null ? 0 : this.datosPersonaFurat.id_persona_accidente_furat;
    this.OBJETO.id_persona_accidente = this.datosPersonaFurat.id_persona_accidente == null ? 0 : this.datosPersonaFurat.id_persona_accidente;
    this.OBJETO.id_tipo_persona_accidente_furat = this.datosPersonaFurat.id_tipo_persona_accidente_furat == null ? 0 : this.datosPersonaFurat.id_tipo_persona_accidente_furat;
    this.OBJETO.id_estado_gravedad = this.datosPersonaFurat.id_estado_gravedad == null ? 0 : this.datosPersonaFurat.id_estado_gravedad;
    this.OBJETO.id_lugar_atencion = this.datosPersonaFurat.id_lugar_atencion == null ? 0 : this.datosPersonaFurat.id_lugar_atencion;
    this.showotraLugarAtencion = this.datosPersonaFurat.id_lugar_atencion == 3 ? true : false;
    this.OBJETO.otro_lugar_atencion = this.datosPersonaFurat.otro_lugar_atencion == null ? "" : this.datosPersonaFurat.otro_lugar_atencion;
    this.OBJETO.id_lugar_defuncion = this.datosPersonaFurat.id_lugar_defuncion == null ? 0 : this.datosPersonaFurat.id_lugar_defuncion;
    this.OBJETO.otro_lugar_defuncion = this.datosPersonaFurat.otro_lugar_defuncion == null ? "" : this.datosPersonaFurat.otro_lugar_defuncion;
    this.OBJETO.id_nacionalidad = this.datosPersonaFurat.id_nacionalidad == null ? 0 : this.datosPersonaFurat.id_nacionalidad;
    this.showotraNacionalidad = this.datosPersonaFurat.id_nacionalidad == 2 ? true : false;
    this.OBJETO.otra_nacionalidad = this.datosPersonaFurat.otra_nacionalidad == null ? "" : this.datosPersonaFurat.otra_nacionalidad;
    this.OBJETO.emision_doc_peru = this.datosPersonaFurat.emision_doc_peru == null ? false : this.datosPersonaFurat.emision_doc_peru;
    this.OBJETO.otro_emision_doc = this.datosPersonaFurat.otro_emision_doc == null ? "" : this.datosPersonaFurat.otro_emision_doc;
    this.OBJETO.tiene_licencia = this.datosPersonaFurat.tiene_licencia == null ? false : this.datosPersonaFurat.tiene_licencia;
    this.OBJETO.numero_licencia = this.datosPersonaFurat.numero_licencia == null ? "" : this.datosPersonaFurat.numero_licencia;
    this.OBJETO.id_clase_categoria = this.datosPersonaFurat.id_clase_categoria == null ? 0 : this.datosPersonaFurat.id_clase_categoria;
    this.OBJETO.esta_vigente = this.datosPersonaFurat.esta_vigente == null ? false : this.datosPersonaFurat.esta_vigente;
    this.OBJETO.emision_licencia_peru = this.datosPersonaFurat.emision_licencia_peru == 2 ? true : false;
    this.OBJETO.otro_emision_licencia = this.datosPersonaFurat.otro_emision_licencia == null ? "" : this.datosPersonaFurat.otro_emision_licencia;
    this.showOtroOrganismoEmisor = this.datosPersonaFurat.id_organismo_emisor == 5 ? true : false;
    this.OBJETO.otro_organismo_emisor = this.datosPersonaFurat.otro_organismo_emisor == null ? "" : this.datosPersonaFurat.otro_organismo_emisor;
    this.OBJETO.aparece_snc = this.datosPersonaFurat.aparece_snc == null ? false : this.datosPersonaFurat.aparece_snc;
    this.OBJETO.dosaje_cualitativo = this.datosPersonaFurat.dosaje_cualitativo == null ? false : this.datosPersonaFurat.dosaje_cualitativo;
    this.OBJETO.hora_dosaje_cualitativo = this.datosPersonaFurat.hora_dosaje_cualitativo == null ? "" : this.datosPersonaFurat.hora_dosaje_cualitativo;
    this.OBJETO.resultado_dosaje_cualitativo = this.resultado_dosaje_cualitativo;
    this.OBJETO.id_lugar_dosaje = this.datosPersonaFurat.id_lugar_dosaje == null ? 0 : this.datosPersonaFurat.id_lugar_dosaje;
    this.OBJETO.otro_lugar_dosaje = this.datosPersonaFurat.otro_lugar_dosaje == null ? "" : this.datosPersonaFurat.otro_lugar_dosaje;
    this.OBJETO.dosaje_cuantitativo = this.datosPersonaFurat.dosaje_cuantitativo == null ? false : this.datosPersonaFurat.dosaje_cuantitativo;
    this.OBJETO.id_razon_dosaje_cuantitativo = this.datosPersonaFurat.id_razon_dosaje_cuantitativo == null ? 0 : this.datosPersonaFurat.id_razon_dosaje_cuantitativo;
    this.OBJETO.otro_razon_dosaje_cuantitativo = this.datosPersonaFurat.otro_razon_dosaje_cuantitativo == null ? "" : this.datosPersonaFurat.otro_razon_dosaje_cuantitativo;
    this.OBJETO.hora_dosaje_cuantitativo = this.datosPersonaFurat.hora_dosaje_cuantitativo == null ? "" : this.datosPersonaFurat.hora_dosaje_cuantitativo;
    this.OBJETO.resultado_dosaje_cuantitativo = this.datosPersonaFurat.resultado_dosaje_cuantitativo == null ? true : this.datosPersonaFurat.resultado_dosaje_cuantitativo;
    this.OBJETO.observaciones = this.datosPersonaFurat.observaciones == null ? "" : this.datosPersonaFurat.observaciones;
    this.OBJETO.id_peaton = this.datosPersonaFurat.id_peaton == null ? 0 : this.datosPersonaFurat.id_peaton;
    this.OBJETO.id_conductor_vehiculo = this.datosPersonaFurat.id_conductor_vehiculo == null ? null : this.datosPersonaFurat.id_conductor_vehiculo;
    this.OBJETO.id_pasajero_vehiculo = this.datosPersonaFurat.id_pasajero_vehiculo == null ? null : this.datosPersonaFurat.id_pasajero_vehiculo;
    this.OBJETO.id_ocupante_vehiculo = this.datosPersonaFurat.id_ocupante_vehiculo == null ? null : this.datosPersonaFurat.id_ocupante_vehiculo;
    this.OBJETO.cod_persona_accidente = this.datosPersonaFurat.cod_persona_accidente == null ? "" : this.datosPersonaFurat.cod_persona_accidente;
    this.OBJETO2.edad_aprox = this.datosPersonaFurat.edad_aprox == null ? "" : this.datosPersonaFurat.edad_aprox;



    this.OBJETO2.id_tipo_persona_accidente = this.datosPersonaFurat.id_tipo_persona_accidente == null ? 0 : this.datosPersonaFurat.id_tipo_persona_accidente;
    this.OBJETO2.id_tipo_documento = this.datosPersonaFurat.id_tipo_documento == null ? 0 : this.datosPersonaFurat.id_tipo_documento;
    this.OBJETO2.id_tipo_sexo = this.datosPersonaFurat.id_tipo_sexo == null ? null : this.datosPersonaFurat.id_tipo_sexo;
    this.OBJETO2.dni = this.datosPersonaFurat.dni == null ? "" : this.datosPersonaFurat.dni;
    this.OBJETO2.id_situacion_pea_pas_ocu = this.datosPersonaFurat.id_situacion_pea_pas_ocu == null ? 0 : this.datosPersonaFurat.id_situacion_pea_pas_ocu;
    //this.OBJETO2.otro_pais = this.datosPersonaFurat.otro_pais == null ? "" : this.datosPersonaFurat.otro_pais;

    //this.OBJETO.otro_emision_licencia = this.datosPersonaFurat.otro_emision_licencia == null ? "" : this.datosPersonaFurat.otro_emision_licencia;
    this.OBJETO.id_organismo_emisor = this.datosPersonaFurat.id_organismo_emisor == null ? 0 : this.datosPersonaFurat.id_organismo_emisor;

    //this.OBJETO.id_razon_dosaje_cuantitativo = this.datosPersonaFurat.id_razon_dosaje_cuantitativo == null ? null : this.datosPersonaFurat.id_razon_dosaje_cuantitativo;
  }
  envento(event) {
    if (event) {
      this.showOtroPais = false;


    } else {
      this.showOtroPais = true;
    }
  }
  validarLugarAtencion(eveno) {
    if (eveno != undefined) {
      if (eveno.id_lugar_atencion == 3) {
        this.showotraLugarAtencion = true;
      } else {
        this.showotraLugarAtencion = false;
        this.OBJETO.otro_lugar_atencion = "";
      }
    } else {
      this.showotraLugarAtencion = false;
      this.OBJETO.otro_lugar_atencion = "";
    }
  }
  validarNacionalidad(eveno) {
    if (eveno != undefined) {
      if (eveno.id_nacionalidad == 2) {
        this.showotraNacionalidad = true;
      } else {
        this.showotraNacionalidad = false;
        this.OBJETO.otra_nacionalidad = "";
      }
    } else {
      this.showotraNacionalidad = false;
      this.OBJETO.otra_nacionalidad = "";
    }
  }
  validarEmisionLicencia(eveno) {
    if (eveno) {
      this.showOtroEmisionLicencia = false;
      this.OBJETO.otro_emision_licencia = "";

    } else {
      this.showOtroEmisionLicencia = true;
    }

  }
  validarDosajeEtilicoCuantitativo(eveno) {
    if (eveno) {
      this.showDosajeCuantitativoNo = false;
      this.showDosajeCuantitativoSi = true;

    } else {
      this.showDosajeCuantitativoNo = true;
      this.showDosajeCuantitativoSi = false;
    }

  }
  validarDosajeEtilicoCualitativo(eveno) {
    if (eveno) {
      this.showDosajeCualitativo = true;

    } else {
      this.showDosajeCualitativo = false;
    }

  }
  validarLicencia(eveno) {
    if (eveno) {
      this.showLicencia = true;
      this.OBJETO.numero_licencia="";
      this.OBJETO.id_clase_categoria=0;
      this.OBJETO.esta_vigente=null;
      this.OBJETO.emision_licencia_peru=null;
      this.OBJETO.otro_emision_licencia="";
      this.OBJETO.id_organismo_emisor=0;
      this.OBJETO.otro_organismo_emisor="";
      this.OBJETO.aparece_snc=null;
    } else {
      this.showLicencia = false;
      this.OBJETO.numero_licencia="";
      this.OBJETO.id_clase_categoria=0;
      this.OBJETO.esta_vigente=null;
      this.OBJETO.emision_licencia_peru=null;
      this.OBJETO.otro_emision_licencia="";
      this.OBJETO.id_organismo_emisor=0;
      this.OBJETO.otro_organismo_emisor="";
      this.OBJETO.aparece_snc=null;
    }

  }
  validarOrganismoEmisor(eveno) {
    if (eveno != undefined) {
      if (eveno.id_organismo_emisor == 5) {
        this.showOtroOrganismoEmisor = true;
      } else {
        this.showOtroOrganismoEmisor = false;
        this.OBJETO.otro_organismo_emisor = "";
      }
    } else {
      this.showOtroOrganismoEmisor = false;
      this.OBJETO.otro_organismo_emisor = "";
    }
  }
  validarLugarDosaje(eveno) {
    if (eveno != undefined) {
      if (eveno.id_lugar_dosaje == 4) {
        this.showOtroLugarDosaje = true;
      } else {
        this.showOtroLugarDosaje = false;
        this.OBJETO.otro_lugar_dosaje = "";
      }
    } else {
      this.showOtroLugarDosaje = false;
      this.OBJETO.otro_lugar_dosaje = "";
    }
  }
  validarRazon(eveno) {
    console.log(eveno);
    if (eveno != undefined) {
      if (eveno == 2) {
        this.showOtroRazonDosaje = true;
      } else {
        this.showOtroRazonDosaje = false;
        this.OBJETO.otro_razon_dosaje_cuantitativo = "";
      }
    } else {
      this.showOtroRazonDosaje = false;
      this.OBJETO.otro_razon_dosaje_cuantitativo = "";
    }
  }


  Grabar() {

    if (this.Validar()) {
      //this.asignarValoreSituacionVia();

      if (this.OBJETO.id_persona_accidente_furat == 0) {
        //this.bMostrar = true;
        this.fs.personaFuratService.insertarPersonaAccidenteFurat(this.OBJETO).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            //this.bMostrar = false;
          });
      } else {
        this.fs.personaFuratService.modificarPersonaAccidenteFurat(this.OBJETO).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            //this.bMostrar = false;
          });
      }
    }
  }

  Validar(): boolean {
    let personaFurat = this.OBJETO;
    let personaFurat2 = this.OBJETO2;
    if (personaFurat2.id_tipo_persona_accidente == null || personaFurat2.id_tipo_persona_accidente == 0) {
      this.funciones.alertaSimple("info", "", "Por favor <br><b>Tipo de persona involucrada</b>", true, () => {
        this.selectTipoPersonaInvolucrada.focus();
      });
      return false;
    }
    if (personaFurat.id_estado_gravedad == null || personaFurat.id_estado_gravedad == 0) {
      this.funciones.alertaSimple("info", "", "Por favor <br><b>Tipo de Gravedad</b>", true, () => {
        this.selectTipoGravedad.focus();
      });
      return false;
    }
    if (personaFurat.id_lugar_atencion == null || personaFurat.id_lugar_atencion == 0) {

      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Lugar de Atención (Lesionado)</b>", true, () => {
        this.selectLugarAtencion.focus();
      });
      return false;
    }
    if (personaFurat.id_lugar_atencion ==3) {
      if (personaFurat.otro_lugar_atencion == null || personaFurat.otro_lugar_atencion == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro Lugar de Atención</b>", true, () => {
          document.getElementById("otro_lugar_atencion").focus();
        });
        return false;
      }
    }


    if (personaFurat.id_lugar_defuncion == null || personaFurat.id_lugar_defuncion == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Lugar de Defunción (Fallecido)</b>", true, () => {
        this.selectLugarDefuncion.focus();
      });
      return false;
    }
    /*if (personaFurat.otro_lugar_defuncion == null || personaFurat.otro_lugar_defuncion == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro Lugar de Defunción</b>", true, () => {
        document.getElementById("otro_lugar_defuncion").focus();
      });
      return false;
    }*/

    if (personaFurat.id_nacionalidad == null || personaFurat.id_nacionalidad == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Nacionalidad</b>", true, () => {
        this.selectTipoNacionalidad.focus();
      });
      return false;
    }
    if(personaFurat.id_nacionalidad==2){
      if (personaFurat.otra_nacionalidad == null || personaFurat.otra_nacionalidad == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otra Nacionalidad</b>", true, () => {
          document.getElementById("otra_nacionalidad").focus();
        });
        return false;
      }
    }
    if (personaFurat.emision_doc_peru == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>País Emisión DOC</b>", true, () => {
        document.getElementById("siPaisEmision").focus();
      });
      return false;
    }
    if(personaFurat.emision_doc_peru == false){
      if (personaFurat.otro_emision_doc == null || personaFurat.otro_emision_doc == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro País</b>", true, () => {
          document.getElementById("otro_emision_doc").focus();
        });
        return false;
      }
    }
    if (personaFurat.tiene_licencia == null) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Cuenta con Licencia de conducir?</b>", true, () => {
        document.getElementById("siLicencia").focus();
      });
      return false;
    }
    if (personaFurat.tiene_licencia) {
      if (personaFurat.numero_licencia == null || personaFurat.numero_licencia == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Número de Licencia</b>", true, () => {
          document.getElementById("numero_licencia").focus();
        });
        return false;
      }
      if (personaFurat.id_clase_categoria == null || personaFurat.id_clase_categoria == 0) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Clase y Categoria</b>", true, () => {
          this.selectTipoClaseCategoria.focus();
        });
        return false;
      }
      if (personaFurat.esta_vigente == null) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Esta Vigente?</b>", true, () => {
          document.getElementById("siVigente").focus();
        });
        return false;
      }

      if (personaFurat.emision_licencia_peru == null) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>País Emisión DOC</b>", true, () => {
          document.getElementById("siEmisionLicencia").focus();
        });
        return false;
      }
      if(personaFurat.emision_licencia_peru==false){
        if (personaFurat.otro_emision_licencia == null || personaFurat.otro_emision_licencia == "") {
          this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro País</b>", true, () => {
            document.getElementById("otro_emision_licencia").focus();
          });
          return false;
        }
      }
      if (personaFurat.id_organismo_emisor == null || personaFurat.id_organismo_emisor == 0) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Organismo Emisor</b>", true, () => {
          this.selectOrganismoEmisor.focus();
        });
        return false;
      }
      if(personaFurat.id_organismo_emisor==5){
        if (personaFurat.otro_organismo_emisor == null || personaFurat.otro_organismo_emisor == "") {
          this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro Organismo Emisor</b>", true, () => {
            document.getElementById("otro_organismo_emisor").focus();
          });
          return false;
        }
      }
      if (personaFurat.aparece_snc == null) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Aparece en el Sistema Nacional de Conductores?</b>", true, () => {
          document.getElementById("siAparece").focus();
        });
        return false;
      }
    }

    if (personaFurat.dosaje_cualitativo == null) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Se sometio a dosaje etilico cualitativo</b>", true, () => {
        document.getElementById("siDosajeCualitativo").focus();
      });
      return false;
    }
    if (personaFurat.dosaje_cualitativo) {
      if (personaFurat.hora_dosaje_cualitativo == null || personaFurat.hora_dosaje_cualitativo == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Hora de dosaje Cualitativo</b>", true, () => {
          document.getElementById("hora_dosaje_cualitativo").focus();
        });
        return false;
      }
      if (personaFurat.resultado_dosaje_cualitativo == null) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Resultado de dosaje etilico cualitativo</b>", true, () => {
          document.getElementById("siResultadoDosaje").focus();
        });
        return false;
      }
      if (personaFurat.id_lugar_dosaje == null || personaFurat.id_lugar_dosaje == 0) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Lugar de Dosaje</b>", true, () => {
          this.selectLugarDosaje.focus();
        });
        return false;
      }
      if(personaFurat.id_lugar_dosaje==4){
        if (personaFurat.otro_lugar_dosaje == null || personaFurat.otro_lugar_dosaje == "") {
          this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Nombre del Lugar de Dosaje</b>", true, () => {
            document.getElementById("otro_lugar_dosaje").focus();
          });
          return false;
        }
      }
    }
    if (personaFurat.dosaje_cuantitativo == null) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Se sometio a dosaje etilico cuantitativo</b>", true, () => {
        document.getElementById("siDosajeCuantitativo").focus();
      });
      return false;
    }
    if (personaFurat.dosaje_cuantitativo==false) {
      if (personaFurat.id_razon_dosaje_cuantitativo == null || personaFurat.id_razon_dosaje_cuantitativo == 0) {
        this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Razón por la que no se sometio al dosaje</b>", true, () => {
          this.selectRazonNoDosaje.focus();
        });
        return false;
      }
      if(personaFurat.id_razon_dosaje_cuantitativo==2){
        if (personaFurat.otro_razon_dosaje_cuantitativo == null || personaFurat.otro_razon_dosaje_cuantitativo == "") {
          this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro Razón por la que no se sometio al dosaje</b>", true, () => {
            document.getElementById("otro_razon_dosaje_cuantitativo").focus();
          });
          return false;
        }
      }
    }
    if (personaFurat.dosaje_cuantitativo) {
      if (personaFurat.hora_dosaje_cuantitativo == null || personaFurat.hora_dosaje_cuantitativo == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Hora de dosaje Cuantitativo</b>", true, () => {
          document.getElementById("hora_dosaje_cuantitativo").focus();
        });
        return false;
      }
      if (personaFurat.resultado_dosaje_cuantitativo == null || personaFurat.resultado_dosaje_cuantitativo == "") {
        this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>el Número de Resultado del desaje</b>", true, () => {
          document.getElementById("resultado_dosaje_cuantitativo").focus();
        });
        return false;
      }
    }

    /**/
    if (personaFurat2.id_tipo_documento == null || personaFurat2.id_tipo_documento == 0) {
      this.funciones.alertaSimple("info", "", "Por favor <br><b>Indicar el RUC o el DNI del propietario legal del vehículo</b>", true, () => {
        this.selectTipoDocumento.focus();
      });
      return false;
    }
    if (personaFurat2.id_tipo_sexo == null || personaFurat2.id_tipo_sexo == 0) {
      this.funciones.alertaSimple("info", "", "Por favor <br><b>Indicar el RUC o el DNI del propietario legal del vehículo</b>", true, () => {
        this.selectTipoSexo.focus();
      });
      return false;
    }
    if (personaFurat2.dni == null || personaFurat2.dni == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Indicar el documento de la persona involucrada</b>", true, () => {
        document.getElementById("dni").focus();
      });
      return false;
    }
    if (personaFurat2.id_situacion_pea_pas_ocu == null || personaFurat2.id_situacion_pea_pas_ocu == "") {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>la situación</b>", true, () => {
        this.selectSituacion.focus();
      });
      return false;
    }
    return true;
  }


  closeModal() {
    this.modalRef.hide();
  }


}
