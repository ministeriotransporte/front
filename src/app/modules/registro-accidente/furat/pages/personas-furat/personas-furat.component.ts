import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalPersonasFuratComponent } from './modales/modal-personas-furat/modal-personas-furat.component';

@Component({
  selector: 'app-personas-furat',
  templateUrl: './personas-furat.component.html',
  styleUrls: ['./personas-furat.component.scss']
})
export class PersonasFuratComponent implements OnInit {

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  listaPersonaFurat:any[];
  totalRegistros:number=0;

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarPersonaFurat(this.numPaginasMostrar, this.numero_Pagina);
  }

  listarPersonaFurat(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.personaFuratService.listarPersonaAccidenteFurat(param).subscribe(
      (data:any)=>{
        if(data){
          this.listaPersonaFurat=data.listapersonaaccidente;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaPersonaFurat=[]
        }

      }
    )
  }

  openModalVehiculoFurat(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosPersonaFurat:null,
        ACCION:1,
      }
    };
    this.bsModalRef = this.modalService.show(ModalPersonasFuratComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarPersonaFurat(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  openEditarPersonaFurat(personaFurat){
    let param={"id_accidente_transito":personaFurat.id_accidente_transito,
               "id_conductor_vehiculo":personaFurat.id_conductor_vehiculo,
               "id_ocupante_vehiculo":personaFurat.id_ocupante_vehiculo,
               "id_pasajero_vehiculo":personaFurat.id_pasajero_vehiculo,
               "id_peaton":personaFurat.id_peaton}
    this.fs.personaFuratService.obtenerPersonaAccidenteFurat(param).subscribe(

      (data:any)=>{

          if(data){
            this.config = {
              ignoreBackdropClick: true,
              keyboard: false,
              class: 'modal-registrar-conductor',
              initialState: {
                ACCION:2,
                datosPersonaFurat:data.listapersonaaccidente[0],
                id_peaton: personaFurat.id_peaton
              }
            };
            this.bsModalRef = this.modalService.show(ModalPersonasFuratComponent, this.config);
            this.bsModalRef.content.retornoValores.subscribe(
              data => {
                this.listarPersonaFurat(this.numPaginasMostrar, this.numero_Pagina);
              }
            );
          }
      }
    )

  }

}
