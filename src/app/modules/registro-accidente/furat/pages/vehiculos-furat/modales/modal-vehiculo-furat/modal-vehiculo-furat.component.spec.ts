import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVehiculoFuratComponent } from './modal-vehiculo-furat.component';

describe('ModalVehiculoFuratComponent', () => {
  let component: ModalVehiculoFuratComponent;
  let fixture: ComponentFixture<ModalVehiculoFuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalVehiculoFuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVehiculoFuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
