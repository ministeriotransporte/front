import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { NgSelectComponent } from '@ng-select/ng-select';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';

@Component({
  selector: 'app-modal-vehiculo-furat',
  templateUrl: './modal-vehiculo-furat.component.html',
  styleUrls: ['./modal-vehiculo-furat.component.scss']
})
export class ModalVehiculoFuratComponent implements OnInit {

  @ViewChild('situacionVehiculo', { static: false }) selectSituacionVehiculo: NgSelectComponent;
  @ViewChild('tipoDocumento', { static: false }) selectTipoDocumento: NgSelectComponent;

  @ViewChild('tipoModalidadTransporte', { static: false }) selectTipoModalidadTransporte: NgSelectComponent;



  @ViewChild('tipoPortaCertificadoSoat', { static: false }) selectPortaCertificadoSoat: NgSelectComponent;
  @ViewChild('tipoPoseeSoat', { static: false }) selectPoseeSoat: NgSelectComponent;
  @ViewChild('tipoVigenteSoat', { static: false }) selectVigenteSoat: NgSelectComponent;
  @ViewChild('tipoSeguro', { static: false }) selectTipoSeguro: NgSelectComponent;

  @ViewChild('tipoCertificado', { static: false }) selectTiCertificado: NgSelectComponent;
  @ViewChild('tipoPoseeCitv', { static: false }) selectPoseeCitv: NgSelectComponent;
  @ViewChild('tipoVigenteCitv', { static: false }) selectTipoVigenteCitv: NgSelectComponent;

  @ViewChild('tipoLugarImpacto', { static: false }) selectTipoLugarImpacto: NgSelectComponent;
  @ViewChild('tipoLugarImpactoMenores', { static: false }) selectTipoLugarImpactoMenores: NgSelectComponent;

  @ViewChild('tipoElementoTransportado', { static: false }) selectElementoTransportado: NgSelectComponent;
  @ViewChild('tipoClasificacionTransporte', { static: false }) selectTipoClasificacion: NgSelectComponent;
  @ViewChild('tipoAmbitoServicio', { static: false }) selectAmbitoServicio: NgSelectComponent;

  listaAmbitoServicio : any[];
  listaClaseVehiculo : any[];
  listaLugarImpacto : any[];
  listaLugarImpactoMenores : any[];
  listaModalidadTransporte : any[];
  listaPortaCitv : any[];
  listaPortaSoat : any[];
  listaPoseeCitv : any[];
  listaPoseeSoat : any[];
  listaSituacionVehiculo : any[];
  listaTipoSeguro : any[];
  listaVigenteCitv : any[];
  listaVigenteSoat : any[];
  listaElementoTransportado : any[];
  listaClasificacionTransporte : any[];
  listaTipoDocumento : any[];

  datosVehiculoFurat;
  ACCION: number = null;

  showOtroPais:boolean=false;
  showOtraModalidad:boolean=false;
  showotraClaseVehiculo:boolean=false;
  showRemolque:boolean=false;
  showOtroSeguro:boolean=false;
  showConsultarReniec:boolean=false;

  usuario;
  numCantidad:number=8;

  OBJETO=
  {
    "id_vehiculo_involucrado_furat":null,
    "id_vehiculo_involucrado":null,
    "cod_vehiculo_involucrado":null,
    "cod_vehiculo_involucrado_furat":null,

    "id_situacion_vehiculo":null,
    "placa_vehiculo_involucrado":null,
    "es_peruano":null,
    "otro_pais":null,
    "numero_pas_ocupante":null,
    "id_tipo_documento":null,
    "numero_documento":null,
    "nombre_propietario":null,
    "ap_paterno_propietario":null,
    "ap_materno_propietario":null,

    "id_tipo_vehiculo":null,
    "otro_tipo_vehiculo":null,
    "placa_remolque":null,
    "placa_remolque2":null,

    "id_modalidad_transporte":null,
    "otro_modalidad_transporte":null,
    "id_elemento_transportado":null,
    "id_clasificacion_transporte":null,

    "ruc_empresa":null,
    "razon_social":null,
    "id_ambito_servicio":null,

    "id_porta_soat":null,
    "id_posee_soat":null,
    "id_vigente_soat":null,
    "id_tipo_seguro":null,
    "otro_tipo_seguro":null,

    "id_porta_citv":null,
    "id_posee_citv":null,
    "id_vigente_citv":null,
    "observaciones_citv":null,

    "id_lugar_impacto":null,
    "id_lugar_impacto_menores":null,
    "descripcion_danios":null,
    "observaciones":null,



  }



  @Output() retornoValores = new EventEmitter();
  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.OBJETO.id_vehiculo_involucrado=this.datosVehiculoFurat.id_vehiculo_involucrado
    this.listarConductorVehiculoControl();

    if(this.ACCION==2){
      this.setear();
    }
  }

  listarConductorVehiculoControl(){
    this.fs.vehiculoFuratService.listarVehiculoFuratControl().subscribe(
      (data:any)=>{
        this.listaAmbitoServicio=data.ambitoservicio;
        this.listaClaseVehiculo=data.clasevehiculo;
        this.listaLugarImpacto=data.lugarimpacto;
        this.listaLugarImpactoMenores=data.lugarimpactomenores;
        this.listaModalidadTransporte=data.modalidadtransporte;
        this.listaPortaCitv=data.portacitv;
        this.listaPortaSoat=data.portasoat;
        this.listaPoseeCitv=data.poseecitv;
        this.listaPoseeSoat=data.poseesoat;
        this.listaSituacionVehiculo=data.situacionvehiculo;
        this.listaTipoSeguro=data.tiposeguro;
        this.listaVigenteCitv=data.vigentecitv;
        this.listaVigenteSoat=data.vigentesoat;
        this.listaElementoTransportado =data.elementotransportado;
        this.listaClasificacionTransporte = data.clasificaciontransporte;
        this.listaTipoDocumento = data.tipodocumento;
      }
    )
  }

  setear(){
    this.OBJETO.id_vehiculo_involucrado_furat = this.datosVehiculoFurat.id_vehiculo_involucrado_furat == null ? 0 : this.datosVehiculoFurat.id_vehiculo_involucrado_furat;
    this.OBJETO.id_vehiculo_involucrado = this.datosVehiculoFurat.id_vehiculo_involucrado == null ? null : this.datosVehiculoFurat.id_vehiculo_involucrado;
    this.OBJETO.cod_vehiculo_involucrado = this.datosVehiculoFurat.cod_vehiculo_involucrado == null ? null : this.datosVehiculoFurat.cod_vehiculo_involucrado;
    this.OBJETO.cod_vehiculo_involucrado_furat = this.datosVehiculoFurat.cod_vehiculo_involucrado_furat == null ? null : this.datosVehiculoFurat.cod_vehiculo_involucrado_furat;

    this.OBJETO.id_situacion_vehiculo = this.datosVehiculoFurat.id_situacion_vehiculo == null ? null : this.datosVehiculoFurat.id_situacion_vehiculo;
    this.OBJETO.placa_vehiculo_involucrado = this.datosVehiculoFurat.placa_vehiculo_involucrado == null ? "" : this.datosVehiculoFurat.placa_vehiculo_involucrado;
    this.OBJETO.es_peruano = this.datosVehiculoFurat.es_peruano == null ? null : this.datosVehiculoFurat.es_peruano;
    this.OBJETO.otro_pais = this.datosVehiculoFurat.otro_pais == null ? "" : this.datosVehiculoFurat.otro_pais;
    this.OBJETO.numero_pas_ocupante = this.datosVehiculoFurat.numero_pas_ocupante == null ? null : this.datosVehiculoFurat.numero_pas_ocupante;
    this.OBJETO.id_tipo_documento = this.datosVehiculoFurat.id_tipo_documento == null ? null : this.datosVehiculoFurat.id_tipo_documento;
    this.OBJETO.numero_documento=this.datosVehiculoFurat.numero_documento == null ? "" : this.datosVehiculoFurat.numero_documento;
    this.OBJETO.nombre_propietario=this.datosVehiculoFurat.nombre_propietario == null ? "" : this.datosVehiculoFurat.nombre_propietario;
    this.OBJETO.ap_paterno_propietario=this.datosVehiculoFurat.ap_paterno_propietario == null ? "" : this.datosVehiculoFurat.ap_paterno_propietario;
    this.OBJETO.ap_materno_propietario=this.datosVehiculoFurat.ap_materno_propietario == null ? "" : this.datosVehiculoFurat.ap_materno_propietario;

    this.OBJETO.id_tipo_vehiculo=this.datosVehiculoFurat.id_tipo_vehiculo == null ? null : this.datosVehiculoFurat.id_tipo_vehiculo;
    this.OBJETO.otro_tipo_vehiculo=this.datosVehiculoFurat.otro_tipo_vehiculo == null ? null : this.datosVehiculoFurat.otro_tipo_vehiculo;
    this.OBJETO.placa_remolque=this.datosVehiculoFurat.placa_remolque == null ? null : this.datosVehiculoFurat.placa_remolque;
    this.OBJETO.placa_remolque2=this.datosVehiculoFurat.placa_remolque2 == null ? null : this.datosVehiculoFurat.placa_remolque2;

    this.OBJETO.id_modalidad_transporte=this.datosVehiculoFurat.id_modalidad_transporte == null ? null : this.datosVehiculoFurat.id_modalidad_transporte;
    this.OBJETO.otro_modalidad_transporte=this.datosVehiculoFurat.otro_modalidad_transporte == null ? "" : this.datosVehiculoFurat.otro_modalidad_transporte;
    this.OBJETO.ap_materno_propietario=this.datosVehiculoFurat.ap_materno_propietario == null ? "" : this.datosVehiculoFurat.ap_materno_propietario;
    this.OBJETO.id_elemento_transportado=this.datosVehiculoFurat.id_elemento_transportado == null ? null : this.datosVehiculoFurat.id_elemento_transportado;
    this.OBJETO.id_clasificacion_transporte=this.datosVehiculoFurat.id_clasificacion_transporte == null ? null : this.datosVehiculoFurat.id_clasificacion_transporte;
    this.OBJETO.ruc_empresa=this.datosVehiculoFurat.ruc_empresa == null ? null : this.datosVehiculoFurat.ruc_empresa;
    this.OBJETO.razon_social=this.datosVehiculoFurat.razon_social == null ? "" : this.datosVehiculoFurat.razon_social;
    this.OBJETO.id_ambito_servicio=this.datosVehiculoFurat.id_ambito_servicio == null ? null : this.datosVehiculoFurat.id_ambito_servicio;

    this.OBJETO.id_porta_soat=this.datosVehiculoFurat.id_porta_soat == null ? null : this.datosVehiculoFurat.id_porta_soat;
    this.OBJETO.id_posee_soat=this.datosVehiculoFurat.id_posee_soat == null ? null : this.datosVehiculoFurat.id_posee_soat;
    this.OBJETO.id_vigente_soat=this.datosVehiculoFurat.id_vigente_soat == null ? null : this.datosVehiculoFurat.id_vigente_soat;
    this.OBJETO.id_tipo_seguro=this.datosVehiculoFurat.id_tipo_seguro == null ? null : this.datosVehiculoFurat.id_tipo_seguro;
    this.OBJETO.otro_tipo_seguro=this.datosVehiculoFurat.otro_tipo_seguro == null ? "" : this.datosVehiculoFurat.otro_tipo_seguro;

    this.OBJETO.id_porta_citv=this.datosVehiculoFurat.id_porta_citv == null ? null : this.datosVehiculoFurat.id_porta_citv;
    this.OBJETO.id_posee_citv=this.datosVehiculoFurat.id_posee_citv == null ? null : this.datosVehiculoFurat.id_posee_citv;
    this.OBJETO.id_vigente_citv=this.datosVehiculoFurat.id_vigente_citv == null ? null : this.datosVehiculoFurat.id_vigente_citv;
    this.OBJETO.observaciones_citv=this.datosVehiculoFurat.observaciones_citv == null ? "" : this.datosVehiculoFurat.observaciones_citv;

    this.OBJETO.id_lugar_impacto=this.datosVehiculoFurat.id_lugar_impacto == null ? null : this.datosVehiculoFurat.id_lugar_impacto;
    this.OBJETO.id_lugar_impacto_menores=this.datosVehiculoFurat.id_lugar_impacto_menores == null ? null : this.datosVehiculoFurat.id_lugar_impacto_menores;
    this.OBJETO.descripcion_danios=this.datosVehiculoFurat.descripcion_danios == null ? "" : this.datosVehiculoFurat.descripcion_danios;
    this.OBJETO.observaciones=this.datosVehiculoFurat.observaciones == null ? "" : this.datosVehiculoFurat.observaciones;

    this.showotraClaseVehiculo = this.datosVehiculoFurat.id_tipo_vehiculo == 21 ? true : false;
    this.showRemolque = this.datosVehiculoFurat.id_tipo_vehiculo == 10 ? true : false;
    this.showOtraModalidad = this.datosVehiculoFurat.id_modalidad_transporte==16 ? true :false;
    this.numCantidad=this.datosVehiculoFurat.id_tipo_documento==1 ? 8 : 11;
    this.showConsultarReniec =this.datosVehiculoFurat.id_tipo_documento==1 ? true : false;
    this.showOtroSeguro = this.datosVehiculoFurat.id_tipo_seguro==3 ? true : false;







  }

  envento(event){
      if(event){
        this.showOtroPais=false;
        this.OBJETO.otro_pais=null;

      }else{
        this.showOtroPais=true;
      }
  }


  validarDocumento(evento){
    this.OBJETO.numero_documento=null;
    this.OBJETO.nombre_propietario=null;
    this.OBJETO.ap_paterno_propietario=null;
    this.OBJETO.ap_materno_propietario=null;
    if(evento!=undefined){
      if(evento.id_tipo_documento==5){
        this.showConsultarReniec=false;
        this.numCantidad=11;
      }else{
        this.showConsultarReniec=true;
        this.numCantidad=8;
      }
    }else{
      this.showConsultarReniec=true;
      this.numCantidad=8;
    }
  }

  validarTipoSeguro(eveno){
    if(eveno!=undefined){
      if(eveno.id_tipo_seguro==3){
        this.showOtroSeguro=true;
      }else{
        this.showOtroSeguro=false;
        this.OBJETO.otro_tipo_seguro="";
      }
    }else{
      this.showOtroSeguro=false;
      this.OBJETO.otro_tipo_seguro="";
    }
  }

  validarInformacionReniec() {
    let valDni = this.OBJETO.numero_documento;
    let cant = 0;
    if (valDni == '' || valDni == null) {
      document.getElementById('numero_documento').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de Dni a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionReniec(valDni).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.OBJETO.nombre_propietario=response.strnombres;
              this.OBJETO.ap_paterno_propietario=response.strapellidopaterno;
              this.OBJETO.ap_materno_propietario=response.strapellidomaterno;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
              this.OBJETO.nombre_propietario=null;
              this.OBJETO.ap_paterno_propietario=null;
              this.OBJETO.ap_materno_propietario=null;
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
            this.OBJETO.nombre_propietario=null;
            this.OBJETO.ap_paterno_propietario=null;
            this.OBJETO.ap_materno_propietario=null;
          }
        );
      } else {
        this.funciones.mensaje('info', 'El DNI ingresado ya se encuentra registrado en el sistema.');
      }
    }
  }

  validarInformacionSunat(){
    let valRUC = this.OBJETO.ruc_empresa;
    let cant = 0;
    if (valRUC == '' || valRUC == null) {
      document.getElementById('ruc_empresa').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de RUC a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionSunat(valRUC).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.OBJETO.razon_social=response.ddp_nombre;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del RUC ingresado.');
              this.OBJETO.razon_social=null;
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del ruc ingresado.');
            this.OBJETO.razon_social=null;
          }
        );
      } else {
        this.funciones.mensaje('info', 'No se encontró información del ruc ingresado.');
      }
    }
  }


  Grabar(){
    if (this.Validar()) {
      //this.asignarValoreSituacionVia();
      let param={
        "id_vehiculo_involucrado_furat":this.OBJETO.id_vehiculo_involucrado_furat,
        "id_vehiculo_involucrado":this.OBJETO.id_vehiculo_involucrado,
        "cod_vehiculo_involucrado":this.OBJETO.cod_vehiculo_involucrado,
        "cod_vehiculo_involucrado_furat":this.OBJETO.cod_vehiculo_involucrado_furat,
        "es_peruano": Boolean(this.OBJETO.es_peruano),
        "otro_pais":this.OBJETO.otro_pais,
        "numero_pas_ocupante":Number(this.OBJETO.numero_pas_ocupante),
        "id_tipo_documento":this.OBJETO.id_tipo_documento,
        "numero_documento":this.OBJETO.numero_documento,
        "nombre_propietario":this.OBJETO.nombre_propietario,
        "ap_paterno_propietario":this.OBJETO.ap_paterno_propietario,
        "ap_materno_propietario":this.OBJETO.ap_materno_propietario,
        "id_elemento_transportado":this.OBJETO.id_elemento_transportado,
        "id_clasificacion_transporte":this.OBJETO.id_clasificacion_transporte,
        "ruc_empresa":this.OBJETO.ruc_empresa,
        "razon_social":this.OBJETO.razon_social,
        "id_ambito_servicio":this.OBJETO.id_ambito_servicio,
        "id_porta_soat":this.OBJETO.id_porta_soat,
        "id_posee_soat":this.OBJETO.id_posee_soat,
        "id_vigente_soat":this.OBJETO.id_vigente_soat,
        "id_tipo_seguro":this.OBJETO.id_tipo_seguro,
        "otro_tipo_seguro":this.OBJETO.otro_tipo_seguro,
        "id_porta_citv":this.OBJETO.id_porta_citv,
        "id_posee_citv":this.OBJETO.id_posee_citv,
        "id_vigente_citv":this.OBJETO.id_vigente_citv,
        "observaciones_citv":this.OBJETO.observaciones_citv,
        "id_lugar_impacto_menores":this.OBJETO.id_lugar_impacto_menores,
        "id_lugar_impacto":this.OBJETO.id_lugar_impacto,
        "descripcion_danios":this.OBJETO.descripcion_danios,
        "observaciones":this.OBJETO.observaciones,
        "activo":true,
        "usuario_creacion":this.usuario,
        "usuario_modificacion":this.usuario,
      }
      if (this.datosVehiculoFurat.id_vehiculo_involucrado_furat == null) {
        //this.bMostrar = true;
        this.fs.vehiculoFuratService.insertarVehiculoInvolucradoFurat(param).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            //this.bMostrar = false;
          });
      } else {
        this.fs.vehiculoFuratService.modificarVehiculoInvolucradoFurat(param).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            //this.bMostrar = false;
          });
      }
    }
  }

  Validar(): boolean {
    let vehiculoFurat = this.OBJETO;
    console.log("vehiculoFurat a registrar",vehiculoFurat);
    if (vehiculoFurat.es_peruano == null) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>País de registro del vehículo</b>", true, () => {
        document.getElementById("siPeruano").focus();
      });
      return false;
    }
    if (vehiculoFurat.es_peruano == false && (vehiculoFurat.otro_pais=="" || vehiculoFurat.otro_pais==null)) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese<br><b>Otro País</b>", true, () => {
        document.getElementById("otro_pais").focus();
      });
      return false;
    }
    if (vehiculoFurat.numero_pas_ocupante == null || vehiculoFurat.numero_pas_ocupante == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Cantidad de Pasajeros y Ocupantes</b>", true, () => {
        document.getElementById("numero_pas_ocupante").focus();
      });
      return false;
    }
    if (vehiculoFurat.id_tipo_documento == null || vehiculoFurat.id_tipo_documento == 0) {
      this.funciones.alertaSimple("info", "", "Por favor <br><b>Indicar el RUC o el DNI del propietario legal del vehículo</b>", true, () => {
        this.selectTipoDocumento.focus();
      });
      return false;
    }
    if (vehiculoFurat.numero_documento == null || vehiculoFurat.numero_documento == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Número de Documento</b>", true, () => {
        document.getElementById("numero_documento").focus();
      });
      return false;
    }
    if (vehiculoFurat.nombre_propietario == null || vehiculoFurat.nombre_propietario == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Nombre del propietario legal</b>", true, () => {
        document.getElementById("nombre_propietario").focus();
      });
      return false;
    }
    if (vehiculoFurat.ap_paterno_propietario == null || vehiculoFurat.ap_paterno_propietario == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Apellido Paterno del propietario legal</b>", true, () => {
        document.getElementById("ap_paterno_propietario").focus();
      });
      return false;
    }
    if (vehiculoFurat.ap_materno_propietario == null || vehiculoFurat.ap_materno_propietario == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Apellido Materno del propietario legal</b>", true, () => {
        document.getElementById("ap_materno_propietario").focus();
      });
      return false;
    }
    if (vehiculoFurat.id_modalidad_transporte == null || vehiculoFurat.id_modalidad_transporte == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Modalidad de Transporte</b>", true, () => {
        this.selectTipoModalidadTransporte.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_elemento_transportado == null || vehiculoFurat.id_elemento_transportado == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Elemento Transportado</b>", true, () => {
        this.selectElementoTransportado.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_clasificacion_transporte == null || vehiculoFurat.id_clasificacion_transporte == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Clasificación del Transporte</b>", true, () => {
        this.selectTipoClasificacion.focus();
      });
      return false;
    }
    if (vehiculoFurat.ruc_empresa == null || vehiculoFurat.ruc_empresa == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>RUC</b>", true, () => {
        document.getElementById("ruc_empresa").focus();
      });
      return false;
    }
    if (vehiculoFurat.razon_social == null || vehiculoFurat.razon_social == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Razón Social</b>", true, () => {
        document.getElementById("razon_social").focus();
      });
      return false;
    }
    if (vehiculoFurat.id_ambito_servicio == null || vehiculoFurat.id_ambito_servicio == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Ámbito de servicio</b>", true, () => {
        this.selectAmbitoServicio.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_porta_soat == null || vehiculoFurat.id_porta_soat == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Porta el certificado contra accidentes?</b>", true, () => {
        this.selectPortaCertificadoSoat.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_posee_soat == null || vehiculoFurat.id_posee_soat == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Posee certificado contra accidentes ?</b>", true, () => {
        this.selectPoseeSoat.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_vigente_soat == null || vehiculoFurat.id_vigente_soat == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿ Está Vigente certificado contra accidentes ?</b>", true, () => {
        this.selectVigenteSoat.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_tipo_seguro == null || vehiculoFurat.id_tipo_seguro == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Tipo de Seguro</b>", true, () => {
        this.selectTipoSeguro.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_tipo_seguro == 3 && (vehiculoFurat.otro_tipo_seguro == null || vehiculoFurat.otro_tipo_seguro == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otro Tipo de Seguro</b>", true, () => {
        document.getElementById("otro_tipo_seguro").focus();
      });
      return false;
    }
    if (vehiculoFurat.id_porta_citv == null || vehiculoFurat.id_porta_citv == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Porta el certificado de inspección vehicular?</b>", true, () => {
        this.selectTiCertificado.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_posee_citv == null || vehiculoFurat.id_posee_citv == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿Posee certificado de inspección vehicular?</b>", true, () => {
        this.selectPoseeCitv.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_vigente_citv == null || vehiculoFurat.id_vigente_citv == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>¿ Está Vigente certificado de inspección vehicular?</b>", true, () => {
        this.selectTipoVigenteCitv.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_lugar_impacto == null || vehiculoFurat.id_lugar_impacto == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Lugar de Impacto</b>", true, () => {
        this.selectTipoLugarImpacto.focus();
      });
      return false;
    }
    if (vehiculoFurat.id_lugar_impacto_menores == null || vehiculoFurat.id_lugar_impacto_menores == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Lugar de Impacto Menores</b>", true, () => {
        this.selectTipoLugarImpactoMenores.focus();
      });
      return false;
    }



    return true;
  }

  closeModal(){
    this.modalRef.hide();
  }

}
