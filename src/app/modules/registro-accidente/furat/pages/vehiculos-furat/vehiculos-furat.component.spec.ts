import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculosFuratComponent } from './vehiculos-furat.component';

describe('VehiculosFuratComponent', () => {
  let component: VehiculosFuratComponent;
  let fixture: ComponentFixture<VehiculosFuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiculosFuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculosFuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
