import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalVehiculoFuratComponent } from './modales/modal-vehiculo-furat/modal-vehiculo-furat.component';

@Component({
  selector: 'app-vehiculos-furat',
  templateUrl: './vehiculos-furat.component.html',
  styleUrls: ['./vehiculos-furat.component.scss']
})
export class VehiculosFuratComponent implements OnInit {
  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  listaVehiculoFurat:any[];
  totalRegistros:number=0;

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarVehiculoInvolucradoFurat(this.numPaginasMostrar, this.numero_Pagina);
  }

  listarVehiculoInvolucradoFurat(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.vehiculoFuratService.listarVehiculoInvolucradoFurat(param).subscribe(
      (data:any)=>{
        if(data){
          this.listaVehiculoFurat=data.listavehiculosinvolucrados;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaVehiculoFurat=[]
        }
        
      }
    )
  }

  openModalVehiculoFurat(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosVehiculoFurat:null,
        ACCION:1,
      }
    };
    this.bsModalRef = this.modalService.show(ModalVehiculoFuratComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarVehiculoInvolucradoFurat(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  openEditarVehiculoFurat(vehiculoFurat){
    console.log("vehiculo",vehiculoFurat);
    let param={"id_accidente_transito":vehiculoFurat.id_accidente_transito,"id_vehiculo_involucrado":vehiculoFurat.id_vehiculo_involucrado}
    this.fs.vehiculoFuratService.obtenerVehiculoInvolucradoFurat(param).subscribe(
      (data:any)=>{
          if(data){
            this.config = {
              ignoreBackdropClick: true,
              keyboard: false,
              class: 'modal-registrar-conductor',
              initialState: {
                ACCION:2,
                datosVehiculoFurat:data.listavehiculoinvolucrado[0],
              }
            };
            this.bsModalRef = this.modalService.show(ModalVehiculoFuratComponent, this.config);
            this.bsModalRef.content.retornoValores.subscribe(
              data => {
                this.listarVehiculoInvolucradoFurat(this.numPaginasMostrar, this.numero_Pagina);
              }
            );
          }
      }
    )
    
  }

}
