import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuratComponent } from './furat.component';

describe('FuratComponent', () => {
  let component: FuratComponent;
  let fixture: ComponentFixture<FuratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
