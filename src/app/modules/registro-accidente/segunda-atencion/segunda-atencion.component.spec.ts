import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SegundaAtencionComponent } from './segunda-atencion.component';

describe('SegundaAtencionComponent', () => {
  let component: SegundaAtencionComponent;
  let fixture: ComponentFixture<SegundaAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegundaAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegundaAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
