import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { isNullOrUndefined } from 'util';
import { SegundaAtencion } from 'src/app/modules/shared/models/accidente/segundaAtencion';

@Component({
  selector: 'app-modal-atencion-secundaria',
  templateUrl: './modal-atencion-secundaria.component.html',
  styleUrls: ['./modal-atencion-secundaria.component.scss']
})
export class ModalAtencionSecundariaComponent implements OnInit {
  @ViewChild('ddlTipoIpress', { static: false }) selectTipoIpress: NgSelectComponent; 
  @ViewChild('ddlTipoCompania', { static: false }) selectTipoCompania: NgSelectComponent; 

  @Output() retornoValores = new EventEmitter();

  listaIpress : any[];
  listaCompania : any[];

  modelRegistroSegundaAtencion: SegundaAtencion;

  datosSegundaAtencionInvolucrado;
  usuario;
  ACCION: number = null;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  vTipoSOAT: boolean = false;
  ShowOtroCompaniaSeguro : boolean=false;

  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private modalService: BsModalService,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarAtencionSecundariaControl();
    this.crearFormGroup();
  }

  listarAtencionSecundariaControl(){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.segundaAtencionService.listarAtencionSecundariaControl(param).subscribe(
      (data:any)=>{
          this.listaIpress=data.ipress;
          this.listaCompania=data.companiaseguro;

      }
    )
  }

  crearFormGroup(){
    if(this.datosSegundaAtencionInvolucrado==null){
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_atencion_secundaria:null,
        id_ipress:null,
        cantidad_heridos_ingresados:null,
        soat_activo:null,
        id_compania_seguro:null,
        otro_compania_seguro:null,
        cantidad_dados_alta:null,
        cantidad_fallecido_intrahospitalario:null,
        cantidad_derivado_rehabilitacion:null
      })
    }else{
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_atencion_secundaria: this.datosSegundaAtencionInvolucrado.id_atencion_secundaria == null ? 0 : this.datosSegundaAtencionInvolucrado.id_atencion_secundaria,
        id_ipress: this.datosSegundaAtencionInvolucrado.id_ipress == null ? 0 : this.datosSegundaAtencionInvolucrado.id_ipress,
        cantidad_heridos_ingresados : this.datosSegundaAtencionInvolucrado.cantidad_heridos_ingresados == null ? 0 : this.datosSegundaAtencionInvolucrado.cantidad_heridos_ingresados,
        soat_activo : this.datosSegundaAtencionInvolucrado.soat_activo == null ? false : this.datosSegundaAtencionInvolucrado.soat_activo,
        id_compania_seguro: this.datosSegundaAtencionInvolucrado.id_compania_seguro == null ? 0 : this.datosSegundaAtencionInvolucrado.id_compania_seguro,
        otro_compania_seguro: this.datosSegundaAtencionInvolucrado.otro_compania_seguro == null ? "" : this.datosSegundaAtencionInvolucrado.otro_compania_seguro,
        cantidad_dados_alta : this.datosSegundaAtencionInvolucrado.cantidad_dados_alta == null ? 0 : this.datosSegundaAtencionInvolucrado.cantidad_dados_alta,
        cantidad_fallecido_intrahospitalario : this.datosSegundaAtencionInvolucrado.cantidad_fallecido_intrahospitalario == null ? 0 : this.datosSegundaAtencionInvolucrado.cantidad_fallecido_intrahospitalario,
        cantidad_derivado_rehabilitacion : this.datosSegundaAtencionInvolucrado.cantidad_derivado_rehabilitacion == null ? 0 : this.datosSegundaAtencionInvolucrado.cantidad_derivado_rehabilitacion,

      })
      if(this.datosSegundaAtencionInvolucrado.soat_activo){
        this.vTipoSOAT=true;
      }else{
        this.vTipoSOAT=false;
      }
      if(this.datosSegundaAtencionInvolucrado.id_compania_seguro==7){
        this.ShowOtroCompaniaSeguro=true;
      }else{
        this.ShowOtroCompaniaSeguro=false;
      }

    }

    
  }

  envento(evento){

  }

  validarOtroTipoCompania(evento){
    if(evento!=undefined){
      if(evento.id_compania_seguro==7){
        this.ShowOtroCompaniaSeguro=true;
      }else{
        this.ShowOtroCompaniaSeguro=false;
        this.formGroup.patchValue({
          otro_compania_seguro:"",
        });
      }
    }
  }


  registrarSegundaAtencion(){
    if (this.Validar()) {
      this.asignarValoresSegundaAtencion();
      if (this.datosSegundaAtencionInvolucrado == null) {
        this.bMostrar = true;
        this.fs.segundaAtencionService.insertarAtencionSecundaria(this.modelRegistroSegundaAtencion).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.segundaAtencionService.modificarAtencionSecundaria(this.modelRegistroSegundaAtencion).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }
  }

  Validar(){
    let segundaAtencion = Object.assign({}, this.formGroup.value);
    if (segundaAtencion.id_ipress == null || segundaAtencion.id_ipress == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>IPRESS (Hospitales, Clinicas y otros)</b>", true, () => {
        this.selectTipoIpress.focus();
      });
      return false;
    }
    if (segundaAtencion.cantidad_heridos_ingresados == null || segundaAtencion.cantidad_heridos_ingresados == 0) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese los <br><b>Heridos Ingresados</b>", true, () => {
        document.getElementById("cantidad_heridos_ingresados").focus();
      });
      return false;
    }
    if (segundaAtencion.id_compania_seguro == null || segundaAtencion.id_compania_seguro == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Compañia de Seguro</b>", true, () => {
        this.selectTipoCompania.focus();
      });
      return false;
    }
    if (segundaAtencion.id_compania_seguro == 7 && (segundaAtencion.otro_compania_seguro == null || segundaAtencion.otro_compania_seguro == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>La descripción de la otra compañía de seguro</b>", true, () => {
        document.getElementById("otro_compania_seguro").focus();
      });
      return false;
    }
    if (segundaAtencion.cantidad_dados_alta == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la cantidad de  <br><b>Dados de alta</b>", true, () => {
        document.getElementById("cantidad_dados_alta").focus();
      });
      return false;
    }
    if (segundaAtencion.cantidad_fallecido_intrahospitalario == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la cantidad de  <br><b>Fallecidos Intrahospitalarios</b>", true, () => {
        document.getElementById("cantidad_fallecido_intrahospitalario").focus();
      });
      return false;
    }
    if (segundaAtencion.cantidad_derivado_rehabilitacion == null) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la cantidad de  <br><b>Derivados a Rehabilitación</b>", true, () => {
        document.getElementById("cantidad_derivado_rehabilitacion").focus();
      });
      return false;
    }
    return true
  }

  asignarValoresSegundaAtencion(){
    let modelRegistroSegundaAtencionEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroSegundaAtencion = new SegundaAtencion();

    this.modelRegistroSegundaAtencion.id_accidente_transito = modelRegistroSegundaAtencionEnvio.id_accidente_transito == null ? 0 : modelRegistroSegundaAtencionEnvio.id_accidente_transito;
    this.modelRegistroSegundaAtencion.id_atencion_secundaria = modelRegistroSegundaAtencionEnvio.id_atencion_secundaria == null ? 0 : modelRegistroSegundaAtencionEnvio.id_atencion_secundaria;
    this.modelRegistroSegundaAtencion.id_ipress = modelRegistroSegundaAtencionEnvio.id_ipress == null ? 0 : modelRegistroSegundaAtencionEnvio.id_ipress;
    this.modelRegistroSegundaAtencion.cantidad_heridos_ingresados = modelRegistroSegundaAtencionEnvio.cantidad_heridos_ingresados == null ? 0 : modelRegistroSegundaAtencionEnvio.cantidad_heridos_ingresados;
    this.modelRegistroSegundaAtencion.soat_activo = modelRegistroSegundaAtencionEnvio.soat_activo == null ? false : modelRegistroSegundaAtencionEnvio.soat_activo;
    this.modelRegistroSegundaAtencion.id_compania_seguro = modelRegistroSegundaAtencionEnvio.id_compania_seguro == null ? 0 : modelRegistroSegundaAtencionEnvio.id_compania_seguro;
    this.modelRegistroSegundaAtencion.cantidad_dados_alta = modelRegistroSegundaAtencionEnvio.cantidad_dados_alta == null ? 0 : modelRegistroSegundaAtencionEnvio.cantidad_dados_alta;
    this.modelRegistroSegundaAtencion.otro_compania_seguro = modelRegistroSegundaAtencionEnvio.otro_compania_seguro == null ? "" : modelRegistroSegundaAtencionEnvio.otro_compania_seguro;
    this.modelRegistroSegundaAtencion.cantidad_fallecido_intrahospitalario = modelRegistroSegundaAtencionEnvio.cantidad_fallecido_intrahospitalario == null ? 0 : modelRegistroSegundaAtencionEnvio.cantidad_fallecido_intrahospitalario;
    this.modelRegistroSegundaAtencion.cantidad_derivado_rehabilitacion = modelRegistroSegundaAtencionEnvio.cantidad_derivado_rehabilitacion == null ? 0 : modelRegistroSegundaAtencionEnvio.cantidad_derivado_rehabilitacion;
    this.modelRegistroSegundaAtencion.usuario_creacion = this.usuario;
    this.modelRegistroSegundaAtencion.usuario_modificacion = this.usuario;
  }


  closeModal(){
    this.modalRef.hide();
  }

}
