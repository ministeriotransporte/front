import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAtencionSecundariaComponent } from './modal-atencion-secundaria.component';

describe('ModalAtencionSecundariaComponent', () => {
  let component: ModalAtencionSecundariaComponent;
  let fixture: ComponentFixture<ModalAtencionSecundariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAtencionSecundariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAtencionSecundariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
