import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtencionSecundariaComponent } from './atencion-secundaria.component';

describe('AtencionSecundariaComponent', () => {
  let component: AtencionSecundariaComponent;
  let fixture: ComponentFixture<AtencionSecundariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtencionSecundariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtencionSecundariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
