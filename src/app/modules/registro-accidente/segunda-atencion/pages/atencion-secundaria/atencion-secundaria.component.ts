import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalAtencionSecundariaComponent } from './modal-atencion-secundaria/modal-atencion-secundaria.component';

@Component({
  selector: 'app-atencion-secundaria',
  templateUrl: './atencion-secundaria.component.html',
  styleUrls: ['./atencion-secundaria.component.scss']
})
export class AtencionSecundariaComponent implements OnInit {
  listaSegundaAtencion : any[];
  totalRegistros:number=0;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  config;
  bsModalRef: BsModalRef;
  usuario;

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarAtencionSecundaria(this.numPaginasMostrar,this.numero_Pagina);
  }

  listarAtencionSecundaria(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.segundaAtencionService.listarAtencionSecundaria(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaSegundaAtencion=data.listaatencionsecundaria;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaSegundaAtencion=[]
        }
        
      }
    )

  }

  openModalAtencionSecundaria(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosSegundaAtencionInvolucrado:null,
        ACCION:1,
      }
    };
    this.bsModalRef = this.modalService.show(ModalAtencionSecundariaComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionSecundaria(this.numPaginasMostrar,this.numero_Pagina);
      }
    );
  }

  openEditarAtencionSecundaria(atencion){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosSegundaAtencionInvolucrado: atencion,
        ACCION:2,
      }
    };
    this.bsModalRef = this.modalService.show(ModalAtencionSecundariaComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarAtencionSecundaria(this.numPaginasMostrar,this.numero_Pagina);
      }
    );
  }

  anularAtencionSecundaria(atencion){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_atencion_secundaria":atencion.id_atencion_secundaria,"usuario_eliminacion":this.usuario};
        this.fs.segundaAtencionService.anularAtencionSecundaria(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarAtencionSecundaria(this.numPaginasMostrar,this.numero_Pagina);
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarAtencionSecundaria(this.numPaginasMostrar,this.numero_Pagina);
  }

}
