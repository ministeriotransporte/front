import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SegundaAtencionComponent } from './segunda-atencion.component';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AtencionSecundariaComponent } from './pages/atencion-secundaria/atencion-secundaria.component';
import { ModalAtencionSecundariaComponent } from './pages/atencion-secundaria/modal-atencion-secundaria/modal-atencion-secundaria.component';

const routes: Routes = [{ path: '', component: SegundaAtencionComponent }];

@NgModule({
  declarations: [SegundaAtencionComponent, AtencionSecundariaComponent, ModalAtencionSecundariaComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
  ],
  entryComponents:[
    ModalAtencionSecundariaComponent,
  ]
})
export class SegundaAtencionModule { }
