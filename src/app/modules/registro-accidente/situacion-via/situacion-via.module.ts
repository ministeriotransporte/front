import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SituacionViaComponent } from './situacion-via.component';
import { SituacionViaAccidenteComponent } from './pages/situacion-via-accidente/situacion-via-accidente.component';
import { ModalSituacionViaAccidenteComponent } from './pages/situacion-via-accidente/modales/modal-situacion-via-accidente/modal-situacion-via-accidente.component';

const routes: Routes = [{ path: '', component: SituacionViaComponent }];

@NgModule({
  declarations: [SituacionViaComponent, SituacionViaAccidenteComponent, ModalSituacionViaAccidenteComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalSituacionViaAccidenteComponent
  ]
})
export class SituacionViaModule { }
