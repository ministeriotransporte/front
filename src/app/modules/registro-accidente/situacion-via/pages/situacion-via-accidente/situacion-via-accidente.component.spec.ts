import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituacionViaAccidenteComponent } from './situacion-via-accidente.component';

describe('SituacionViaAccidenteComponent', () => {
  let component: SituacionViaAccidenteComponent;
  let fixture: ComponentFixture<SituacionViaAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituacionViaAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituacionViaAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
