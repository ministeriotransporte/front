import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSituacionViaAccidenteComponent } from './modal-situacion-via-accidente.component';

describe('ModalSituacionViaAccidenteComponent', () => {
  let component: ModalSituacionViaAccidenteComponent;
  let fixture: ComponentFixture<ModalSituacionViaAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSituacionViaAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSituacionViaAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
