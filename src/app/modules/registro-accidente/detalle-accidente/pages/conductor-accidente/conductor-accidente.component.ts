import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalConductorComponent } from './modales/modal-conductor/modal-conductor.component';

@Component({
  selector: 'app-conductor-accidente',
  templateUrl: './conductor-accidente.component.html',
  styleUrls: ['./conductor-accidente.component.scss']
})
export class ConductorAccidenteComponent implements OnInit {

  listaConductores:any[];
  totalRegistros:number=0;

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  @Output() retornoValores = new EventEmitter();


  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private router: Router,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarConductor(this.numPaginasMostrar,this.numero_Pagina);
  }

  listarConductor(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.accidenteTransitoService.listarConductorVehiculo(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaConductores=data.listaconductorvehiculo;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaConductores=[]
        }
      }
    )
  }
  openModalNuevoConductor(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:1,
        datosConductorInvolucrado:null,
      }
    };
    this.bsModalRef = this.modalService.show(ModalConductorComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarConductor(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  openEditarConductor(conductor){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:2,
        datosConductorInvolucrado: conductor
      }
    };
    this.bsModalRef = this.modalService.show(ModalConductorComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarConductor(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  anularConductor(conductor){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_conductor_vehiculo":conductor.id_conductor_vehiculo,"usuario_eliminacion":this.usuario};
        this.fs.accidenteTransitoService.anularConductorVehiculo(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarConductor(this.numPaginasMostrar, this.numero_Pagina);
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarConductor(this.numPaginasMostrar, this.numero_Pagina);
  }

}
