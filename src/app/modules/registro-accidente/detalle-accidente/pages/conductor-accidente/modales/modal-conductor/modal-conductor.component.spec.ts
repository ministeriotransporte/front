import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConductorComponent } from './modal-conductor.component';

describe('ModalConductorComponent', () => {
  let component: ModalConductorComponent;
  let fixture: ComponentFixture<ModalConductorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConductorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConductorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
