import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { ConductorVehiculo } from 'src/app/modules/shared/models/accidente/conductorVehiculo';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-modal-conductor',
  templateUrl: './modal-conductor.component.html',
  styleUrls: ['./modal-conductor.component.scss']
})
export class ModalConductorComponent implements OnInit {

  @ViewChild('ddllicencias', { static: false }) selectTipoEstadoLicencia: NgSelectComponent;
  @ViewChild('ddlTipoDosaleje', { static: false }) selectTipoDosajeEtilico: NgSelectComponent;
  @ViewChild('ddlTipoResultado', { static: false }) selectTipoResultado: NgSelectComponent;
  @ViewChild('ddlEstadoConductor', { static: false }) selectTipoEstadoConductor: NgSelectComponent;
  @ViewChild('ddlEstadoConfirmacion', { static: false }) selectTipoEstadoConfirmacion: NgSelectComponent;
  @ViewChild('ddlistaPlacas', { static: false }) selectTipoPlacas: NgSelectComponent;
  @ViewChild('ddllTipoDocumento', { static: false }) selectTipoDocumento: NgSelectComponent;
  @ViewChild('ddllTipoConductor', { static: false }) selectTipoConductor: NgSelectComponent;
  @ViewChild('ddllTipoSexo', { static: false }) selectTipoSexo: NgSelectComponent;
  
  

  @Output() retornoValores = new EventEmitter();
  listaEstadoLicencia : any[];
  listaDosajeEtilico:any[];
  listaResultado:any[];
  listaEstadoConductor:any[];
  listaEstadoConfirmacion:any[];
  listaPlacas:any[];
  listaTipoDocumento:any[];
  listaTipoConductor:any[];
  listaGenero : any[];

  modelRegistroConductor: ConductorVehiculo;

  showMostrarOblidatorio: boolean=true;
  showPuntosFirmes : boolean = false;
  showMostrarResultado : boolean = true;
  showMostrarConfirmacion : boolean = false;

  datosConductorInvolucrado
  usuario;
  cambiarEditar = false;

  ACCION: number = null;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  vTipoFalta: boolean = false;

  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarConductorVehiculoControl();
    this.crearFormGroup();

  }

  crearFormGroup(){
    if(this.datosConductorInvolucrado==null){
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_tipo_conductor:null,
        dni:null,
        nombre:null,
        apellido_paterno:null,
        apellido_materno:null,
        id_estado_licencia:null,
        punto_firme:null,
        sancion:false,
        id_estado_confirmado:null,
        id_tipo_dosaje_etilico:null,
        id_resultado_dosaje_etilico:null,
        id_estado_conductor:null,
        id_vehiculo_involucrado:null,
        id_tipo_documento:1,
        fecha_nacimiento:null,
        id_tipo_sexo:null
      })
    }else{
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_conductor_vehiculo:this.datosConductorInvolucrado.id_conductor_vehiculo == null ? 0 : this.datosConductorInvolucrado.id_conductor_vehiculo,
        id_tipo_conductor : this.datosConductorInvolucrado.id_tipo_conductor == null ? 0 : this.datosConductorInvolucrado.id_tipo_conductor,
        dni:this.datosConductorInvolucrado.dni == null ? "" : this.datosConductorInvolucrado.dni,
        nombre:this.datosConductorInvolucrado.nombre == null ? "" : this.datosConductorInvolucrado.nombre,
        apellido_paterno:this.datosConductorInvolucrado.apellido_paterno == null ? "" : this.datosConductorInvolucrado.apellido_paterno,
        apellido_materno:this.datosConductorInvolucrado.apellido_materno == null ? "" : this.datosConductorInvolucrado.apellido_materno,
        id_estado_licencia:this.datosConductorInvolucrado.id_estado_licencia == null ? 0 : this.datosConductorInvolucrado.id_estado_licencia,
        punto_firme:this.datosConductorInvolucrado.punto_firme == null ? 0 : this.datosConductorInvolucrado.punto_firme,
        sancion:this.datosConductorInvolucrado.sancion == null ?null : this.datosConductorInvolucrado.sancion,
        id_estado_confirmado:this.datosConductorInvolucrado.id_estado_confirmado == null ? null : this.datosConductorInvolucrado.id_estado_confirmado,
        id_tipo_dosaje_etilico:this.datosConductorInvolucrado.id_tipo_dosaje_etilico == null ? 0 : this.datosConductorInvolucrado.id_tipo_dosaje_etilico,
        id_resultado_dosaje_etilico:this.datosConductorInvolucrado.id_resultado_dosaje_etilico == null ? null : this.datosConductorInvolucrado.id_resultado_dosaje_etilico,
        id_estado_conductor:this.datosConductorInvolucrado.id_estado_conductor == null ? 0 : this.datosConductorInvolucrado.id_estado_conductor,
        id_vehiculo_involucrado:this.datosConductorInvolucrado.id_vehiculo_involucrado == null ? 0 : this.datosConductorInvolucrado.id_vehiculo_involucrado,
        id_tipo_documento:this.datosConductorInvolucrado.id_tipo_documento == null ? 0 : this.datosConductorInvolucrado.id_tipo_documento,
        fecha_nacimiento: this.datosConductorInvolucrado.fecha_nacimiento == null ? null : new Date(this.datosConductorInvolucrado.fecha_nacimiento),
        id_tipo_sexo : this.datosConductorInvolucrado.id_tipo_sexo == null ? 0  : this.datosConductorInvolucrado.id_tipo_sexo,

        
      })
      if(this.datosConductorInvolucrado.sancion){
        this.vTipoFalta=true;
        this.showPuntosFirmes=true;
      }else{
        this.vTipoFalta=false;
        this.showPuntosFirmes=false;
      }
      if(this.datosConductorInvolucrado.id_tipo_documento==2){
        this.tipoDocumentoCadena="Carnet Extranjeria";
        this.numCantidad=12;
      }else{
        this.tipoDocumentoCadena="DNI";
        this.numCantidad=8;
      }

      if(this.datosConductorInvolucrado.id_tipo_dosaje_etilico==2){
        this.showMostrarResultado=false;
      }else{
        this.showMostrarResultado=true;
      }
      if(this.datosConductorInvolucrado.id_estado_conductor==2){
        this.showMostrarConfirmacion=true;
      }else{
        this.showMostrarConfirmacion=false;
      }
    }

    
  }

  envento(evento,tipoSnacion){
    if (tipoSnacion=="true") {
      this.showPuntosFirmes=true;
      this.vTipoFalta = true;
    } else {
      this.showPuntosFirmes=false;
      this.vTipoFalta = false;
      this.formGroup.patchValue({
        punto_firme:null,
      })
    }
  }

  validarResultado(evento){
    if(evento!=undefined){
      if(evento.id_tipo_dosaje_etilico==2){
        this.showMostrarResultado=false;
        this.formGroup.patchValue({
          id_resultado_dosaje_etilico:null
        })
      }else{
        this.showMostrarResultado=true;
      }
    }
  }

  listarConductorVehiculoControl(){
    this.fs.accidenteTransitoService.listarConductorVehiculoControl().subscribe(
      (data:any)=>{
        this.listaEstadoLicencia=data.estadolicencia;
        this.listaDosajeEtilico=data.dosajeetilico;
        this.listaResultado=data.resultadodosajeetilico;
        this.listaEstadoConductor=data.estadoconductor;
        this.listaEstadoConfirmacion=data.estadoconfirmado;
        this.listaTipoDocumento=data.tipodocumento;
        this.listaTipoConductor=data.tipoconductor;
        this.listaGenero=data.tiposexo;
      }
    )

    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.accidenteTransitoService.listarVehiculoInvolucradoCombo(param).subscribe(
      (data:any)=>{
        this.listaPlacas=data.listavehiculosinvolucrados;
      }
    )
  }

  tipoDocumentoCadena:string="DNI";
  numCantidad:number=8;
  validarDocumento(evento){
    if(evento!=undefined){
      this.tipoDocumentoCadena=evento.descripcion;
      if(evento.id_tipo_documento==2){
        //this.tipoDocumentoCadena="Carnet Extranjeria";
        this.numCantidad=12;
      }else{
        //this.tipoDocumentoCadena="DNI";
        this.numCantidad=8;
      }
    }else{

    }
  }

  validarCamposObligatorios(evento){
    if(evento!=undefined){
      if(evento.id_tipo_conductor==2){
        this.showMostrarOblidatorio=false;
      }else{
        this.showMostrarOblidatorio=true;
      }
    }
  }

  validarEstadoConfirmacion(evento){
    if(evento!=undefined){
      if(evento.id_estado_conductor==2){
        this.showMostrarConfirmacion=true;
      }else{
        this.showMostrarConfirmacion=false;
        this.formGroup.patchValue({
          id_estado_confirmado:null,
        })
      }
    }
  }

  validarDNI() {
    const valDni = this.formGroup.get('dni').value;
    if (!isNullOrUndefined(valDni) && valDni.length == 8) {
      //this.validarInformacionReniec();
    } else {
      this.formGroup.patchValue({
        nombre: null,
        apellido_paterno: null,
        apellido_materno: null,

      });
      //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
      //imagen.src = '';
    }
  }

  validarInformacionReniec() {
    let valDni = this.formGroup.get('dni').value;
    let cant = 0;
    if (valDni == '' || valDni == null) {
      document.getElementById('dni').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de Dni a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionReniec(valDni).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.formGroup.patchValue({
                nombre: response.strnombres,
                apellido_paterno: response.strapellidopaterno,
                apellido_materno: response.strapellidomaterno,
              });
              //this.foto = response.strfoto;
              //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = response.strfoto;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
              this.formGroup.patchValue({
                nombre: null,
                apellido_paterno: null,
                apellido_materno: null,
              });
              //let imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = '';
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
            this.formGroup.patchValue({
              nombre: null,
              apellido_paterno: null,
              apellido_materno: null,
            });
          }
        );
      } else {
        this.funciones.mensaje('info', 'El DNI ingresado ya se encuentra registrado en el sistema.');
      }
    }
  }

  registrarConductor(){
    if (this.Validar()) {
      this.asignarValoreConductor();
      if (this.datosConductorInvolucrado == null) {
        this.bMostrar = true;
        this.fs.accidenteTransitoService.insertarConductorVehiculo(this.modelRegistroConductor).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", respuesta.mensaje);
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.accidenteTransitoService.modificarConductorVehiculo(this.modelRegistroConductor).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", respuesta.mensaje);
            }
            this.bMostrar = false;
          });
      }
    }
  }

  Validar(): boolean {
    let conductor = Object.assign({}, this.formGroup.value);
    if (conductor.id_tipo_conductor == null || conductor.id_tipo_conductor == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Tipo de Conductor</b>", true, () => {
        this.selectTipoConductor.focus();
      });
      return false;
    }
    if (conductor.id_tipo_conductor!=2 && (conductor.id_tipo_documento == null || conductor.id_tipo_documento == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Tipo Documento</b>", true, () => {
        this.selectTipoDocumento.focus();
      });
      return false;
    }
    if (conductor.id_tipo_conductor!=2 && (conductor.dni == null || conductor.dni == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>DNI</b>", true, () => {
        document.getElementById("dni").focus();
      });
      return false;
    }
    if (conductor.id_tipo_conductor!=2 && (conductor.fecha_nacimiento == null || conductor.fecha_nacimiento == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>Fecha de Nacimiento</b>", true, () => {
        document.getElementById("fecha_nacimiento").focus();
      });
      return false;
    }
    if (conductor.id_tipo_conductor!=2 && (conductor.id_tipo_sexo == null || conductor.id_tipo_sexo == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Sexo</b>", true, () => {
        this.selectTipoSexo.focus();
      });
      return false;
    }
    if (conductor.nombre == null || conductor.nombre == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Nombre del conductor</b>", true, () => {
        document.getElementById("nombre").focus();
      });
      return false;
    }
    if (conductor.apellido_paterno == null || conductor.apellido_paterno == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Paterno del conductor</b>", true, () => {
        document.getElementById("apellido_paterno").focus();
      });
      return false;
    }
    if (conductor.apellido_materno == null || conductor.apellido_materno == "") {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Materno</b>", true, () => {
        document.getElementById("apellido_materno").focus();
      });
      return false;
    }
    if (conductor.id_estado_licencia == null || conductor.id_estado_licencia == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado de la Licencia</b>", true, () => {
        this.selectTipoEstadoLicencia.focus();
      });
      return false;
    }
    if (conductor.sancion == null) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione <br><b>Registra Sanciones</b>", true, () => {
        //document.getElementById("punto_firme").focus();
      });
      return false;
    }
    if ((conductor.sancion===true || conductor.sancion==="true") &&  (conductor.punto_firme == null || conductor.punto_firme == 0 || conductor.punto_firme == "0")) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Puntos Firmes</b>", true, () => {
        document.getElementById("punto_firme").focus();
      });
      return false;
    }
    if (conductor.id_tipo_dosaje_etilico == null || conductor.id_tipo_dosaje_etilico == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Dosaje Etílico</b>", true, () => {
        this.selectTipoDosajeEtilico.focus();
      });
      return false;
    }
    if (conductor.id_tipo_dosaje_etilico!=2 &&  (conductor.id_resultado_dosaje_etilico == null || conductor.id_resultado_dosaje_etilico == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Resultado</b>", true, () => {
        this.selectTipoResultado.focus();
      });
      return false;
    }
    if (conductor.id_estado_conductor == null || conductor.id_estado_conductor == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado Conductor</b>", true, () => {
        this.selectTipoEstadoConductor.focus();
      });
      return false;
    }
    if (conductor.id_estado_conductor==2 && (conductor.id_estado_confirmado == null || conductor.id_estado_confirmado == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado Confirmación</b>", true, () => {
        this.selectTipoEstadoConfirmacion.focus();
      });
      return false;
    }
    if (conductor.id_vehiculo_involucrado == null || conductor.id_vehiculo_involucrado == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Placa de automóvil del conductor</b>", true, () => {
        this.selectTipoPlacas.focus();
      });
      return false;
    }
    return true;
  }

  asignarValoreConductor(){
    let modelRegistroConductorEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroConductor = new ConductorVehiculo();
    this.modelRegistroConductor.id_conductor_vehiculo = modelRegistroConductorEnvio.id_conductor_vehiculo == null ? 0 : modelRegistroConductorEnvio.id_conductor_vehiculo;
    this.modelRegistroConductor.id_tipo_conductor = modelRegistroConductorEnvio.id_tipo_conductor == null ? 0 : modelRegistroConductorEnvio.id_tipo_conductor;
    this.modelRegistroConductor.id_tipo_documento = modelRegistroConductorEnvio.id_tipo_documento == null ? 0 : modelRegistroConductorEnvio.id_tipo_documento;
    this.modelRegistroConductor.dni = modelRegistroConductorEnvio.dni == null ? "" :modelRegistroConductorEnvio.dni;
    this.modelRegistroConductor.nombre = modelRegistroConductorEnvio.nombre == null ? 0 : modelRegistroConductorEnvio.nombre;
    this.modelRegistroConductor.apellido_paterno = modelRegistroConductorEnvio.apellido_paterno == null ? 0 : modelRegistroConductorEnvio.apellido_paterno;
    this.modelRegistroConductor.apellido_materno = modelRegistroConductorEnvio.apellido_materno == null ? 0 : modelRegistroConductorEnvio.apellido_materno;
    this.modelRegistroConductor.id_estado_licencia = modelRegistroConductorEnvio.id_estado_licencia == null ? 0 : modelRegistroConductorEnvio.id_estado_licencia;
    
    this.modelRegistroConductor.sancion = modelRegistroConductorEnvio.sancion == null ? false : modelRegistroConductorEnvio.sancion;
    this.modelRegistroConductor.punto_firme = modelRegistroConductorEnvio.punto_firme == null ? 0 : Number(modelRegistroConductorEnvio.punto_firme);
    this.modelRegistroConductor.id_tipo_dosaje_etilico = modelRegistroConductorEnvio.id_tipo_dosaje_etilico == null ? 0 : modelRegistroConductorEnvio.id_tipo_dosaje_etilico;
    this.modelRegistroConductor.id_resultado_dosaje_etilico = modelRegistroConductorEnvio.id_resultado_dosaje_etilico == null ? null : modelRegistroConductorEnvio.id_resultado_dosaje_etilico;
    this.modelRegistroConductor.id_estado_conductor = modelRegistroConductorEnvio.id_estado_conductor == null ? 0 : modelRegistroConductorEnvio.id_estado_conductor;
    this.modelRegistroConductor.id_estado_confirmado = modelRegistroConductorEnvio.id_estado_confirmado == null ? null : modelRegistroConductorEnvio.id_estado_confirmado;
    this.modelRegistroConductor.id_vehiculo_involucrado = modelRegistroConductorEnvio.id_vehiculo_involucrado == null ? 0 : modelRegistroConductorEnvio.id_vehiculo_involucrado;
    this.modelRegistroConductor.fecha_nacimiento = modelRegistroConductorEnvio.fecha_nacimiento == null ? null : modelRegistroConductorEnvio.fecha_nacimiento;
    this.modelRegistroConductor.id_tipo_sexo = modelRegistroConductorEnvio.id_tipo_sexo == null ? 0 : modelRegistroConductorEnvio.id_tipo_sexo;
    this.modelRegistroConductor.activo = true;
    this.modelRegistroConductor.usuario_creacion = this.usuario;
    this.modelRegistroConductor.usuario_modificacion = this.usuario;
  }

  closeModal(){
    this.modalRef.hide();
  }
  

}
