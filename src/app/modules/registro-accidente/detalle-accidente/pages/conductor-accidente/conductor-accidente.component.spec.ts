import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConductorAccidenteComponent } from './conductor-accidente.component';

describe('ConductorAccidenteComponent', () => {
  let component: ConductorAccidenteComponent;
  let fixture: ComponentFixture<ConductorAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConductorAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConductorAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
