import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { SituacionVia } from 'src/app/modules/shared/models/accidente/situacionVia';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-situacion-via',
  templateUrl: './modal-situacion-via.component.html',
  styleUrls: ['./modal-situacion-via.component.scss']
})
export class ModalSituacionViaComponent implements OnInit {
  @ViewChild('tipoTransitabilidad', { static: false }) selectTipoTransitabilidad: NgSelectComponent;
  @ViewChild('tipoVia', { static: false }) selectTipoVia: NgSelectComponent;
  @ViewChild('tipoZona', { static: false }) selectTipoZona: NgSelectComponent;

  listaTransitabilidad:any[];
  listaTipoVia:any[];
  listaZona:any[];

  modelRegistroSituacionVia: SituacionVia;

  bMostrar: boolean = false;

  formGroup: FormGroup;

  ACCION: number = null;
  datosSitacionViaInvolucrado;
  usuario;

  ShowOtroTipoVia

  @Output() retornoValores = new EventEmitter();

  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    private funciones : Funciones,
    private ruta : ParametrosGeneralesServiceService,
    public modalRef: BsModalRef,) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.crearFormGroup();
    this.listarSituacionViaControl();
  }

  crearFormGroup(){
    if(this.datosSitacionViaInvolucrado==null){
      this.formGroup = this.fb.group({
        id_situacion_via:null,
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_tipo_via:null,
        otro_tipo_via:"",
        id_tipo_zona:null,
        id_tipo_transitabilidad:null,
        activo:null,
      })
    }else{
      this.formGroup = this.fb.group({
        id_situacion_via:this.datosSitacionViaInvolucrado.id_situacion_via == null ? 0 : this.datosSitacionViaInvolucrado.id_situacion_via,
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_tipo_via:this.datosSitacionViaInvolucrado.id_tipo_via == null ? 0 : this.datosSitacionViaInvolucrado.id_tipo_via,
        otro_tipo_via:this.datosSitacionViaInvolucrado.otro_tipo_via == null ? "" : this.datosSitacionViaInvolucrado.otro_tipo_via,
        id_tipo_zona:this.datosSitacionViaInvolucrado.id_tipo_zona == null ? 0 : this.datosSitacionViaInvolucrado.id_tipo_zona,
        id_tipo_transitabilidad:this.datosSitacionViaInvolucrado.id_tipo_transitabilidad == null ? 0 : this.datosSitacionViaInvolucrado.id_tipo_transitabilidad,
      })

      if(this.datosSitacionViaInvolucrado.id_tipo_via==9){
        this.ShowOtroTipoVia=true;
      }else{
        this.ShowOtroTipoVia=false;
      }
    }

  }

  validarOtroTipoVia(evento){
    if(evento!=undefined){
      if(evento.id_tipo_via==9){
        this.ShowOtroTipoVia=true;
      }else{
        this.ShowOtroTipoVia=false;
        this.formGroup.patchValue({
          otro_tipo_via:"",
        });
      }
    }
  }

  
  listarSituacionViaControl(){
    this.fs.accidenteTransitoService.listarSituacionViaControl().subscribe(
      (data:any)=>{
        this.listaTransitabilidad=data.tipotransitabilidad;
        this.listaTipoVia=data.tipovia;
        this.listaZona=data.tipozona;
      }
    )
  }

  Grabar(){
    if (this.Validar()) {
      this.asignarValoreSituacionVia();
      if (this.datosSitacionViaInvolucrado == null) {
        this.bMostrar = true;
        this.fs.accidenteTransitoService.insertarSituacionVia(this.modelRegistroSituacionVia).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.accidenteTransitoService.modificarSituacionVia(this.modelRegistroSituacionVia).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }
  }

  Validar(): boolean {
    let via = Object.assign({}, this.formGroup.value);
    if (via.id_tipo_transitabilidad == null || via.id_tipo_transitabilidad == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>TRANSITABILIDAD</b>", true, () => {
        this.selectTipoTransitabilidad.focus();
      });
      return false;
    }
    if (via.id_tipo_via == null || via.id_tipo_via == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Tipo de Vía</b>", true, () => {
        this.selectTipoVia.focus();
      });
      return false;
    }
    if (via.id_tipo_via == 9 && (via.otro_tipo_via == null || via.otro_tipo_via == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>La descripción del otro tipo de Vía</b>", true, () => {
        document.getElementById("otro_tipo_via").focus();
      });
      return false;
    }
    if (via.id_tipo_zona == null || via.id_tipo_zona == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Zona Urbana / Zonal</b>", true, () => {
        this.selectTipoZona.focus();
      });
      return false;
    }
    return true;
  }

  asignarValoreSituacionVia(){
    let modelRegistroSituacionViaEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroSituacionVia = new SituacionVia();
    this.modelRegistroSituacionVia.id_situacion_via = modelRegistroSituacionViaEnvio.id_situacion_via == null ? 0 : modelRegistroSituacionViaEnvio.id_situacion_via;
    this.modelRegistroSituacionVia.id_accidente_transito = modelRegistroSituacionViaEnvio.id_accidente_transito == null ? 0 : modelRegistroSituacionViaEnvio.id_accidente_transito;
    this.modelRegistroSituacionVia.id_tipo_via = modelRegistroSituacionViaEnvio.id_tipo_via == null ? 0 : modelRegistroSituacionViaEnvio.id_tipo_via;
    this.modelRegistroSituacionVia.id_tipo_zona = modelRegistroSituacionViaEnvio.id_tipo_zona == null ? 0 : modelRegistroSituacionViaEnvio.id_tipo_zona;
    this.modelRegistroSituacionVia.id_tipo_transitabilidad = modelRegistroSituacionViaEnvio.id_tipo_transitabilidad == null ? 0 : modelRegistroSituacionViaEnvio.id_tipo_transitabilidad;
    this.modelRegistroSituacionVia.otro_tipo_via = modelRegistroSituacionViaEnvio.otro_tipo_via == null ? "" :modelRegistroSituacionViaEnvio.otro_tipo_via;
   
    this.modelRegistroSituacionVia.activo = true;
    this.modelRegistroSituacionVia.usuario_creacion = this.usuario;
    this.modelRegistroSituacionVia.usuario_modificacion = this.usuario;
  }

  closeModal(){
    this.modalRef.hide();
  }

}
