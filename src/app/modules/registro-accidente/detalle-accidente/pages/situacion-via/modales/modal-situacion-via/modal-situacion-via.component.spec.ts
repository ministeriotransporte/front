import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSituacionViaComponent } from './modal-situacion-via.component';

describe('ModalSituacionViaComponent', () => {
  let component: ModalSituacionViaComponent;
  let fixture: ComponentFixture<ModalSituacionViaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSituacionViaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSituacionViaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
