import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalSituacionViaComponent } from './modales/modal-situacion-via/modal-situacion-via.component';


@Component({
  selector: 'app-situacion-via',
  templateUrl: './situacion-via.component.html',
  styleUrls: ['./situacion-via.component.scss']
})
export class SituacionViaComponent implements OnInit {
  listaSiruacionVia:any[];

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  mostrarNuevaVia: boolean = true;


  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.obtenerSituacionVia();
    
  }
  
  obtenerSituacionVia(){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.accidenteTransitoService.obtenerSituacionVia(param).subscribe(
      (data:any)=>{
        if(data.listaconductorvehiculo!=null){
          this.listaSiruacionVia=data.listaconductorvehiculo;
          this.mostrarNuevaVia = false;
        }else{
          this.listaSiruacionVia=[];
          this.mostrarNuevaVia = true;
        }
      }
    )
  }

  openModalNuevoSituacionVia(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:1,
        datosSitacionViaInvolucrado:null,
      }
    };
    this.bsModalRef = this.modalService.show(ModalSituacionViaComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.obtenerSituacionVia();
      }
    );
  }
  openEditarSituacionVia(situacion){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:2,
        datosSitacionViaInvolucrado: situacion
      }
    };
    this.bsModalRef = this.modalService.show(ModalSituacionViaComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.obtenerSituacionVia();
      }
    );
  }

  anularSituacionVia(via){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_situacion_via":via.id_situacion_via,"usuario_eliminacion":this.usuario};
        this.fs.accidenteTransitoService.anularSituacionVia(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.obtenerSituacionVia();
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  

}
