import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituacionViaComponent } from './situacion-via.component';

describe('SituacionViaComponent', () => {
  let component: SituacionViaComponent;
  let fixture: ComponentFixture<SituacionViaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituacionViaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituacionViaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
