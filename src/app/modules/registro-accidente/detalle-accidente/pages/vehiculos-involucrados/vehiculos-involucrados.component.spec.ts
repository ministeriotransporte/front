import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculosInvolucradosComponent } from './vehiculos-involucrados.component';

describe('VehiculosInvolucradosComponent', () => {
  let component: VehiculosInvolucradosComponent;
  let fixture: ComponentFixture<VehiculosInvolucradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiculosInvolucradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculosInvolucradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
