import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalVehiculoInvolucradoComponent } from './modales/modal-vehiculo-involucrado/modal-vehiculo-involucrado.component';

@Component({
  selector: 'app-vehiculos-involucrados',
  templateUrl: './vehiculos-involucrados.component.html',
  styleUrls: ['./vehiculos-involucrados.component.scss']
})
export class VehiculosInvolucradosComponent implements OnInit {

  @Output() retornoValores = new EventEmitter();

  listaVehiculosInvolucrados: any[];
  totalRegistros:number=0;

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private router: Router,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarVehiculoInvolucrado(this.numPaginasMostrar,this.numero_Pagina);
  }

  listarVehiculoInvolucrado(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.accidenteTransitoService.listarVehiculoInvolucrado(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaVehiculosInvolucrados=data.listavehiculosinvolucrados;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaVehiculosInvolucrados=[]
        }
        
      }
    )

  }

  openModalNuevoVehiculo(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosVehiculoInvolucrado:null,
        ACCION:1,
      }
    };
    this.bsModalRef = this.modalService.show(ModalVehiculoInvolucradoComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarVehiculoInvolucrado(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  openEditarVehiculo(vehiculo){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-vehiculo',
      initialState: {
        datosVehiculoInvolucrado: vehiculo,
        ACCION:2,
      }
    };
    this.bsModalRef = this.modalService.show(ModalVehiculoInvolucradoComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarVehiculoInvolucrado(this.numPaginasMostrar, this.numero_Pagina);
      }
    );
  }

  anularVehiculo(vehiculo){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_vehiculo_involucrado":vehiculo.id_vehiculo_involucrado,"usuario_eliminacion":this.usuario};
        this.fs.accidenteTransitoService.anularVehiculoInvolucrado(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarVehiculoInvolucrado(this.numPaginasMostrar, this.numero_Pagina);
          }else {
            this.funciones.mensaje("info",data.mensaje);
            //this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarVehiculoInvolucrado(this.numPaginasMostrar, this.numero_Pagina);
  }

}
