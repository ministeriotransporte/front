import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVehiculoInvolucradoComponent } from './modal-vehiculo-involucrado.component';

describe('ModalVehiculoInvolucradoComponent', () => {
  let component: ModalVehiculoInvolucradoComponent;
  let fixture: ComponentFixture<ModalVehiculoInvolucradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalVehiculoInvolucradoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVehiculoInvolucradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
