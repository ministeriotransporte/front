import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { VehiculoInvolucrado } from 'src/app/modules/shared/models/accidente/vehiculoInvolucrado';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-modal-vehiculo-involucrado',
  templateUrl: './modal-vehiculo-involucrado.component.html',
  styleUrls: ['./modal-vehiculo-involucrado.component.scss']
})
export class ModalVehiculoInvolucradoComponent implements OnInit {
  @ViewChild('tipoVehiculo', { static: false }) selectTipoVehiculo: NgSelectComponent;
  @ViewChild('tipoEstadoSoat', { static: false }) selectTipoEstadoSOAT: NgSelectComponent;
  @ViewChild('tipoCompaniaSoat', { static: false }) selectTipoCompania: NgSelectComponent;
  @ViewChild('tipoCitv', { static: false }) selectTipoCitv: NgSelectComponent;
  //@ViewChild('tipoServicioTransporte', { static: false }) selectTipoServicioTransporte: NgSelectComponent;
  //@ViewChild('tipoMercancias', { static: false }) selectTipoMercancias: NgSelectComponent;
  //@ViewChild('tipoMaterialesPeligrosos', { static: false }) selectTipoMaterialesPeligrosos: NgSelectComponent;
  //@ViewChild('tipoPermisoEventual', { static: false }) selectTipoPermisoEventual: NgSelectComponent;
  @ViewChild('tarjetaCirculacion', { static: false }) selectTipoTarjetaCirculacion: NgSelectComponent;
  @ViewChild('situacionVehiculo', { static: false }) selectSituacionVehiculo: NgSelectComponent;
  @ViewChild('modalidadTransoprte', { static: false }) selectModalidadTransporte: NgSelectComponent;
  @ViewChild('estadoModalidad', { static: false }) selectEstadoModalidad: NgSelectComponent;
  
  
  


  @Output() retornoValores = new EventEmitter();

  listTipoVehiculo: any[];
  listEstadoSoat : any[];
  listNombreCompanias : any[];
  listTipoCitv : any[];
  //listTipoTransportePersonas : any[];
  //listTransporteTerrestre : any[];
  //listMaterialesPeligrosos : any[];
  //listPermisosTransporte : any[];
  listTarjetasCirculacion : any[];
  listSituacionVehiculo : any[];
  listModalidadTransporte : any[]; 
  listEstadoModalidad : any[];

  bMostrar:Boolean=false;
  modelRegistroVehiculo: VehiculoInvolucrado;


  formGroup: FormGroup;
  datosVehiculoInvolucrado

  usuario;
  ACCION: number = null;

  ShowOtroCompaniaSOAT:boolean=false;
  showPlacaRemolque : boolean=false;
  showOtraModalidad : boolean=false;
  showMostrarOblidatorio : boolean = true;
  showMostrraNombreCompania : boolean = true;
  vTipoCorresponde: boolean = false;

  isMeridian = false;
  showSpinners = false;
  readonly = true;

  fechaRegistroAccidente:any;

  showDatosSutran:boolean=false;

  fechaActual : any;
  constructor(private fs : FacadeService,
              private fb: FormBuilder,
              public funciones : Funciones,
              public modalRef: BsModalRef,
              private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.fechaActual=new Date();
    this.fechaRegistroAccidente=new Date(this.ruta.getRastro().accidente.p_fecha_registro_accidente_transito);
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarVehiculoInvolucradoControl();
    this.crearFormGroup();
  }

  crearFormGroup(){
    if(this.datosVehiculoInvolucrado==null){
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_situacion_vehiculo: null,
        id_vehiculo_involucrado:null,
        placa_vehiculo_involucrado:null,
        placa_remolque:null,
        id_tipo_vehiculo:null,
        id_estado_soat:null,
        id_compania_seguro:null,
        id_estado_citv:null,
        id_modalidad_transporte : null,
        otro_modalidad_transporte:null,
        id_estado_mod_transporte : null,
        //id_servicio_transporte_persona:null,
        //id_servicio_transporte_mercancia:null,
        //id_material_peligroso:null,
        //id_eventual_transporte_persona:null,
        id_tarjeta_circulacion:null,
        fecha_registro:null,
        hora_registro:null,
        latitud:null,
        longitud:null,
        gps_sutran:false,
        otro_compania_seguro:""
      })
    }else{
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_situacion_vehiculo: this.datosVehiculoInvolucrado.id_situacion_vehiculo == null ? 0 : this.datosVehiculoInvolucrado.id_situacion_vehiculo,
        id_vehiculo_involucrado: this.datosVehiculoInvolucrado.id_vehiculo_involucrado == null ? 0 : this.datosVehiculoInvolucrado.id_vehiculo_involucrado,
        placa_vehiculo_involucrado: this.datosVehiculoInvolucrado.placa_vehiculo_involucrado == null ? "" : this.datosVehiculoInvolucrado.placa_vehiculo_involucrado,
        placa_remolque:this.datosVehiculoInvolucrado.placa_remolque == null ? "" : this.datosVehiculoInvolucrado.placa_remolque,
        id_tipo_vehiculo:this.datosVehiculoInvolucrado.id_tipo_vehiculo == null ? null : this.datosVehiculoInvolucrado.id_tipo_vehiculo,
        id_estado_soat:this.datosVehiculoInvolucrado.id_estado_soat == null ? null : this.datosVehiculoInvolucrado.id_estado_soat,
        id_compania_seguro:this.datosVehiculoInvolucrado.id_compania_seguro == null ? null : this.datosVehiculoInvolucrado.id_compania_seguro,
        id_estado_citv:this.datosVehiculoInvolucrado.id_estado_citv == null ? null : this.datosVehiculoInvolucrado.id_estado_citv,
        id_modalidad_transporte:this.datosVehiculoInvolucrado.id_modalidad_transporte == null ? null : this.datosVehiculoInvolucrado.id_modalidad_transporte,
        otro_modalidad_transporte: this.datosVehiculoInvolucrado.otro_modalidad_transporte == null ? "" : this.datosVehiculoInvolucrado.otro_modalidad_transporte,
        id_estado_mod_transporte : this.datosVehiculoInvolucrado.id_estado_mod_transporte == null ? null : this.datosVehiculoInvolucrado.id_estado_mod_transporte,
        //id_servicio_transporte_persona:this.datosVehiculoInvolucrado.id_servicio_transporte_persona == null ? 0 : this.datosVehiculoInvolucrado.id_servicio_transporte_persona,
        //id_servicio_transporte_mercancia:this.datosVehiculoInvolucrado.id_servicio_transporte_mercancia == null ? 0 : this.datosVehiculoInvolucrado.id_servicio_transporte_mercancia,
        //id_material_peligroso:this.datosVehiculoInvolucrado.id_material_peligroso == null ? 0 : this.datosVehiculoInvolucrado.id_material_peligroso,
        //id_eventual_transporte_persona:this.datosVehiculoInvolucrado.id_eventual_transporte_persona == null ? 0 : this.datosVehiculoInvolucrado.id_eventual_transporte_persona,
        id_tarjeta_circulacion:this.datosVehiculoInvolucrado.id_tarjeta_circulacion == null ? null : this.datosVehiculoInvolucrado.id_tarjeta_circulacion,
        fecha_registro:this.datosVehiculoInvolucrado.fecha_registro == null ? null : new Date(this.datosVehiculoInvolucrado.fecha_registro),
        hora_registro: this.datosVehiculoInvolucrado.hora_registro == null ? null : this.datosVehiculoInvolucrado.hora_registro,
        latitud: this.datosVehiculoInvolucrado.latitud == null ? null : this.datosVehiculoInvolucrado.latitud,
        longitud: this.datosVehiculoInvolucrado.longitud == null ? null : this.datosVehiculoInvolucrado.longitud,
        otro_compania_seguro: this.datosVehiculoInvolucrado.otro_compania_seguro == null ? "" : this.datosVehiculoInvolucrado.otro_compania_seguro,
        gps_sutran:this.datosVehiculoInvolucrado.gps_sutran == null ?null : this.datosVehiculoInvolucrado.gps_sutran,
      })

      if(this.datosVehiculoInvolucrado.gps_sutran){
        this.showDatosSutran=true;
        this.vTipoCorresponde=true;
      }else{
        this.vTipoCorresponde=false;
        this.showDatosSutran=false;
      }

      if(this.datosVehiculoInvolucrado.id_compania_seguro==7){
        this.ShowOtroCompaniaSOAT=true;
      }else{
        this.ShowOtroCompaniaSOAT=false;
      }
      if(this.datosVehiculoInvolucrado.id_tipo_vehiculo==10){
        this.showPlacaRemolque=true;
      }else{
        this.showPlacaRemolque=false;
      }

      if(this.datosVehiculoInvolucrado.id_modalidad_transporte==16){
        this.showOtraModalidad=true;
      }else{
        this.showOtraModalidad=false;
      }
      if(this.datosVehiculoInvolucrado.id_estado_soat==3){
        this.showMostrraNombreCompania=false;
      }else{
        this.showMostrraNombreCompania=true;
      }
    }
  }

  validarOtroTipoCompania(evento){
    if(evento!=undefined){
      if(evento.id_compania_seguro==7){
        this.ShowOtroCompaniaSOAT=true;
      }else{
        this.ShowOtroCompaniaSOAT=false;
        this.formGroup.patchValue({
          otro_compania_seguro:"",
        });
      }
    }
  }

  validarCamposObligatorios(evento){
    if(evento!=undefined){
      if(evento.id_situacion_vehiculo==2){
        this.showMostrarOblidatorio=false;
      }else{
        this.showMostrarOblidatorio=true;
      }
    }
  }

  validarNombreCompania(evento){
    if(evento!=undefined){
      if(evento.id_estado_soat==3){
        this.showMostrraNombreCompania=false;
        this.formGroup.patchValue({
          id_compania_seguro:null
        })
      }else{
        this.showMostrraNombreCompania=true;
      }
    }
  }
  

  validarOtraModalidadTransporte(evento){
    if(evento!=undefined){
      if(evento.id_modalidad_transporte==16){
        this.showOtraModalidad=true;
      }else{
        this.showOtraModalidad=false;
        this.formGroup.patchValue({
          otro_modalidad_transporte:"",
        });
      }
    }
  }

  validarTipoPlaca(evento){
    if(evento!=undefined){
      if(evento.id_tipo_vehiculo==9 || evento.id_tipo_vehiculo==10 /*|| evento.id_tipo_vehiculo==11 || evento.id_tipo_vehiculo==12*/){

        this.showPlacaRemolque=true;
      }else{
        this.showPlacaRemolque=false;
        this.formGroup.patchValue({
          placa_remolque:"",
        });
      }
    }
  }
  

  listarVehiculoInvolucradoControl(){
    this.fs.accidenteTransitoService.listarVehiculoInvolucradoControl().subscribe(
      (data:any)=>{
        this.listTipoVehiculo=data.tipovehiculo;
        this.listEstadoSoat=data.estadosoat;
        this.listNombreCompanias=data.companiaseguro;
        this.listTipoCitv=data.estadocitv;
        //this.listTipoTransportePersonas=data.transportepersona;
        //this.listTransporteTerrestre=data.transportemercancia;
        //this.listMaterialesPeligrosos=data.materialpeligroso;
        //this.listPermisosTransporte= data.eventualtransportepasajero;
        this.listTarjetasCirculacion = data.tarjetacirculacion;
        this.listSituacionVehiculo = data.situacionvehiculo;
        this.listModalidadTransporte = data.modalidadtransporte;
        this.listEstadoModalidad = data.estadomodalidad;

      }
    )
  }

  envento(evento,valor){
    if (valor=="true") {
      this.showDatosSutran=true;
      this.vTipoCorresponde = true;
    } else {
      this.showDatosSutran=false;
      this.vTipoCorresponde = false;
      this.formGroup.patchValue({
        latitud:null,
        longitud:null,
        fecha_registro:null,
        hora_registro:null

      })
    }
  }

  Grabar(){
    if (this.validarControles()) {
      this.asignarValoresVehiculo();
      if (this.datosVehiculoInvolucrado == null) {
        this.bMostrar = true;
        this.fs.accidenteTransitoService.insertarVehiculoInvolucrado(this.modelRegistroVehiculo).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.accidenteTransitoService.modificarVehiculoInvolucrado(this.modelRegistroVehiculo).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }

  }

  asignarValoresVehiculo() {
    let modelRegistroVehiculoEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroVehiculo = new VehiculoInvolucrado();
    this.modelRegistroVehiculo.id_accidente_transito = modelRegistroVehiculoEnvio.id_accidente_transito == null ? 0 : modelRegistroVehiculoEnvio.id_accidente_transito;
    this.modelRegistroVehiculo.id_situacion_vehiculo = modelRegistroVehiculoEnvio == null ? 0 : modelRegistroVehiculoEnvio.id_situacion_vehiculo;
    this.modelRegistroVehiculo.id_vehiculo_involucrado = modelRegistroVehiculoEnvio.id_vehiculo_involucrado == null ? 0 : modelRegistroVehiculoEnvio.id_vehiculo_involucrado;
    this.modelRegistroVehiculo.placa_vehiculo_involucrado = modelRegistroVehiculoEnvio.placa_vehiculo_involucrado == null ? "" :modelRegistroVehiculoEnvio.placa_vehiculo_involucrado;
    this.modelRegistroVehiculo.placa_remolque = modelRegistroVehiculoEnvio.placa_remolque == null ? "" : modelRegistroVehiculoEnvio.placa_remolque;
    this.modelRegistroVehiculo.id_tipo_vehiculo = modelRegistroVehiculoEnvio.id_tipo_vehiculo == null ? null : modelRegistroVehiculoEnvio.id_tipo_vehiculo;
    this.modelRegistroVehiculo.id_estado_soat = modelRegistroVehiculoEnvio.id_estado_soat == null ? null : modelRegistroVehiculoEnvio.id_estado_soat;
    this.modelRegistroVehiculo.id_compania_seguro = modelRegistroVehiculoEnvio.id_compania_seguro == null ? null : modelRegistroVehiculoEnvio.id_compania_seguro;
    this.modelRegistroVehiculo.id_estado_citv = modelRegistroVehiculoEnvio.id_estado_citv == null ? null : modelRegistroVehiculoEnvio.id_estado_citv;
    this.modelRegistroVehiculo.id_modalidad_transporte = modelRegistroVehiculoEnvio.id_modalidad_transporte == null ? null : modelRegistroVehiculoEnvio.id_modalidad_transporte; 
    this.modelRegistroVehiculo.otro_modalidad_transporte = modelRegistroVehiculoEnvio.otro_modalidad_transporte == null ? "" : modelRegistroVehiculoEnvio.otro_modalidad_transporte;
    this.modelRegistroVehiculo.id_estado_mod_transporte = modelRegistroVehiculoEnvio.id_estado_mod_transporte == null ? null : modelRegistroVehiculoEnvio.id_estado_mod_transporte;
    //this.modelRegistroVehiculo.id_servicio_transporte_persona = modelRegistroVehiculoEnvio.id_servicio_transporte_persona == null ? 0 : modelRegistroVehiculoEnvio.id_servicio_transporte_persona;
    //this.modelRegistroVehiculo.id_servicio_transporte_mercancia = modelRegistroVehiculoEnvio.id_servicio_transporte_mercancia == null ? 0 : modelRegistroVehiculoEnvio.id_servicio_transporte_mercancia;
    //this.modelRegistroVehiculo.id_material_peligroso = modelRegistroVehiculoEnvio.id_material_peligroso == null ? 0 : modelRegistroVehiculoEnvio.id_material_peligroso;
    //this.modelRegistroVehiculo.id_eventual_transporte_persona = modelRegistroVehiculoEnvio.id_eventual_transporte_persona == null ? 0 : modelRegistroVehiculoEnvio.id_eventual_transporte_persona;
    this.modelRegistroVehiculo.id_tarjeta_circulacion = modelRegistroVehiculoEnvio.id_tarjeta_circulacion == null ? null : modelRegistroVehiculoEnvio.id_tarjeta_circulacion;
    this.modelRegistroVehiculo.gps_sutran = modelRegistroVehiculoEnvio.gps_sutran == null ? false : modelRegistroVehiculoEnvio.gps_sutran;

    this.modelRegistroVehiculo.fecha_registro = modelRegistroVehiculoEnvio.fecha_registro == null ? null : new Date(modelRegistroVehiculoEnvio.fecha_registro);
    this.modelRegistroVehiculo.hora_registro = modelRegistroVehiculoEnvio.hora_registro == null ? null : modelRegistroVehiculoEnvio.hora_registro;
    this.modelRegistroVehiculo.otro_compania_seguro = modelRegistroVehiculoEnvio.otro_compania_seguro == null ? "" :modelRegistroVehiculoEnvio.otro_compania_seguro;
    this.modelRegistroVehiculo.latitud = modelRegistroVehiculoEnvio.latitud == null ? null :modelRegistroVehiculoEnvio.latitud;
    this.modelRegistroVehiculo.longitud = modelRegistroVehiculoEnvio.longitud == null ? null :modelRegistroVehiculoEnvio.longitud;
    this.modelRegistroVehiculo.activo = true;

    this.modelRegistroVehiculo.usuario_creacion = this.usuario;
    this.modelRegistroVehiculo.usuario_modificacion = this.usuario;

    //Fin
  }

  validarControles() : boolean{
    let vehiculo = Object.assign({}, this.formGroup.value);
    if (vehiculo.id_situacion_vehiculo == null || vehiculo.id_situacion_vehiculo == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Situación del Vehículo</b>", true, () => {
        this.selectSituacionVehiculo.focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_tipo_vehiculo == null || vehiculo.id_tipo_vehiculo == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Tipo de Vehículo</b>", true, () => {
        this.selectTipoVehiculo.focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.placa_vehiculo_involucrado == null || vehiculo.placa_vehiculo_involucrado == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>Placa</b>", true, () => {
        document.getElementById("placa_vehiculo_involucrado").focus();
      });
      return false;
    }
    if (vehiculo.id_tipo_vehiculo == 10 && (vehiculo.placa_remolque == null || vehiculo.placa_remolque == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>La placa del Remolque</b>", true, () => {
        document.getElementById("placa_remolque").focus();
      });
      return false;
    }

    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_estado_soat == null || vehiculo.id_estado_soat == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Estado SOAT</b>", true, () => {
        this.selectTipoEstadoSOAT.focus();
      });
      return false;
    }
    if (vehiculo.id_estado_soat!=3 && vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_compania_seguro == null || vehiculo.id_compania_seguro == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Compañía de SOAT</b>", true, () => {
        this.selectTipoCompania.focus();
      });
      return false;
    }
    if (vehiculo.id_compania_seguro == 7 && vehiculo.id_situacion_vehiculo!=2 && (vehiculo.otro_compania_seguro == null || vehiculo.otro_compania_seguro == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>La descripción de la otra compañía de SOAT</b>", true, () => {
        document.getElementById("otro_compania_seguro").focus();
      });
      return false;
    }

    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_estado_citv == null || vehiculo.id_estado_citv == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Estado de CITV</b>", true, () => {
        this.selectTipoCitv.focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_modalidad_transporte == null || vehiculo.id_modalidad_transporte == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Modalidad de Transporte</b>", true, () => {
        this.selectModalidadTransporte.focus();
      });
      return false;
    }
    if (vehiculo.id_modalidad_transporte == 16 && vehiculo.id_situacion_vehiculo!=2 && (vehiculo.otro_modalidad_transporte == null || vehiculo.otro_modalidad_transporte == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese <br><b>Otra Modalidad de Transporte</b>", true, () => {
        document.getElementById("otro_modalidad_transporte").focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_estado_mod_transporte == null || vehiculo.id_estado_mod_transporte == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Estado Modalidad</b>", true, () => {
        this.selectEstadoModalidad.focus();
      });
      return false;
    }
    /*if (vehiculo.id_servicio_transporte_persona == null || vehiculo.id_servicio_transporte_persona == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Servicio de Transporte de Personas</b>", true, () => {
        this.selectTipoServicioTransporte.focus();
      });
      return false;
    }*/
    /*if (vehiculo.id_servicio_transporte_mercancia == null || vehiculo.id_servicio_transporte_mercancia == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Servicio de Transporte Terrestre de Mercancias</b>", true, () => {
        this.selectTipoMercancias.focus();
      });
      return false;
    }*/
    /*if (vehiculo.id_material_peligroso == null || vehiculo.id_material_peligroso == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la <br><b>Autorización de Materiales Peligrosos</b>", true, () => {
        this.selectTipoMaterialesPeligrosos.focus();
      });
      return false;
    }*/
    /*if (vehiculo.id_eventual_transporte_persona == null || vehiculo.id_eventual_transporte_persona == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Permiso eventual para transporte de personas</b>", true, () => {
        this.selectTipoPermisoEventual.focus();
      });
      return false;
    }*/
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.id_tarjeta_circulacion == null || vehiculo.id_tarjeta_circulacion == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>Tarjeta de circulación</b>", true, () => {
        this.selectTipoTarjetaCirculacion.focus();
      });
      return false;
    }
    if (vehiculo.gps_sutran == null ) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el <br><b>GPS SUTRAN</b>", true, () => {
        //this.selectTipoTarjetaCirculacion.focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.gps_sutran == "true" || vehiculo.gps_sutran==true) && (vehiculo.latitud == null || vehiculo.latitud=="")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Latitud</b>", true, () => {
        document.getElementById("latitudgps").focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.gps_sutran == "true" ||  vehiculo.gps_sutran==true) && (vehiculo.longitud == null || vehiculo.longitud=="" )) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Longitud</b>", true, () => {
        document.getElementById("longitud").focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.gps_sutran == "true" ||  vehiculo.gps_sutran==true) && (vehiculo.fecha_registro == null || vehiculo.fecha_registro=="")  ) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Fecha de Registro</b>", true, () => {
        document.getElementById("fecha_registro").focus();
      });
      return false;
    }
    if (vehiculo.id_situacion_vehiculo!=2 && (vehiculo.gps_sutran == "true" ||  vehiculo.gps_sutran==true) && (vehiculo.hora_registro == null || vehiculo.hora_registro=="")  ) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <br><b>Hora de Registro</b>", true, () => {
        document.getElementById("hora_registro").focus();
      });
      return false;
    }
    return true;
  }

  closeModal(){
    this.modalRef.hide();
  }

}
