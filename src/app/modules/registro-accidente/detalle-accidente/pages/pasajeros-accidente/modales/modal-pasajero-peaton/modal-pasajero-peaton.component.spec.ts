import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPasajeroPeatonComponent } from './modal-pasajero-peaton.component';

describe('ModalPasajeroPeatonComponent', () => {
  let component: ModalPasajeroPeatonComponent;
  let fixture: ComponentFixture<ModalPasajeroPeatonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPasajeroPeatonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPasajeroPeatonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
