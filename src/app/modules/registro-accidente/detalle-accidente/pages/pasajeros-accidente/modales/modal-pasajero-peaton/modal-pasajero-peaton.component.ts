import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { BsModalRef, BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { isNullOrUndefined } from 'util';
import { PasajeroPeaton } from 'src/app/modules/shared/models/accidente/pasajeroPeaton';

@Component({
  selector: 'app-modal-pasajero-peaton',
  templateUrl: './modal-pasajero-peaton.component.html',
  styleUrls: ['./modal-pasajero-peaton.component.scss']
})
export class ModalPasajeroPeatonComponent implements OnInit {
  @ViewChild('ddllTipoPasajero', { static: false }) selectTipoPasajero: NgSelectComponent;
  @ViewChild('ddllTipoDocumento', { static: false }) selectTipoDocumento: NgSelectComponent;
  @ViewChild('ddlEstadoInicial', { static: false }) selectTipoEstadoInicial: NgSelectComponent;
  @ViewChild('ddlEstadoConfirmacion', { static: false }) selectTipoEstadoConfirmacion: NgSelectComponent;
  @ViewChild('ddlistaPlacas', { static: false }) selectTipoPlacas: NgSelectComponent;
  @ViewChild('ddllTipoSexo', { static: false }) selectTipoSexo: NgSelectComponent;
  @ViewChild('ddllSituacionPersona', { static: false }) selectTipoSituacion: NgSelectComponent;

  @Output() retornoValores = new EventEmitter();

  listaPersonaAccidente:any[];
  listaEstadoInicial:any[];
  listaEstadoConfirmacion:any[];
  listaTipoDocumento:any[];
  listaPlacas:any[];
  listaGenero : any[];
  listaSituacionPersona : any[];

  modelRegistroPasajeroPeaton: PasajeroPeaton;
  usuario;
  cambiarEditar = false;

  datosPasajeroPeatonInvolucrado

  ACCION: number = null;

  formGroup: FormGroup;
  bMostrar: boolean = false;

  numCantidad:number=8;

  showMostrarConfirmacion : boolean = false;
  showEdadAproximada : boolean = false;

  constructor(private fs : FacadeService,
    private fb: FormBuilder,
    public funciones : Funciones,
    public modalRef: BsModalRef,
    private ruta : ParametrosGeneralesServiceService) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarPasajeroPeatonControl();
    this.crearFormGroup();
  }

  crearFormGroup(){
    if(this.datosPasajeroPeatonInvolucrado==null){
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_peaton:null,
        dni:null,
        id_pasajero_vehiculo:null,
        nombre:null,
        apellido_paterno:null,
        apellido_materno:null,
        id_estado_inicial:null,
        id_estado_confirmado:null,
        id_tipo_persona_accidente:null,
        id_vehiculo_involucrado:null,
        id_tipo_documento:1,
        fecha_nacimiento:null,
        id_tipo_sexo:null,
        id_ocupante_vehiculo:null,
        id_situacion_pea_pas_ocu:null,
        edad_aprox:null,
      })
    }else{
      this.formGroup = this.fb.group({
        id_accidente_transito:this.ruta.getRastro().accidente.p_envio_id_accidente_transito,
        id_pasajero_vehiculo:this.datosPasajeroPeatonInvolucrado.id_pasajero_vehiculo == null ? 0 : this.datosPasajeroPeatonInvolucrado.id_pasajero_vehiculo,
        id_peaton:this.datosPasajeroPeatonInvolucrado.id_peaton == null ? 0 : this.datosPasajeroPeatonInvolucrado.id_peaton,
        id_ocupante_vehiculo: this.datosPasajeroPeatonInvolucrado.id_ocupante_vehiculo == null ? 0 : this.datosPasajeroPeatonInvolucrado.id_ocupante_vehiculo,
        id_conductor_vehiculo:this.datosPasajeroPeatonInvolucrado.id_conductor_vehiculo == null ? 0 : this.datosPasajeroPeatonInvolucrado.id_conductor_vehiculo,
        dni:this.datosPasajeroPeatonInvolucrado.dni == null ? "" : this.datosPasajeroPeatonInvolucrado.dni,
        nombre:this.datosPasajeroPeatonInvolucrado.nombre == null ? "" : this.datosPasajeroPeatonInvolucrado.nombre,
        apellido_paterno:this.datosPasajeroPeatonInvolucrado.apellido_paterno == null ? "" : this.datosPasajeroPeatonInvolucrado.apellido_paterno,
        apellido_materno:this.datosPasajeroPeatonInvolucrado.apellido_materno == null ? "" : this.datosPasajeroPeatonInvolucrado.apellido_materno,
        id_estado_confirmado:this.datosPasajeroPeatonInvolucrado.id_estado_confirmado == null ? null : this.datosPasajeroPeatonInvolucrado.id_estado_confirmado,
        id_estado_inicial : this.datosPasajeroPeatonInvolucrado.id_estado_inicial == null ? null : this.datosPasajeroPeatonInvolucrado.id_estado_inicial,
        id_tipo_persona_accidente:this.datosPasajeroPeatonInvolucrado.id_tipo_persona_accidente == null ? null : this.datosPasajeroPeatonInvolucrado.id_tipo_persona_accidente,
        id_vehiculo_involucrado:this.datosPasajeroPeatonInvolucrado.id_vehiculo_involucrado == null ? null : this.datosPasajeroPeatonInvolucrado.id_vehiculo_involucrado,
        id_tipo_documento:this.datosPasajeroPeatonInvolucrado.id_tipo_documento == null ? null : this.datosPasajeroPeatonInvolucrado.id_tipo_documento,
        fecha_nacimiento:this.datosPasajeroPeatonInvolucrado.fecha_nacimiento == null ? null : new Date(this.datosPasajeroPeatonInvolucrado.fecha_nacimiento),
        id_tipo_sexo:this.datosPasajeroPeatonInvolucrado.id_tipo_sexo == null ? null : this.datosPasajeroPeatonInvolucrado.id_tipo_sexo,
        id_situacion_pea_pas_ocu: this.datosPasajeroPeatonInvolucrado.id_situacion_pea_pas_ocu == null ? null : this.datosPasajeroPeatonInvolucrado.id_situacion_pea_pas_ocu,
        edad_aprox : this.datosPasajeroPeatonInvolucrado.edad_aprox == null ? null : this.datosPasajeroPeatonInvolucrado.edad_aprox,
      })

      if(this.datosPasajeroPeatonInvolucrado.id_tipo_documento==2){
        this.tipoDocumentoCadena="Carnet Extranjeria";
        this.numCantidad=12;
      }else{
        this.tipoDocumentoCadena="DNI";
        this.numCantidad=8;
      }
      if(this.datosPasajeroPeatonInvolucrado.id_estado_inicial==2){
        this.showMostrarConfirmacion=true;
      }else{
        this.showMostrarConfirmacion=false;
      }
      if(this.datosPasajeroPeatonInvolucrado.id_situacion_pea_pas_ocu ==2){
        this.showEdadAproximada=true;
      }else{
        this.showEdadAproximada=false;
      }

    }
  }

  listarPasajeroPeatonControl(){
    this.fs.accidenteTransitoService.listarPasajeroPeatonControl().subscribe(
      (data:any)=>{
        this.listaPersonaAccidente=data.tipopersonaaccidente;
        this.listaEstadoInicial=data.estadoinicial;
        this.listaEstadoConfirmacion=data.estadoconfirmado;
        this.listaTipoDocumento=data.tipodocumento;
        this.listaGenero=data.tiposexo;
        this.listaSituacionPersona = data.situacionpeapasocu;
      }
    )
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito}
    this.fs.accidenteTransitoService.listarVehiculoInvolucradoCombo(param).subscribe(
      (data:any)=>{
        this.listaPlacas=data.listavehiculosinvolucrados;
      }
    )
  }

  tipoDocumentoCadena:string="DNI";
  validarDocumento(evento){
    if(evento!=undefined){
      this.tipoDocumentoCadena=evento.descripcion;
      if(evento.id_tipo_documento==2){
        this.numCantidad=12;
        //this.tipoDocumentoCadena="Carnet Extranjeria";
      }else{
        this.numCantidad=8;
        //this.tipoDocumentoCadena="DNI";
      }
    }
  }

  validarEdad(evento){
    if(evento!=undefined){
      if(evento.id_situacion_pea_pas_ocu==2){
        this.showEdadAproximada=true;
      }else{
        this.showEdadAproximada=false;
      }
    }
  }

  validarEstadoConfirmacion(evento){
    if(evento!=undefined){
      if(evento.id_estado_inicial==2){
        this.showMostrarConfirmacion=true;
      }else{
        this.showMostrarConfirmacion=false;
        this.formGroup.patchValue({
          id_estado_confirmado:null,
        })
      }
    }
  }

  validarDNI() {
    const valDni = this.formGroup.get('dni').value;
    if (!isNullOrUndefined(valDni) && valDni.length == 8) {
      //this.validarInformacionReniec();
    } else {
      this.formGroup.patchValue({
        nombre: null,
        apellido_paterno: null,
        apellido_materno: null,

      });
      //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
      //imagen.src = '';
    }
  }

  validarInformacionReniec() {
    let valDni = this.formGroup.get('dni').value;
    let cant = 0;
    if (valDni == '' || valDni == null) {
      document.getElementById('dni').focus();
      this.funciones.mensaje('info', 'Debe ingresar el N° de Dni a validar.');
    } else {
      if (cant == 0) {
        this.fs.dataExternaService.consultarInformacionReniec(valDni).subscribe(
          data => {
            const response = data as any;
            if (data != null && data != '') {
              this.formGroup.patchValue({
                nombre: response.strnombres,
                apellido_paterno: response.strapellidopaterno,
                apellido_materno: response.strapellidomaterno,
              });
              //this.foto = response.strfoto;
              //const imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = response.strfoto;
            } else {
              this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
              this.formGroup.patchValue({
                nombre: null,
                apellido_paterno: null,
                apellido_materno: null,
              });
              //let imagen: HTMLImageElement = document.getElementsByName('imgFoto')[0] as HTMLImageElement;
              //imagen.src = '';
            }
          },
          error => {
            this.funciones.mensaje('info', 'No se encontró información del Dni ingresado.');
            this.formGroup.patchValue({
              nombre: null,
              apellido_paterno: null,
              apellido_materno: null,
            });
          }
        );
      } else {
        this.funciones.mensaje('info', 'El DNI ingresado ya se encuentra registrado en el sistema.');
      }
    }
  }

  registrarPasajeroPeaton(){
    if (this.Validar()) {
      this.asignarValorePasajeroPeaton();
      if (this.datosPasajeroPeatonInvolucrado == null) {
        this.bMostrar = true;
        this.fs.accidenteTransitoService.insertarPasajeroPeaton(this.modelRegistroPasajeroPeaton).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      } else {
        this.fs.accidenteTransitoService.modificarPasajeroPeaton(this.modelRegistroPasajeroPeaton).subscribe
          (data => {
            let respuesta = data as any;
            if (respuesta.resultado > 0) {
              this.funciones.mensaje("success", this.funciones.mostrarMensaje("insertar", ""));
              this.retornoValores.emit(0);
              this.modalRef.hide();
            }
            else {
              this.funciones.mensaje("info", this.funciones.mostrarMensaje("error", ""));
            }
            this.bMostrar = false;
          });
      }
    }
  }

  Validar(): boolean {
    let conductor = Object.assign({}, this.formGroup.value);
    if (conductor.id_situacion_pea_pas_ocu == null || conductor.id_situacion_pea_pas_ocu == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione la<br><b>Situación de la Persona </b>", true, () => {
        this.selectTipoSituacion.focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu==2 && (conductor.edad_aprox == null || conductor.edad_aprox == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione ingrese la<br><b>Edad Aproximada </b>", true, () => {
        document.getElementById("edad_aprox").focus();
      });
      return false;
    }
    if (conductor.id_tipo_persona_accidente == null || conductor.id_tipo_persona_accidente == 0) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Pasajero / Peaton</b>", true, () => {
        this.selectTipoPasajero.focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.id_tipo_documento == null || conductor.id_tipo_documento == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Tipo Documento</b>", true, () => {
        this.selectTipoDocumento.focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.dni == null || conductor.dni == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>DNI</b>", true, () => {
        document.getElementById("dni").focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.fecha_nacimiento == null || conductor.fecha_nacimiento == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese la <b>Fecha de Nacimiento</b>", true, () => {
        document.getElementById("fecha_nacimiento").focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.id_tipo_sexo == null || conductor.id_tipo_sexo == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Sexo</b>", true, () => {
        this.selectTipoSexo.focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.nombre == null || conductor.nombre == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Nombre del conductor</b>", true, () => {
        document.getElementById("nombre").focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.apellido_paterno == null || conductor.apellido_paterno == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Paterno del conductor</b>", true, () => {
        document.getElementById("apellido_paterno").focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.apellido_materno == null || conductor.apellido_materno == "")) {
      this.funciones.alertaSimple("info", "", "Por favor ingrese el <b>Apellido Materno</b>", true, () => {
        document.getElementById("apellido_materno").focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 &&  (conductor.id_estado_inicial == null || conductor.id_estado_inicial == 0)) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado</b>", true, () => {
        this.selectTipoEstadoInicial.focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 && (conductor.id_estado_inicial==2 && (conductor.id_estado_confirmado == null || conductor.id_estado_confirmado == 0))) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Estado Confirmación</b>", true, () => {
        this.selectTipoEstadoConfirmacion.focus();
      });
      return false;
    }
    if (conductor.id_situacion_pea_pas_ocu!=2 &&  (conductor.id_tipo_persona_accidente==1 && (conductor.id_vehiculo_involucrado == null || conductor.id_vehiculo_involucrado == 0))) {
      this.funciones.alertaSimple("info", "", "Por favor seleccione el<br><b>Placa de automóvil del conductor</b>", true, () => {
        this.selectTipoPlacas.focus();
      });
      return false;
    }
    return true;
  }

  asignarValorePasajeroPeaton(){
    let modelRegistroPasajeroPeatonEnvio = Object.assign({}, this.formGroup.value);
    this.modelRegistroPasajeroPeaton = new PasajeroPeaton();
    this.modelRegistroPasajeroPeaton.id_pasajero_vehiculo = modelRegistroPasajeroPeatonEnvio.id_pasajero_vehiculo == null ? 0 : modelRegistroPasajeroPeatonEnvio.id_pasajero_vehiculo;
    this.modelRegistroPasajeroPeaton.id_accidente_transito = modelRegistroPasajeroPeatonEnvio.id_accidente_transito == null ? 0 : modelRegistroPasajeroPeatonEnvio.id_accidente_transito;
    this.modelRegistroPasajeroPeaton.id_tipo_persona_accidente = modelRegistroPasajeroPeatonEnvio.id_tipo_persona_accidente == null ? null : modelRegistroPasajeroPeatonEnvio.id_tipo_persona_accidente;
    this.modelRegistroPasajeroPeaton.id_peaton = modelRegistroPasajeroPeatonEnvio.id_peaton == null ? 0 : modelRegistroPasajeroPeatonEnvio.id_peaton;
    this.modelRegistroPasajeroPeaton.id_ocupante_vehiculo = modelRegistroPasajeroPeatonEnvio.id_ocupante_vehiculo == null ? 0 : modelRegistroPasajeroPeatonEnvio.id_ocupante_vehiculo;
    this.modelRegistroPasajeroPeaton.id_tipo_documento = modelRegistroPasajeroPeatonEnvio.id_tipo_documento == null ? null : modelRegistroPasajeroPeatonEnvio.id_tipo_documento;
    this.modelRegistroPasajeroPeaton.dni = modelRegistroPasajeroPeatonEnvio.dni == null ? "" :modelRegistroPasajeroPeatonEnvio.dni;
    this.modelRegistroPasajeroPeaton.nombre = modelRegistroPasajeroPeatonEnvio.nombre == null ? "" : modelRegistroPasajeroPeatonEnvio.nombre;
    this.modelRegistroPasajeroPeaton.apellido_paterno = modelRegistroPasajeroPeatonEnvio.apellido_paterno == null ? "" : modelRegistroPasajeroPeatonEnvio.apellido_paterno;
    this.modelRegistroPasajeroPeaton.apellido_materno = modelRegistroPasajeroPeatonEnvio.apellido_materno == null ? "" : modelRegistroPasajeroPeatonEnvio.apellido_materno;
    this.modelRegistroPasajeroPeaton.id_estado_inicial = modelRegistroPasajeroPeatonEnvio.id_estado_inicial == null ? null : modelRegistroPasajeroPeatonEnvio.id_estado_inicial;
    this.modelRegistroPasajeroPeaton.id_estado_confirmado = modelRegistroPasajeroPeatonEnvio.id_estado_confirmado == null ? null : modelRegistroPasajeroPeatonEnvio.id_estado_confirmado;
    this.modelRegistroPasajeroPeaton.id_vehiculo_involucrado = modelRegistroPasajeroPeatonEnvio.id_vehiculo_involucrado == null ? null : modelRegistroPasajeroPeatonEnvio.id_vehiculo_involucrado;
    this.modelRegistroPasajeroPeaton.fecha_nacimiento = modelRegistroPasajeroPeatonEnvio.fecha_nacimiento == null ? null : new Date(modelRegistroPasajeroPeatonEnvio.fecha_nacimiento);
    this.modelRegistroPasajeroPeaton.id_tipo_sexo = modelRegistroPasajeroPeatonEnvio.id_tipo_sexo == null ? null : modelRegistroPasajeroPeatonEnvio.id_tipo_sexo;
    this.modelRegistroPasajeroPeaton.id_situacion_pea_pas_ocu = modelRegistroPasajeroPeatonEnvio.id_situacion_pea_pas_ocu == null ? null : modelRegistroPasajeroPeatonEnvio.id_situacion_pea_pas_ocu;
    this.modelRegistroPasajeroPeaton.edad_aprox = modelRegistroPasajeroPeatonEnvio.edad_aprox == null ? null : modelRegistroPasajeroPeatonEnvio.edad_aprox;
    this.modelRegistroPasajeroPeaton.activo = true;
    this.modelRegistroPasajeroPeaton.usuario_creacion = this.usuario;
    this.modelRegistroPasajeroPeaton.usuario_modificacion = this.usuario;
  }

  closeModal(){
    this.modalRef.hide();
  }

}
