import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasajerosAccidenteComponent } from './pasajeros-accidente.component';

describe('PasajerosAccidenteComponent', () => {
  let component: PasajerosAccidenteComponent;
  let fixture: ComponentFixture<PasajerosAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasajerosAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasajerosAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
