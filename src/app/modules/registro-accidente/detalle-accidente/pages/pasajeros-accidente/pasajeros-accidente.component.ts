import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametrosGeneralesServiceService } from 'src/app/core/services/parametros-generales-service.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { ModalPasajeroPeatonComponent } from './modales/modal-pasajero-peaton/modal-pasajero-peaton.component';

@Component({
  selector: 'app-pasajeros-accidente',
  templateUrl: './pasajeros-accidente.component.html',
  styleUrls: ['./pasajeros-accidente.component.scss']
})
export class PasajerosAccidenteComponent implements OnInit {

  listaPasajeros:any[];
  totalRegistros:number=0;

  config;
  bsModalRef: BsModalRef;
  usuario;

  numero_Pagina: number = 0;
  numPaginasMostrar: number = 5;

  @Output() retornoValores = new EventEmitter();

  constructor(private fs:FacadeService,
    private modalService: BsModalService,
    private router: Router,
    private ruta:ParametrosGeneralesServiceService,
    private funciones : Funciones) { }

  ngOnInit() {
    this.usuario=sessionStorage.getItem("Usuario");
    this.listarPasajero(this.numPaginasMostrar,this.numero_Pagina);
  }

  listarPasajero(numero_Pagina: number, numPaginasMostrar: number){
    let param={"id_accidente_transito":this.ruta.getRastro().accidente.p_envio_id_accidente_transito,"skip":numPaginasMostrar,"take":numero_Pagina}
    this.fs.accidenteTransitoService.listarPasajeroPeaton(param).subscribe(
      (data:any)=>{
        if(data.cantidad>0){
          this.listaPasajeros=data.listapasajeropeaton;
          this.totalRegistros=data.cantidad;
        }else{
          this.listaPasajeros=[]
        }
      }
    )
  }

  openModalPasajeroPeaton(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:1,
        datosPasajeroPeatonInvolucrado:null,
      }
    };
    this.bsModalRef = this.modalService.show(ModalPasajeroPeatonComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarPasajero(this.numPaginasMostrar,this.numero_Pagina);
      }
    );
  }

  openEditarPasajeroPeaton(pasajero){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-registrar-conductor',
      initialState: {
        ACCION:2,
        datosPasajeroPeatonInvolucrado: pasajero
      }
    };
    this.bsModalRef = this.modalService.show(ModalPasajeroPeatonComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.listarPasajero(this.numPaginasMostrar,this.numero_Pagina);
      }
    );
  }

  anularPasajeroPeaton(pasajero){
    this.funciones.alertaRetorno("question","","¿Está seguro que desea eliminar este registro?",true,(rpta)=>{
      if(rpta.value){
        let parametro={"id_tipo_persona_accidente":pasajero.id_tipo_persona_accidente,"id_pasajero_vehiculo":pasajero.id_pasajero_vehiculo,
        "id_peaton":pasajero.id_peaton, "usuario_eliminacion":this.usuario};
        this.fs.accidenteTransitoService.anularPasajeroPeaton(parametro).subscribe(
          (data:any)=>{
          if(data.resultado>0){
            this.funciones.mensaje("success", this.funciones.mostrarMensaje("eliminacion", ""));
            this.listarPasajero(this.numPaginasMostrar,this.numero_Pagina);
          }else {
            this.funciones.mensaje("warning", this.funciones.mostrarMensaje("error", ""));
          }
        });      
      }
    });
  }

  paginaActiva: number = 0;
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numero_Pagina = this.paginaActiva;
    this.listarPasajero(this.numPaginasMostrar,this.numero_Pagina);
  }

}
