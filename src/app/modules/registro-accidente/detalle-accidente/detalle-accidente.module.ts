import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { DetalleAccidenteComponent } from './detalle-accidente.component';
import { VehiculosInvolucradosComponent } from './pages/vehiculos-involucrados/vehiculos-involucrados.component';
import { ConductorAccidenteComponent } from './pages/conductor-accidente/conductor-accidente.component';
import { PasajerosAccidenteComponent } from './pages/pasajeros-accidente/pasajeros-accidente.component';
import { ModalVehiculoInvolucradoComponent } from './pages/vehiculos-involucrados/modales/modal-vehiculo-involucrado/modal-vehiculo-involucrado.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalConductorComponent } from './pages/conductor-accidente/modales/modal-conductor/modal-conductor.component';
import { ModalPasajeroPeatonComponent } from './pages/pasajeros-accidente/modales/modal-pasajero-peaton/modal-pasajero-peaton.component';
import { SituacionViaComponent } from './pages/situacion-via/situacion-via.component';
import { ModalSituacionViaComponent } from './pages/situacion-via/modales/modal-situacion-via/modal-situacion-via.component';

const routes: Routes = [{ path: '', component: DetalleAccidenteComponent }];

@NgModule({
  declarations: [DetalleAccidenteComponent, VehiculosInvolucradosComponent, ConductorAccidenteComponent, PasajerosAccidenteComponent, ModalVehiculoInvolucradoComponent, ModalConductorComponent, ModalPasajeroPeatonComponent, SituacionViaComponent, ModalSituacionViaComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalVehiculoInvolucradoComponent, 
    ModalConductorComponent,
    ModalPasajeroPeatonComponent,
    ModalSituacionViaComponent
  ]
})
export class DetalleAccidenteModule { }
