import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleAccidenteComponent } from './detalle-accidente.component';

describe('DetalleAccidenteComponent', () => {
  let component: DetalleAccidenteComponent;
  let fixture: ComponentFixture<DetalleAccidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleAccidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleAccidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
