import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { UiSwitchModule } from 'ngx-ui-switch';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgSelectModule } from '@ng-select/ng-select';
import { TreeviewModule } from 'ngx-treeview';
import { InputFileComponent } from './components/input-file/input-file.component';
import { HasClaimDirective } from 'src/app/core/guards/has-claim.directive';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [InputFileComponent,HasClaimDirective],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,   
    AccordionModule.forRoot(),
    PaginationModule.forRoot(), 
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    UiSwitchModule,
    CollapseModule,
    NgSelectModule,
    TooltipModule.forRoot(),
    TreeviewModule.forRoot(),
    NgxMaterialTimepickerModule,
    PDFExportModule,
    TextMaskModule,
  ],
  exports:[
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    PaginationModule,
    AccordionModule,
    UiSwitchModule,
    CollapseModule,
    BsDatepickerModule,
    InputFileComponent,
    TooltipModule,
    TreeviewModule,
    HasClaimDirective,
    TimepickerModule,
    NgxMaterialTimepickerModule,
    PDFExportModule,
    TextMaskModule
  ]
})
export class SharedModule { }
