
export class PasajeroPeaton {
    public id_tipo_persona_accidente :number = 0;
    public id_peaton : number =0;
    public id_ocupante_vehiculo : number = 0;
    public id_pasajero_vehiculo : number = 0;
    public id_accidente_transito : number = 0;
    public id_vehiculo_involucrado : number = 0;
    public id_tipo_documento : number = 0;
    public dni : string = "";
    public nombre : string = "";
    public apellido_paterno : string = "";
    public apellido_materno : string = "";
    public id_estado_inicial : number = 0;
    public id_estado_confirmado : number = 0;
    public fecha_nacimiento :  Date | String = null;
    public id_tipo_sexo : number = 0;
    public id_situacion_pea_pas_ocu : number = 0;
    public edad_aprox : number =0;

    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
    
}