import { Time } from '@angular/common';

export class PrimeraAtencion {
    public id_accidente_transito:number = 0;
    public id_atencion_primaria:number = 0;
    public id_tipo_unidad_atencion : number=0;
    public id_tipo_documento : number = 0;
    public dni_responsable : string = "";
    public nombre_responsable : string = "";
    public apellido_pat_responsable : string = "";
    public apellido_mat_responsable : string = "";
    public telefono : string = "";
    public id_persona_accidente:number = 0;
    public id_ipress: number = 0;
    public fecha_registro:  Date | String = null;
    public hora_registro : Time = null;
    public id_estado_inicial_atencion: number = 0;
    //public id_estado_final_atencion: number = 0;
    //public lugar_derivacion : string = "";
    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    public persona :Persona;
    
}

 export class Persona{
    public id_tipo_persona_accidente:number = 0;
    public id_persona_accidente:number = 0;
    public id_peaton:number = 0;
    public id_pasajero_vehiculo:number = 0;
    public id_conductor_vehiculo:number = 0;
    public id_accidente_transito:number = 0;
    public id_ocupante_vehiculo : number = 0;
    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}