import { Time } from '@angular/common';

export class VehiculoInvolucrado {
    public id_accidente_transito:number = 0;
    public id_situacion_vehiculo : number = 0;
    public id_vehiculo_involucrado: number = 0;
    public placa_vehiculo_involucrado : string = "";
    public placa_remolque : string = "";
    public otro_compania_seguro : string = "";
    public id_tipo_vehiculo: number = 0;
    public id_estado_soat: number = 0;
    public id_compania_seguro: number = 0;
    public id_estado_citv: number = 0;
    public id_modalidad_transporte : number = 0;
    public otro_modalidad_transporte : string = ""; 
    public id_estado_mod_transporte : number = 0;
    //public id_servicio_transporte_persona: number = 0;
    //public id_servicio_transporte_mercancia: number = 0;
    //public id_material_peligroso: number = 0;
    //public id_eventual_transporte_persona: number = 0;
    public id_tarjeta_circulacion: number = 0;
    public gps_sutran : boolean=false;

    public fecha_registro:  Date | String = null;
    public hora_registro : Time = null;

    public latitud : string = "";
    public longitud : string = "";

    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}