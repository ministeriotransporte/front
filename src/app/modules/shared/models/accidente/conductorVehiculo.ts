
export class ConductorVehiculo {
    public id_conductor_vehiculo:number = 0;
    public id_tipo_conductor : number = 0;
    public id_tipo_documento : number = 0;
    public dni : string = "";
    public nombre : string = "";
    public apellido_paterno : string = "";
    public apellido_materno : string = "";
    public id_estado_confirmado:number = 0;
    public id_estado_licencia: number = 0;
    public punto_firme : number = 0;
    public id_tipo_dosaje_etilico: number = 0;
    public id_resultado_dosaje_etilico: number = 0;
    public id_estado_conductor: number = 0;
    public id_estado_confirmacion: number = 0;
    public id_vehiculo_involucrado: number = 0;
    public sancion: boolean = false;
    public fecha_nacimiento :  Date | String = null;
    public id_tipo_sexo : number = 0;

    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}