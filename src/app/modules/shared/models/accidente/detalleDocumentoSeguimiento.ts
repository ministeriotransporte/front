export class detalleDocumentoSeguimiento{
    public id_seg_post_accidente_archivo:number;
    public id_seguimiento_post_accidente: number;
    public nombre_documento:string;
    public activo:boolean;
    public descripcion : string;
    public nombre_documento_original : string;
    public usuario_modificacion:string;
}