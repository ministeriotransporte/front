export class SegundaAtencion {
    public id_accidente_transito:number = 0;
    public id_atencion_secundaria:number = 0;
    public id_ipress : number=0;
    public cantidad_heridos_ingresados: number = 0;
    public soat_activo : boolean=false;
    public id_compania_seguro : number =0;
    public cantidad_dados_alta : number = 0;
    public otro_compania_seguro : string = "";
    public cantidad_fallecido_intrahospitalario : number = 0;
    public cantidad_derivado_rehabilitacion : number = 0;

    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}