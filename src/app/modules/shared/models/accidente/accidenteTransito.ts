import { Time } from '@angular/common';

export class AccidenteTransito {
    public id_accidente_transito:number = 0;
    public cod_accidente_transito : string = "";
    public cod_acc_transito_externo : string = "";
    public fecha_registro:  Date | String = null;
    public hora_registro : Time = null;
    public id_clase_accidente: number = 0;
    public otro_clase_accidente : string = "";
    public direccion : string = "";
    public nombre_documento : string = "";
    public nombre_documento_original : string = "";
    public descripcion :string = "";
    public es_por_mapa : boolean = true;
    public latitud : string = "";
    public longitud : string = "";
    public iddpto:number = 0;
    public idprov:number = 0;
    public iddist:number = 0;
    public detalle_hechos : string = "";
    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}