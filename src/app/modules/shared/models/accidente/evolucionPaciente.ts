import { Time } from '@angular/common';
export class EvolucionPaciente {
    public id_atencion_primaria : number= 0;
    public id_atencion_primaria_detalle :number = 0;
    public fecha_egreso_hospital :  Date | String = null;
    public hora_egreso_hospital : Time = null;
    public id_estado_final_atencion : number = 0;
    public lugar_derivacion : string = "";
    public id_situacion_lesionado : number = 0;

    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}