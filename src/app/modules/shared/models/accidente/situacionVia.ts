
export class SituacionVia {
    public id_situacion_via : number= 0;
    public id_accidente_transito :number = 0;
    public id_tipo_via :number = 0;
    public otro_tipo_via : string = "";
    public id_tipo_zona : number =0;
    public id_tipo_transitabilidad : number = 0;

    public activo : boolean = true;
    public usuario_creacion: string="";
    public usuario_modificacion: string="";
    
}