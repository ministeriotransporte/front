export class FiltrosRegistroAccidente {
    public cod_accidente_transito: string = null;
    public cod_acc_transito_externo : string = null;
    public fecha_inicial : string = null;
    public fecha_final : string = null;
    public id_clase_accidente: number  = null;
    public otros_clase_accidente: string = null;
 
    public num_filas: number = 10;
    public num_pagina: number = 0;
    public indice: number = 1;
    public orden: number = 0;
}