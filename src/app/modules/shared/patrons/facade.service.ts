import { Injectable, Injector } from '@angular/core';
import { MantenimientoMenuService } from '../services/mantenimiento/menu.service';
import { MaestraService } from '../services/maestra.service';
import { UsuarioService } from '../services/mantenimiento/usuario.service';
import { DataExternaService } from '../services/data-externa.service';
import { MantenimientoPerfilService } from '../services/mantenimiento/perfil.service';
import { MantenimientoComponenteService } from '../services/mantenimiento/componente.service';
import { AccidenteTransitoService } from '../services/accidente/accidente-transito.service';
import { PrincipalService } from 'src/app/core/http/principal.service';
import { PrimeraAtencionService } from '../services/accidente/primera-atencion.service';
import { SegundaAtencionService } from '../services/accidente/segunda-atencion.service';
import { PostSeguimientoService } from '../services/accidente/post-seguimiento.service';
import { AccidenteTransitoFuratService } from '../services/accidente/accidente-transito-furat.service';
import { VehiculoFuratService } from '../services/accidente/vehiculo-furat.service';
import { PersonaFuratService } from '../services/accidente/persona-furat.service';

@Injectable({
  providedIn: 'root'
})
export class FacadeService {
  constructor(private injector: Injector) { }
  private _mantenimientoMenuService: MantenimientoMenuService;
  private _maestraService: MaestraService;
  private _usuarioService: UsuarioService;
  private _dataExternaService: DataExternaService;
  private _mantenimientoPerfilService: MantenimientoPerfilService;
  private _mantenimientoComponenteService: MantenimientoComponenteService;
  private _accidenteTransitoService : AccidenteTransitoService;
  private _principalService: PrincipalService;
  private _primeraAtencionService: PrimeraAtencionService;
  private _segundaAtencionService: SegundaAtencionService;
  private _postSeguimientoService : PostSeguimientoService;
  private _accidenteTransitoFuratService : AccidenteTransitoFuratService;
  private _vehiculoFuratService : VehiculoFuratService;
  private _personaFuratService : PersonaFuratService;

  public get PrincipalService(): PrincipalService { 
    if (!this._principalService) {
        this._principalService = this.injector.get(PrincipalService);
    }
    return this._principalService;
  }
  
  public get mantenimientoMenuService(): MantenimientoMenuService {
    if (!this._mantenimientoMenuService) {
      this._mantenimientoMenuService = this.injector.get(MantenimientoMenuService);
    }
    return this._mantenimientoMenuService;
  }
  public get maestraService(): MaestraService {
    if (!this._maestraService) {
      this._maestraService = this.injector.get(MaestraService);
    }
    return this._maestraService;
  }

  public get usuarioService(): UsuarioService {
    if (!this._usuarioService) {
      this._usuarioService = this.injector.get(UsuarioService);
    }
    return this._usuarioService;
  }

  public get dataExternaService(): DataExternaService {
    if (!this._dataExternaService) {
      this._dataExternaService = this.injector.get(DataExternaService);
    }
    return this._dataExternaService;
  }

  public get mantenimientoPerfilService(): MantenimientoPerfilService {
    if (!this._mantenimientoPerfilService) {
      this._mantenimientoPerfilService = this.injector.get(MantenimientoPerfilService);
    }
    return this._mantenimientoPerfilService;
  }

  public get mantenimientoComponenteService(): MantenimientoComponenteService {
    if (!this._mantenimientoComponenteService) {
      this._mantenimientoComponenteService = this.injector.get(MantenimientoComponenteService);
    }
    return this._mantenimientoComponenteService;
  }

  public get accidenteTransitoService(): AccidenteTransitoService {
    if (!this._accidenteTransitoService) {
      this._accidenteTransitoService = this.injector.get(AccidenteTransitoService);
    }
    return this._accidenteTransitoService;
  }

  public get primeraAtencionService(): PrimeraAtencionService {
    if (!this._primeraAtencionService) {
      this._primeraAtencionService = this.injector.get(PrimeraAtencionService);
    }
    return this._primeraAtencionService;
  }

  public get segundaAtencionService(): SegundaAtencionService {
    if (!this._segundaAtencionService) {
      this._segundaAtencionService = this.injector.get(SegundaAtencionService);
    }
    return this._segundaAtencionService;
  }

  public get postSeguimientoService(): PostSeguimientoService {
    if (!this._postSeguimientoService) {
      this._postSeguimientoService = this.injector.get(PostSeguimientoService);
    }
    return this._postSeguimientoService;
  }

  public get accidenteTransitoFuratService(): AccidenteTransitoFuratService {
    if (!this._accidenteTransitoFuratService) {
      this._accidenteTransitoFuratService = this.injector.get(AccidenteTransitoFuratService);
    }
    return this._accidenteTransitoFuratService;
  }

  public get vehiculoFuratService(): VehiculoFuratService {
    if (!this._vehiculoFuratService) {
      this._vehiculoFuratService = this.injector.get(VehiculoFuratService);
    }
    return this._vehiculoFuratService;
  }

  public get personaFuratService(): PersonaFuratService {
    if (!this._personaFuratService) {
      this._personaFuratService = this.injector.get(PersonaFuratService);
    }
    return this._personaFuratService;
  }

}