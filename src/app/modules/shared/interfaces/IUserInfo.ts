import { IPerfil } from './IPerfil';

export interface IUserInfo {
    id_usuario: string;
    usuario: string;
    nombre: string;
    apellido_paterno: string;
    apellido_materno: string;
    cambio_clave: string;
    cod_perfil: string;
    fecha_modificacion: string;
    id_perfil: number;
    perfil: string;
    access_token: string;
    //componentes: IPerfil[];
    componentes: string;
    //menus: IMenusPerfil[];
    menus:string;
    telefono: ITelefonoUsuario[];
    correo: ICorreosUsuario[];
    departamento: IDepartamentoUsuario[];
    //modulo:             string;
}

export interface IComponentesPerfil {
    Id_Perfil_Componente: number;
    Id_Perfil: number;
    Nombre_Componente: string;
    Visible: boolean;
}
export interface IMenusPerfil {
    Id_Menu: number;
    Nombre_Menu: string;
    Url: string;
    Nivel: number;
    Orden: number;
    Id_Menu_Padre: number;
    Icono: string;
    Visible: boolean;
}
export interface ICorreosUsuario {
    id_usuario: number;
    correo_electronico: string;
}
export interface ITelefonoUsuario {
    id_usuario: number;
    numero_telefono: string;
}
export interface IDepartamentoUsuario {
    coddepa: string;
    departamento: string;
}
