import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FacadeService } from '../../patrons/facade.service';
import { Funciones } from '../../functions/funciones';
import { environment } from 'src/environments/environment';
import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.scss']
})
export class InputFileComponent implements OnInit {

  @Input() idComp: string;
  @Input() Value: string;
  @Input() MensajeMaxSize: string;
  @Input() Extensiones: string;
  @Input() IdTipoArchivo: number;
  @Output() onChange = new EventEmitter();
  @Input() RutaDescarga: string=null;
  @Input() Descargable: boolean=false;
  sePuedeDescargar:boolean=false;
  tamanio;

  textoFormatos: string = null;
  constructor(private fs: FacadeService, private funciones: Funciones) { }

  ngOnInit() {
    this.tamanio=this.funciones.formatBytes(environment.MAX_SIZE_UPLOAD,0)
    this.sePuedeDescargar=(this.RutaDescarga==null || this.RutaDescarga=="" || this.RutaDescarga==undefined )?false:true;
    this.tituloDelSpan();  
  }

  tituloDelSpan() {
    if (this.Extensiones != null) {
      let textoExtensiones = this.Extensiones.split(',');
      this.textoFormatos = "";
      for (let i = 0; i < textoExtensiones.length; i++) {      
        if (textoExtensiones.length <= 1 || i == textoExtensiones.length - 1) {
          this.textoFormatos = this.textoFormatos.concat(textoExtensiones[i]); 
        } else {
          this.textoFormatos = this.textoFormatos.concat(textoExtensiones[i].concat(", "));
        }           
      }
    }
    else{
      this.Extensiones=sessionStorage.getItem("extensiones");      
      this.tituloDelSpan();
    }

  }
  validarExtensionArchivo(FileEntrada: any): boolean {

    if (!this.Extensiones) return true;

    let extension = FileEntrada.name.substring(FileEntrada.name.lastIndexOf(".")).toLowerCase();
    let extensionesPermitidas = this.Extensiones.split(',');

    let existe = extensionesPermitidas.find(x => x == extension);

    return !!existe;
  }

  validarTamanioArchivo(FileEntrada: any): boolean {
    return FileEntrada.size <= environment.MAX_SIZE_UPLOAD;
  }
  pdfSrc: any;
  arrBytes: any;
  lanzarOnChange(event) {
    event.sizeOK = true;
    event.extensionOK = true;

    let FileEntrada = event.target.files[0];

    if (FileEntrada) {

      let esArchivoValido = this.validarExtensionArchivo(FileEntrada);

      if (!esArchivoValido) {
        event.extensionOK = false;
        event.uploaded = null;
        event.target.value = '';
        this.funciones.mensaje("warning", "Por favor verifique el tipo de archivo");
        this.onChange.emit(event);
        return;
      }

      esArchivoValido = this.validarTamanioArchivo(FileEntrada);

      if (!esArchivoValido) {
        event.sizeOK = false;
        event.uploaded = null;
        event.target.value = '';
        this.funciones.mensaje("warning", "Por favor verifique el tamaño de su archivo");
        this.onChange.emit(event);
        return;
      }

      let InputSalida: HTMLInputElement = document.getElementsByName(this.idComp)[0] as HTMLInputElement;
      InputSalida.value = FileEntrada.name;

        this.fs.maestraService.registrarArchivoData(FileEntrada, this.IdTipoArchivo).subscribe(
          response => {
            event.uploaded = response;
            event.nombreArchivo = InputSalida.value;
            this.onChange.emit(event);
          },
          () => {
            this.funciones.mensaje("warning", "Problemas con el servidor, por favor intente subir su archivo más tarde.");
            event.uploaded = null;
            event.nombreArchivo = null;
            this.onChange.emit(event);
          });
    }
  }
  focus() {
    let InputSalida: HTMLInputElement = document.getElementsByName(this.idComp)[0] as HTMLInputElement;
    InputSalida.focus()
  }
}





