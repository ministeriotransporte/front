import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menu:any=[];
  constructor() { }

  ngOnInit() {

    //sessionStorage.setItem("Menus",JSON.stringify(aaa));
    document.getElementById("sidebarCollapse").addEventListener("click", () => {
      document.getElementById("sidebar").classList.toggle("active");
    });
    this.ArmarMenu();
  }

  ArmarMenu(){
      if(sessionStorage.getItem("Menus")!=null && sessionStorage.getItem("Menus")!=undefined){
        let info=JSON.parse(JSON.parse(sessionStorage.getItem("Menus")));
        this.menu=this.getJSONmenu(info);
        console.log("menu construido", this.menu);
      }
  }
  getJSONmenu(data: any):any {

    let menu:any;
    let i:number=0;
    let item:any;
    let rpta:any=[];
    for (i=0;i<data.length;i++)
    {
      item = data[i];
    
      if (item["Nivel"]==0)
      {
        item.hijos=this.cargarMenu(item,data,item["Id_Menu"],(item["Nivel"]+1));
        rpta.push(item);
      }
    }
    return rpta; 
  }

  cargarMenu(itempadre,_data,_id_menu,_nivel)
  {
    let i:number=0;
    let item:any;
    let submenu=[];
    //let items=[];
    
    for (i=0;i<_data.length;i++)
    {
      item = _data[i];
      if (item["Id_Menu_Padre"] == _id_menu && item["Nivel"] == _nivel)
      {
        item.hijos=this.cargarMenu(item,_data,item["Id_Menu"],(item["Nivel"]+1));
        submenu.push(item);
      }
    }

    return submenu;
  }


}
