import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UsuarioResetearClave } from '../../models/mantenimiento/usuario/usuario.model';
import { Funciones } from '../../functions/funciones';
import { FacadeService } from '../../patrons/facade.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  txtUsuario="";
  txtPerfil=""
  modalRef: BsModalRef;
  entidadResetearClave: UsuarioResetearClave;
  idUsuario: number = null;
  
  constructor(private router:Router,private modalService: BsModalService, public funciones: Funciones, public fs: FacadeService) { }

  ngOnInit() {
    this.idUsuario = parseInt(sessionStorage.getItem("IdUsuario"));
    this.txtUsuario=sessionStorage.getItem("Nombre_Usuario") + " " + sessionStorage.getItem("Apellido_Paterno") + " " + sessionStorage.getItem("Apellido_Materno");
    this.txtPerfil=sessionStorage.getItem("Nombre_Perfil");
  }
  Salir(){
    sessionStorage.clear();
    this.router.navigate(['/login'])
  }

  cambiarContrasena(template: TemplateRef<any>) {
    this.entidadResetearClave = new UsuarioResetearClave();
    this.entidadResetearClave.id_usuario = this.idUsuario;
    let config;
    config = {
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
      }
    };
    this.modalRef = this.modalService.show(template, config);
  }

  RegistrarReseteo() {
    if (this.entidadResetearClave.repetirClave != this.entidadResetearClave.nuevaClave) {
      this.funciones.mensaje("warning", "Deben de coincidir las contraseñas");
      return;
    } else {
      // let pEnvio = {
      //   id_usuario: this.entidadResetearClave.id_usuario,
      //   contrasenia: this.entidadResetearClave.nuevaClave,
      //   cambio_clave: "false"
      // }
      this.fs.usuarioService.actualizarContrasenia(this.entidadResetearClave.id_usuario,this.entidadResetearClave.nuevaClave,"false").subscribe(
        data => {
          let respuesta = data as any;
          if (respuesta != null) {
            if (respuesta.id_usuario > 0) {
              this.funciones.mensaje("success", "El cambio de contraseña fue realizado satisfactoriamente.");
              this.modalRef.hide();
            }
          }
        }
      );
    }
  }
}
