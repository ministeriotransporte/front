import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MetodoService } from './metodo.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})

export class MaestraService {

    constructor(private http: HttpClient, private apiService: MetodoService,private httpFile: Http) { }

    registrarArchivoData(file: any, tipo: any) {
        let formData: FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let headers = new Headers()
        let accessToken = sessionStorage.getItem("token");
        headers.set("Authorization", `Bearer ${accessToken}`);
        let options = new RequestOptions({ headers: headers });
        let apiUrl = environment.host + "api/File/subirSeguimientoAccidente?intFile=" + tipo;
        return this.httpFile.post(apiUrl, formData, options);
    }
}
