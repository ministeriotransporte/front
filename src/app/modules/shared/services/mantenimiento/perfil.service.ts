import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class MantenimientoPerfilService {
  ListarDetalleMenuCombo(idPerfil: number) {
    throw new Error("Method not implemented.");
  }

  constructor(private http: HttpClient, private apiService: MetodoService) { }

  listarPerfiles(ipInput: any) {
    const href = environment.host + "api/login/ListarPerfil";
    return this.http.get(href + "?ipInput=" + JSON.stringify(ipInput));
  }

  actualizarPerfiles(perfil: any) {
    return this.apiService.POST("api/login/ModificarPerfil", perfil);
  }
  registrarPerfiles(perfil: any) {
    return this.apiService.POST("api/login/InsertarPerfil", perfil);
  }

  /////asignar componentes
  listarDetallePerfilMenu(ipInput: any) {
    return this.apiService.GET("api/login/ListarDetallePerfilMenu", ipInput);
  }

  ListarComponentePendiente(ipInput: any) {
    return this.apiService.GET("api/login/ListarComponentePendiente", ipInput);
  }

  procesarDetallePerfilMenuComponente(perfil: any) {
    return this.apiService.POST("api/login/ProcesarDetallePerfilMenuComponente", perfil);
  }

  listarDetallePerfilMenuComponente(ipInput: any) {
    return this.apiService.GET("api/login/ListarDetallePerfilMenuComponente", ipInput);
  }

  anularDetallePerfilMenuComponente(componente: any) {
    return this.apiService.POST("api/login/AnularDetallePerfilMenuComponente", componente);
  }

  ModificarDetallePerfilMenuComponente(perfil: any) {
    return this.apiService.POST("api/login/ModificarDetallePerfilMenuComponente", perfil);
  }
  
  ////asiganar menu
  listarDetalleMenu(ipInput: any) {
    return this.apiService.GET("api/login/ListarDetalleMenu", ipInput);
  }

  insertarDetallePerfilMenu(perfil: any) {
    return this.apiService.POST("api/login/insertarDetallePerfilMenu", perfil);
  }

  anularPerfil(ipInput: any){
    return this.apiService.POST("api/login/anularPerfil", ipInput);
  }

  //Organismo
  listarOrganismo(ipInput: any) {
    return this.apiService.GET("api/login/listarOrganismo", ipInput);
  }
  listarPerfilCombo(ipInput: any) {
    return this.apiService.GET("api/login/listarPerfilCombo", ipInput);
  }
  listarPerfilOrganismo(ipInput: any) {
    return this.apiService.GET("api/login/listarPerfilOrganismo", ipInput);
  }
}
