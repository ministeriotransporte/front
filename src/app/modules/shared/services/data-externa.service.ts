import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataExternaService {

  constructor(private http: HttpClient) { }

  consultarInformacionReniec(strDni: string) {
    const href = environment.host + 'api/DataExterna/consultarInformacionReniec';
    return this.http.get(`${href}?strDni=${strDni}`);
  }
  consultarInformacionSunat(strRuc: string) {
    const href = environment.host + 'api/DataExterna/consultarInformacionSunat';
    return this.http.get(`${href}?strRuc=${strRuc}`);
  }
}
