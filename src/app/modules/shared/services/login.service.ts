import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) {

  }
  
  IniciarSesion(usuario:string,clave:string){
    let body: HttpParams = new HttpParams()

    .append('grant_type', "password")
    .append('Password',clave)
    .append('UserName',usuario);

    var headers_object = new HttpHeaders().
    set('Content-Type', 'application/x-www-form-urlencoded');

    const httpOptions = {
      headers: headers_object
    };
  
    return this.http.post(environment.host + "Token",body);
  }
}
