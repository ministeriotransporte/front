import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class PersonaFuratService {

  constructor(private apiService: MetodoService) { }

  listarPersonaFuratControl() {
    return this.apiService.GET("api/FichaFurat/listarPersonaFuratControl");
  }
  listarPersonaAccidenteFurat(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/listarPersonaAccidenteFurat", ipInput);
  }
  obtenerPersonaAccidenteFurat(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/obtenerPersonaAccidenteFurat", ipInput);
  }
  insertarPersonaAccidenteFurat(personaFurat: any) {
    return this.apiService.POST("api/FichaFurat/insertarPersonaAccidenteFurat", personaFurat);
  }
  modificarPersonaAccidenteFurat(personaFurat: any) {
    return this.apiService.POST("api/FichaFurat/modificarPersonaAccidenteFurat", personaFurat);
  }
}
