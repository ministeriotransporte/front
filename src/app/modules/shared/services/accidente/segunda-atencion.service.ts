import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class SegundaAtencionService {

  constructor(private apiService: MetodoService) { }

  listarAtencionSecundariaControl(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarAtencionSecundariaControl",ipInput);
  }
  insertarAtencionSecundaria(segundaAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarAtencionSecundaria", segundaAtencion);
  }
  listarAtencionSecundaria(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarAtencionSecundaria", ipInput);
  }
  modificarAtencionSecundaria(segundaAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarAtencionSecundaria", segundaAtencion);
  }
  anularAtencionSecundaria(segundaAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/anularAtencionSecundaria", segundaAtencion);
  }
}
