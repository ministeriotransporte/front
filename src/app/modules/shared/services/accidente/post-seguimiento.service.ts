import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class PostSeguimientoService {

  constructor(private apiService: MetodoService) { }

  listarSeguimientoAccidente(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarSeguimientoAccidente",ipInput);
  }
  insertarSeguimientoAccidente(seguimiento: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarSeguimientoAccidente", seguimiento);
  }
  modificarSeguimientoAccidente(seguimiento: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarSeguimientoAccidente", seguimiento);
  }
  anularSeguimientoAccidente(primeraAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/anularSeguimientoAccidente", primeraAtencion);
  }

  ///// evolucion del paciente
  listarAtencionPrimariaDetalle(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarAtencionPrimariaDetalle",ipInput);
  }
  insertarAtencionPrimariaDetalle(lesioando: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarAtencionPrimariaDetalle", lesioando);
  }
  modificarAtencionPrimariaDetalle(lesioando: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarAtencionPrimariaDetalle", lesioando);
  }
  listarSeguimientoAccidenteControl() {
    return this.apiService.GET("api/AccidenteTransito/listarSeguimientoAccidenteControl");
  }
  

}
