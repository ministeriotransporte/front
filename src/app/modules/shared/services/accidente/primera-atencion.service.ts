import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class PrimeraAtencionService {

  constructor(private apiService: MetodoService) { }

  listarAtencionPrimariaControl() {
    return this.apiService.GET("api/AccidenteTransito/listarAtencionPrimariaControl");
  }
  insertarAtencionPrimaria(primeraAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarAtencionPrimaria", primeraAtencion);
  }

  listarAtencionPrimaria(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarAtencionPrimaria", ipInput);
  }
  modificarAtencionPrimaria(primeraAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarAtencionPrimaria", primeraAtencion);
  }
  obtenerAtencionPrimaria(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/obtenerAtencionPrimaria", ipInput);
  }

  anularAtencionPrimaria(primeraAtencion: any) {
    return this.apiService.POST("api/AccidenteTransito/anularAtencionPrimaria", primeraAtencion);
  }
  listarLesionado(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarLesionado", ipInput);
  }
  

}
