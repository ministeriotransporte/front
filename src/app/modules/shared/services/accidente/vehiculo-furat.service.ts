import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class VehiculoFuratService {

  constructor(private apiService: MetodoService) { }

  listarVehiculoFuratControl() {
    return this.apiService.GET("api/FichaFurat/listarVehiculoFuratControl");
  }
  listarVehiculoInvolucradoFurat(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/listarVehiculoInvolucradoFurat", ipInput);
  }
  obtenerVehiculoInvolucradoFurat(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/obtenerVehiculoInvolucradoFurat", ipInput);
  }
  insertarVehiculoInvolucradoFurat(vehiculoFurat: any) {
    return this.apiService.POST("api/FichaFurat/insertarVehiculoInvolucradoFurat  ", vehiculoFurat);
  }
  modificarVehiculoInvolucradoFurat(vehiculoFurat: any) {
    return this.apiService.POST("api/FichaFurat/modificarVehiculoInvolucradoFurat", vehiculoFurat);
  }
}
