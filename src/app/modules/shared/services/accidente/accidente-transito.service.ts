import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class AccidenteTransitoService {

  constructor(private apiService: MetodoService) { }

  listarAccidenteTransitoControl() {
    return this.apiService.GET("api/AccidenteTransito/listarAccidenteTransitoControl");
  }

  insertarAccidenteTransito(accidente: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarAccidenteTransito", accidente);
  }

  obtenerAccidenteTransito(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/obtenerAccidenteTransito", ipInput);
  }

  modificarAccidenteTransito(accidente: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarAccidenteTransito", accidente);
  }

  anularAccidenteTransito(accidente: any) {
    return this.apiService.POST("api/AccidenteTransito/anularAccidenteTransito", accidente);
  }

  obtenerUbigeoXPuntos(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/obtenerUbigeoXPuntos", ipInput);
  }

  listarDepartamento() {
    return this.apiService.GET("api/AccidenteTransito/listarDepartamento");
  }

  listarProvincia(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarProvincia", ipInput);
  }

  listarDistrito(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarDistrito", ipInput);
  }

  ///vehiculo involucrado
  listarVehiculoInvolucradoControl() {
    return this.apiService.GET("api/AccidenteTransito/listarVehiculoInvolucradoControl");
  }

  anularVehiculoInvolucrado(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/anularVehiculoInvolucrado", vehiculo);
  }

  insertarVehiculoInvolucrado(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarVehiculoInvolucrado", vehiculo);
  }

  modificarVehiculoInvolucrado(accidente: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarVehiculoInvolucrado", accidente);
  }

  listarVehiculoInvolucrado(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarVehiculoInvolucrado", ipInput);
  }


  /////CONDUCTOR
  listarConductorVehiculoControl() {
    return this.apiService.GET("api/AccidenteTransito/listarConductorVehiculoControl");
  }

  insertarConductorVehiculo(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarConductorVehiculo", vehiculo);
  }

  listarConductorVehiculo(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarConductorVehiculo", ipInput);
  }

  anularConductorVehiculo(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/anularConductorVehiculo", vehiculo);
  }

  listarVehiculoInvolucradoCombo(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarVehiculoInvolucradoCombo", ipInput);
  }
  modificarConductorVehiculo(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarConductorVehiculo", vehiculo);
  }

  ///PASAJERO PEATON
  listarPasajeroPeatonControl() {
    return this.apiService.GET("api/AccidenteTransito/listarPasajeroPeatonControl");
  }
  insertarPasajeroPeaton(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarPasajeroPeaton", vehiculo);
  }
  listarPasajeroPeaton(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/listarPasajeroPeaton", ipInput);
  }
  anularPasajeroPeaton(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/anularPasajeroPeaton", vehiculo);
  }
  modificarPasajeroPeaton(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarPasajeroPeaton", vehiculo);
  }

  ////situacion via
  listarSituacionViaControl() {
    return this.apiService.GET("api/AccidenteTransito/listarSituacionViaControl");
  }
  obtenerSituacionVia(ipInput: any) {
    return this.apiService.GET("api/AccidenteTransito/obtenerSituacionVia", ipInput);
  }
  insertarSituacionVia(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/insertarSituacionVia", vehiculo);
  }
  modificarSituacionVia(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/modificarSituacionVia", vehiculo);
  }
  anularSituacionVia(vehiculo: any) {
    return this.apiService.POST("api/AccidenteTransito/anularSituacionVia", vehiculo);
  }

  





  











}
