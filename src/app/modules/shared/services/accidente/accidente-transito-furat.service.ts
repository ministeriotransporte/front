import { Injectable } from '@angular/core';
import { MetodoService } from '../metodo.service';

@Injectable({
  providedIn: 'root'
})
export class AccidenteTransitoFuratService {

  constructor(private apiService: MetodoService) { }

  listarAccidenteFuratControl() {
    return this.apiService.GET("api/FichaFurat/listarAccidenteFuratControl");
  }
  listarCausaSecundaria(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/listarCausaSecundaria", ipInput);
  }
  listarDepartamento() {
    return this.apiService.GET("api/FichaFurat/listarDepartamento");
  }

  listarProvincia(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/listarProvincia", ipInput);
  }

  listarDistrito(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/listarDistrito", ipInput);
  }
  obtenerUbigeoXPuntos(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/obtenerUbigeoXPuntos", ipInput);
  }
  obtenerAccidenteTransitoFurat(ipInput: any) {
    return this.apiService.GET("api/FichaFurat/obtenerAccidenteTransitoFurat", ipInput);
  }
  insertarAccidenteTransitoFurat(accidenteFurat: any) {
    return this.apiService.POST("api/FichaFurat/insertarAccidenteTransitoFurat  ", accidenteFurat);
  }

  modificarAccidenteTransitoFurat(accidenteFurat: any) {
    return this.apiService.POST("api/FichaFurat/modificarAccidenteTransitoFurat", accidenteFurat);
  }

}
