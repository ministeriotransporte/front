import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { Componente } from 'src/app/modules/shared/models/mantenimiento/componente/componente.model';
import { ModalRegistrarComponenteComponent } from '../../modales/modal-registrar-componente/modal-registrar-componente.component';

@Component({
  selector: 'app-componentes',
  templateUrl: './componentes.component.html',
  styleUrls: ['./componentes.component.scss']
})
export class ComponentesComponent implements OnInit {

  campoBusqueda:string = "";
  bsModalRef: BsModalRef;
  totalComponentes:number =0;
  arregloComponente = [];
  lstParametros;
  response;
  config;
  paginaActual:number =1;
  numero_pagina: number =0;
  total_filas: number =10;
  constructor(private modalService: BsModalService, private funciones : Funciones, private fs : FacadeService) { }

  ngOnInit() {
    this.ListadoPrincipal();
  }

  ListadoPrincipal(){
    let envio = {
      nombre_componente: this.campoBusqueda,
      skip: this.total_filas,
      take: this.numero_pagina
    }
    this.fs.mantenimientoComponenteService.listaComponentes(envio).subscribe(
      data =>{
          this.response = data;
          if(this.response != ""){
            if(this.response[0].cantidad_componentes != 0){
              this.arregloComponente = this.response[0].componentes;
              this.totalComponentes = this.response[0].cantidad_componentes
            }else{
              this.arregloComponente = [];
              this.totalComponentes = 0;
            };
          }else{
            this.arregloComponente = [];
            this.totalComponentes = 0;
          }
      }
    )
  }
  cambiarPagina(pagina){
    this.numero_pagina = (pagina.page * 10) - 10;
    let envio = {
      nombre_componente: this.campoBusqueda == null?"":this.campoBusqueda,
      skip: this.total_filas,
      take: this.numero_pagina
    }
    this.fs.mantenimientoComponenteService.listaComponentes(envio).subscribe(
      data => {
        this.response = data;
        if(this.response != ""){
          if(this.response[0].cantidad_perfiles != 0){
            this.arregloComponente = this.response[0].componentes;
            this.totalComponentes = this.response[0].cantidad_componentes
          }else{
            this.arregloComponente = [];
            this.totalComponentes = 0;
          };
        }else{
          this.arregloComponente = [];
          this.totalComponentes = 0;
        }
      }
    )
  }
  busqueda(){
    let envio = {
      nombre_componente: this.campoBusqueda == null?"":this.campoBusqueda,
      skip: this.total_filas,
      take: this.numero_pagina
    }
    this.fs.mantenimientoComponenteService.listaComponentes(envio).subscribe(
      data => {
        this.response = data;        
        if(this.response != ""){
          if(this.response[0].cantidad_componentes != 0){
            this.arregloComponente = this.response[0].componentes;
            this.totalComponentes = this.response[0].cantidad_componentes;
          }else{
            this.arregloComponente = [];
            this.totalComponentes = 0;
          }
        }else{
          this.arregloComponente = [];
          this.totalComponentes = 0;
        }
      }
    )
  }
  // modalAgregarMenuPadre(objpadre){
  //   this.config = {
  //     ignoreBackdropClick: true,
  //     keyboard: false,
  //     initialState: {
  //       entidadEditar: null,
  //       entidadPadre:objpadre
  //     }
  //   };
  //   // this.bsModalRef = this.modalService.show(ModalPerfilesComponent, this.config);
  //   // this.bsModalRef.content.retornoValores.subscribe(
  //   //   data => {
  //   //     let pagina = {page:this.paginaActual}
  //   //     this.cambiarPagina(pagina);
  //   //   }
  //   // )
  // }

  modalEditarComponente(obj){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-componente",
      initialState: {
        entidadEditar: obj
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistrarComponenteComponent,this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        let pagina = {page:this.paginaActual}
        this.cambiarPagina(pagina);
      }
    )
  }

  mostrarAlerta(codigo){
    this.funciones.alertaRetorno("question","Deseas eliminar el siguiente registro?","",true,(respuesta) =>{
      if(respuesta.value){
        this.eliminar(codigo);
      }
    })
  }
  eliminar(codigo){
    let entidadEliminar = new Componente();
    entidadEliminar.id_componente = codigo;
    entidadEliminar.usuario_anulacion = sessionStorage.getItem("Usuario");
    this.fs.mantenimientoComponenteService.anularComponente(entidadEliminar).subscribe(
      (data: any) => {        
        if(data.id_componente > 0){
            this.funciones.alertaSimple("success","Se eliminó el componente!","",true);
            this.busqueda();
        }
        else{
            this.funciones.alertaSimple("error","Ocurrio un error al momento eliminar el componente","",true);
        }
      }
    )
  }
  modalAgregarComponente(){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-componente",
      initialState: {
        entidadEditar: null
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistrarComponenteComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        let pagina = {page:this.paginaActual}
        this.cambiarPagina(pagina);
      }
    )
  }
}
