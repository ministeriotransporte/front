import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService } from 'node_modules/ngx-bootstrap/modal';
import { BsModalRef } from 'node_modules/ngx-bootstrap/modal';
import { ModalRegistrarMenuComponent } from '../../modales/modal-registrar-menu/modal-registrar-menu.component';
import { Subscription } from 'rxjs/internal/Subscription';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { MenuModal, Menu } from 'src/app/modules/shared/models/mantenimiento/menu/menu.model';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit, OnDestroy {

  campoBusqueda: number = null
  bsModalRef: BsModalRef;
  totalMenu: number = 0;
  arregloMenu = [];
  lstParametros;
  response;
  config;
  menus;
  paginaActual: number = 1;
  menu_seleccionado: number;
  nombreUsuario: string = sessionStorage.getItem("Usuario");
  Suscripciones:Subscription= new Subscription();

  constructor(private modalService: BsModalService, private fs: FacadeService, private funciones: Funciones) { }
  ngOnDestroy(): void {
    this.Suscripciones.unsubscribe();
  }

  ngOnInit() {
    this.listarMenus();
    this.ListadoPrincipal()
  }
  listarMenus() {
    this.fs.mantenimientoMenuService.listarMenuCombo().subscribe(
      data => {
        this.menus = data;
      }
    ) 
  }
  ListadoPrincipal() {
    let envio = {
      id_menu_padre: this.campoBusqueda == null ? 0 : this.campoBusqueda
    }
    this.fs.mantenimientoMenuService.listarMenu(envio).subscribe(
      (data: any) => {
        this.response = data;
        if (this.response != null) {
          this.arregloMenu = this.response;
          this.totalMenu = this.arregloMenu.length;
          this.construirListadoPrincipal(this.arregloMenu);
        } else {
          this.arregloMenu = [];
          this.totalMenu = 0;
        }
        this.campoBusqueda = this.campoBusqueda == 0 ? null : this.campoBusqueda;
      }
    )
  }
  lstParametrosHijoParametro = [];
  beParametroMenu: MenuModal;
  construirListadoPrincipal(pListadoMenu) {
    this.lstParametrosHijoParametro = [];
    if (pListadoMenu != null) {
      if (pListadoMenu.length > 0) {
        pListadoMenu.forEach(d => {
          if (d.nivel == 0) {
            this.lstParametrosHijoParametro.push(d);
            this.beParametroMenu = new MenuModal();
            this.beParametroMenu = d;
            this.agregarHijo(this.beParametroMenu, pListadoMenu, this.lstParametrosHijoParametro);
          }
        });
      }
    }
  }
  agregarHijo(beParametro: MenuModal, lstParametros, lstParametrosHijoParametro) {
    let beHijo: MenuModal;
    let list = [];

    lstParametros.forEach(x => {
      if (x.id_menu_padre == beParametro.id_menu) {
        list.push(x);
      }
    });

    let concatena = "";
    if (list.length > 0) {
      for (let x = 0; x <= list[0].nivel - 1; x++) {
        concatena = concatena + "      ";
      }
    }

    list.forEach(d => {
      beHijo = new MenuModal();
      beHijo = d;
      d.nombre_menu = concatena + d.nombre_menu;
      lstParametrosHijoParametro.push(d);
      this.agregarHijo(beHijo, lstParametros, lstParametrosHijoParametro);
    });
    return lstParametrosHijoParametro;
  }
  busqueda() {
    let envio = {
      id_menu_padre: this.campoBusqueda == null ? 0 : this.campoBusqueda
    }

    this.fs.mantenimientoMenuService.listarMenu(envio).subscribe(
      data => {
        this.response = data;
        if (this.response != "") {
          this.arregloMenu = this.response;
          this.totalMenu = this.arregloMenu.length;
          this.construirListadoPrincipal(this.arregloMenu);
        } else {
          this.arregloMenu = [];
          this.totalMenu = 0;
        }
        this.campoBusqueda = this.campoBusqueda == 0 ? null : this.campoBusqueda;
      }
    )
  }
  modalAgregarMenuPadre(objpadre) {
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-menu",
      initialState: {
        entidadEditar: null,
        entidadPadre: objpadre
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistrarMenuComponent, this.config);
    this.Suscripciones.add(this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.busqueda();
      }
    ));
  }

  modalEditarMenu(obj) {
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-menu",
      initialState: {
        entidadEditar: obj
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistrarMenuComponent, this.config);
    this.Suscripciones.add(this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.busqueda();
      }
    ));
  }
  mostrarAlerta(codigo) {
    this.funciones.alertaRetorno("question", "Deseas eliminar el siguiente registro?", "", true, (respuesta) => {
      if (respuesta.value) {
        this.eliminar(codigo);
      }
    })
  }
  eliminar(codigo) {
    let entidadEliminar = new Menu();
    entidadEliminar.id_menu = codigo;
    entidadEliminar.usuario_anulacion = this.nombreUsuario; //sessionStorage.getItem("Usuario");
    this.fs.mantenimientoMenuService.anularMenu(entidadEliminar).subscribe(
      (data: any) => {
        if (data.id_menu > 0) {
          this.funciones.alertaSimple("success", "Se deshabilitó el menú!", "", true);
          this.busqueda();
        }
        else {
          this.funciones.alertaSimple("error", "Ocurrio un error al momento deshabilitar el menú", "", true);
        }
      }
    )
  }
  modalAgregarMenu() {
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-menu modal-app",
      initialState: {
        entidadEditar: null
        //,ListadoSistemas: this.ListadoSistemas
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistrarMenuComponent, this.config);
    this.Suscripciones.add(this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.busqueda();
      }
    ));
  }

  CargarListadoMenus(item: any): void {

    if (item != undefined) {
      this.busqueda();
    }
    else {
      this.menus = [];
    }
  }
}

