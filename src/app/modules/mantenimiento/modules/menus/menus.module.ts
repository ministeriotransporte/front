import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PlantillaModule } from 'src/app/modules/shared/plantilla/plantilla.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MenusComponent } from './pages/menus/menus.component';
import { ModalRegistrarMenuComponent } from './modales/modal-registrar-menu/modal-registrar-menu.component';

const routes: Routes = [
  { path : "", component: MenusComponent
  }
];

@NgModule({
  declarations: [MenusComponent, ModalRegistrarMenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    PlantillaModule,
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalRegistrarMenuComponent
  ]
})
export class MenusModule { }
