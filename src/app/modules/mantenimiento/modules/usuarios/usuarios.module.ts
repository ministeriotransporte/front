import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { ModalRegistroUsuarioComponent } from './modales/modal-registro-usuario/modal-registro-usuario.component';
import { PlantillaModule } from 'src/app/modules/shared/plantilla/plantilla.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ModalVerUbigeoUsuarioComponent } from './modales/modal-ver-ubigeo-usuario/modal-ver-ubigeo-usuario.component';

const routes: Routes = [
  { path : "", component: UsuariosComponent
  }
];

@NgModule({
  declarations: [UsuariosComponent, ModalRegistroUsuarioComponent, ModalVerUbigeoUsuarioComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    PlantillaModule,
    ModalModule.forRoot()
  ],
  entryComponents:[
    ModalRegistroUsuarioComponent,
    ModalVerUbigeoUsuarioComponent
  ]
})
export class UsuariosModule { }
