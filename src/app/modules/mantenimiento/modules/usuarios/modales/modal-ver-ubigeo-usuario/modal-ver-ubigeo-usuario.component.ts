import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-ver-ubigeo-usuario',
  templateUrl: './modal-ver-ubigeo-usuario.component.html',
  styleUrls: ['./modal-ver-ubigeo-usuario.component.scss']
})
export class ModalVerUbigeoUsuarioComponent implements OnInit {

  arregloUbigeoUsuario: any[];
  lstUbigeoUsuario = [];
  constructor(public modalRef: BsModalRef) { }

  ngOnInit() {
    if(this.arregloUbigeoUsuario != null){
      this.arregloUbigeoUsuario.forEach(element => {
        if(this.lstUbigeoUsuario.indexOf(element.departamento)== -1){
          this.lstUbigeoUsuario.push(element.departamento);
        }
      });
    }
  }
  closeModal(){
    this.modalRef.hide();
  }
}