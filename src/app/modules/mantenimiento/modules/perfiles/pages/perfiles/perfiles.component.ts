import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalRegistroPerfilComponent } from '../../modales/modal-registro-perfil/modal-registro-perfil.component';
import { ModalAsignacionMenuComponent } from '../../modales/modal-asignacion-menu/modal-asignacion-menu.component';
import { ModalAsignacionComponentesComponent } from '../../modales/modal-asignacion-componentes/modal-asignacion-componentes.component';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { Perfiles } from 'src/app/modules/shared/models/mantenimiento/perfil/perfil.model';


@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.scss']
})
export class PerfilesComponent implements OnInit {

  campoBusqueda: string = "";
  bsModalRef: BsModalRef;
  totalPerfiles: number = 0;
  arregloPerfil: any = [];
  response: any;
  config: any;
  paginaActual: number = 1;
  nomUsuario = sessionStorage.getItem("Usuario");
  constructor(private modalService: BsModalService, private funciones: Funciones, private fs: FacadeService) { }

  ngOnInit() {
    this.ListadoPrincipal();
  }

  ListadoPrincipal() {
    let pEnvio = {
      descripcion: this.campoBusqueda
    }
    this.fs.mantenimientoPerfilService.listarPerfiles(pEnvio).subscribe(
      data => {
        this.response = data;
        if (this.response != null) {
          this.arregloPerfil = this.response;
          this.totalPerfiles = this.response.length;
        } else {
          this.arregloPerfil = [];
          this.totalPerfiles = 0;
        }
      }
    )
  }

  buscarConsultaPrincipal() {
    this.ListadoPrincipal();
  }
  modalAsignacionMenu(idPerfil) {
    this.config = {
      ignoreBackdropClick: true,
      class: "modal-asignacion-menu",
      keyboard: false,
      initialState: {
        idPerfil: idPerfil
      }
    };
    this.bsModalRef = this.modalService.show(ModalAsignacionMenuComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
      }
    )
  }

  modalAsignacionComponente(idPerfil) {
    this.config = {
      ignoreBackdropClick: true,
      class: "modal-asignacion-perfil-componente modal-app",
      keyboard: false,
      initialState: {
        idPerfil: idPerfil
      }
    };
    this.bsModalRef = this.modalService.show(ModalAsignacionComponentesComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
       
      }
    )
  }

  modalEditarPerfil(obj) {
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        entidadEditar: obj
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistroPerfilComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.ListadoPrincipal();
      }
    )
  }
  mostrarAlerta(codigo) {
    this.funciones.alertaRetorno("question", "Deseas eliminar el siguiente registro?", "", true, (respuesta) => {
      if (respuesta.value) {
        this.eliminar(codigo);
      }
    })
  }
  eliminar(codigo) {
    let entidadEliminar = new Perfiles();
    entidadEliminar.id_perfil = codigo;
    entidadEliminar.usuario_eliminacion = this.nomUsuario;
    this.fs.mantenimientoPerfilService.anularPerfil(entidadEliminar).subscribe(
      (data: any) => {
        if (data.id_perfil > 0) {
          this.funciones.alertaSimple("success", "Se eliminó el perfil!", "", true);
          this.ListadoPrincipal();
        }
        else {
          this.funciones.alertaSimple("error", "Ocurrio un error al momento eliminar el perfil", "", true);
        }
      }
    )
  }
  modalAgregarPerfil() {
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-perfil",
      initialState: {
        entidadEditar: null
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistroPerfilComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.ListadoPrincipal();
      }
    )
  }
  modalAgregarPerfilHijo(item : any){
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      class: "modal-registrar-perfil",
      initialState: {
        entidadEditar: item
      }
    };
    this.bsModalRef = this.modalService.show(ModalRegistroPerfilComponent, this.config);
    this.bsModalRef.content.retornoValores.subscribe(
      data => {
        this.ListadoPrincipal();
      }
    )
  }
}
