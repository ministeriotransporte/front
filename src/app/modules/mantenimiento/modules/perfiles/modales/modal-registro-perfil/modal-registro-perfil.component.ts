import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Perfiles } from 'src/app/modules/shared/models/mantenimiento/perfil/perfil.model';
import { FacadeService } from 'src/app/modules/shared/patrons/facade.service';
import { Funciones } from 'src/app/modules/shared/functions/funciones';


@Component({
  selector: 'app-modal-registro-perfil',
  templateUrl: './modal-registro-perfil.component.html',
  styleUrls: ['./modal-registro-perfil.component.scss']
})
export class ModalRegistroPerfilComponent implements OnInit {
  @Output() retornoValores = new EventEmitter();
  entidadModal : Perfiles;
  entidadEditar;
  cambiarEditar:boolean = true;
  nomUsuario: string = sessionStorage.getItem("Usuario");
  lstEntidades = [];
  lstEntidadesPorPerfil = [];
  mostrarPerfilSeleccion: boolean = false;
  constructor(public modalRef: BsModalRef, private fs : FacadeService, private funciones : Funciones) { }

  ngOnInit() {
    this.cargarControlesRegistro();
    this.entidadModal = new Perfiles();
    if(this.entidadEditar !=null){
      this.setearCamposEditar();
      this.cambiarEditar = false;
      this.mostrarPerfilSeleccion = true;
    }else{
      this.mostrarPerfilSeleccion = false;
    }
  }
  cargarControlesRegistro(){
    this.fs.mantenimientoPerfilService.listarPerfilCombo({}).subscribe(
      (data: any)=>{
        if(data != null){
          this.lstEntidades = data.organismo;
        }
      }
    );    
  }

  seleccionarEntidad(item){
    if(item != null){
      let pEnvio = {
        id_organismo: item.id_organismo
      }
      this.fs.mantenimientoPerfilService.listarPerfilOrganismo(pEnvio).subscribe(
        (data: any)=>{
          if(data != null){
            this.lstEntidadesPorPerfil = data.lista_perfil;
          }
        }
      );  
    }else{
      this.lstEntidadesPorPerfil = [];
    }
  }
  closeModal() {
    this.retornoValores.emit(0);
    this.modalRef.hide();
  }
  administrarPerfil(){
    if(this.entidadEditar==null){
      this.guardarPerfil();
    }else{
      this.editarPerfil();
    }
  }
  setearCamposEditar(){
    this.entidadModal.id_perfil = this.entidadEditar.id_perfil;
    this.entidadModal.descripcion = this.entidadEditar.descripcion;
    this.entidadModal.id_organismo = this.entidadEditar.id_organismo;
    this.entidadModal.flag_etapa = this.entidadEditar.flag_etapa;
    this.entidadModal.usuario_modificacion = this.nomUsuario;
  }
  editarPerfil(){
    let entidadEditar = new Perfiles();
    entidadEditar.id_perfil = this.entidadModal.id_perfil;
    entidadEditar.descripcion = this.entidadModal.descripcion;
    entidadEditar.id_organismo = 2;//this.entidadModal.id_organismo;
    entidadEditar.flag_etapa = true;//this.entidadModal.flag_etapa;
    entidadEditar.usuario_modificacion = this.nomUsuario;
    this.fs.mantenimientoPerfilService.actualizarPerfiles(entidadEditar).subscribe(
      data=>{
        if(data == 0){
          this.funciones.mensaje("error",this.funciones.mostrarMensaje("error",""));
        }
        else{
          this.funciones.mensaje("success",this.funciones.mostrarMensaje("actualizar",""));
          this.retornoValores.emit(0);
          this.modalRef.hide();
        }
      }
    );
  }
  guardarPerfil(){ 
    let entidadRegistrar = new Perfiles();
    entidadRegistrar.id_perfil = 0;
    entidadRegistrar.descripcion = this.entidadModal.descripcion;
    entidadRegistrar.id_organismo = 2;//this.entidadModal.id_organismo;
    entidadRegistrar.flag_etapa = true;//this.entidadModal.flag_etapa;
    entidadRegistrar.usuario_creacion = this.nomUsuario;
    this.fs.mantenimientoPerfilService.registrarPerfiles(entidadRegistrar).subscribe(
      data=>{
        if(data == 0){
          this.funciones.mensaje("error",this.funciones.mostrarMensaje("error",""));
        }
        else{
          this.funciones.mensaje("success",this.funciones.mostrarMensaje("insertar",""));
          this.retornoValores.emit(0);
          this.modalRef.hide();
        }
      }
    )    
  }

}