import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { LoginService } from '../shared/services/login.service';
import { IUserInfo } from '../shared/interfaces/IUserInfo';
import { AuthService } from 'src/app/core/guards/auth.service';
import { UsuarioResetearClave } from '../shared/models/mantenimiento/usuario/usuario.model';
import { Funciones } from '../shared/functions/funciones';
import { UsuarioService } from '../shared/services/mantenimiento/usuario.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  inicio : FormGroup;
  htmlStr: string = '';
  redirigirPagina: string = "";
  entidadResetearClave: UsuarioResetearClave;
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService,private router:Router, private loginService:LoginService,private authService: AuthService, public funciones: Funciones, private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.entidadResetearClave = new UsuarioResetearClave();
    sessionStorage.clear();
    this.inicio=new FormGroup({
      UserName: new FormControl(''),
      Password:  new FormControl('')
    });
    let video:HTMLVideoElement=document.getElementById("app-video") as HTMLVideoElement;
    video.autoplay=true;
    video.loop=true;
    video.muted=true;
  }
  validarIngresoDatosLogin(): any {
    this.htmlStr="";
    let user: any = Object.assign({}, this.inicio.value);
    if (user.UserName == "" || user.Password == "") {

      let control: string = (user.UserName == "" ? "txtUsuario" : "txtPassword");
      this.htmlStr = (user.UserName == "" ? "Para iniciar sesión ingrese su Usuario" : "Para iniciar sesión ingrese su Contraseña");

      document.getElementById(control).focus();
      document.getElementById('botonLogin').style.display = 'block;'
      return null;
    }

    return user;
  }
  onSubmit(template: TemplateRef<any>) {
    let user: any = this.validarIngresoDatosLogin();

    if (user != null) {
      document.getElementById('botonLogin').style.display = 'none;'
      this.htmlStr = 'Verificando ...';
      this.loginService.IniciarSesion(user.UserName, user.Password).subscribe(
        (userInfo: IUserInfo) => {
          new Observable(this.authService.SessionStorageUserInfo(userInfo)).subscribe(
            ingresoDirecto => {
              this.htmlStr = "";
              sessionStorage.setItem("token", userInfo.access_token);
              if (userInfo.menus != "[]") {
                sessionStorage.setItem("Menus", JSON.stringify(userInfo.menus));
                if (JSON.parse(userInfo.menus.toString()).length > 0) {
                  this.redirigirPagina = JSON.parse(userInfo.menus.toString())[0].Url;
                }
              } else {
                sessionStorage.setItem("Menus", "[]");
                this.funciones.mensaje("info","Su perfil no tiene ninguna opción del menu asignado.");
                return false;
              }
              if (userInfo.componentes != "[]") {
                sessionStorage.setItem("Componentes", JSON.stringify(userInfo.componentes));
              }
              else {
                sessionStorage.setItem("Componentes", "[]");
              }
              if (ingresoDirecto) {
                this.cambiarContrasena(template, parseInt(userInfo.id_usuario),this.redirigirPagina);
              } else {
                if (this.redirigirPagina != "") {
                  this.router.navigate([this.redirigirPagina]);
                }
              }
            });
        }, () => {
          this.htmlStr = "El usuario y/o contraseña no coinciden";
        }
      );
    }
  }

  cambiarContrasena(template: TemplateRef<any>, pIdUsuario: number, pPaginaRedireccionar) {
    this.entidadResetearClave.id_usuario = pIdUsuario;
    let config;
    config = {
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
      }
    };
    this.modalRef = this.modalService.show(template,config);
  }
  RegistrarReseteo() {
    if (this.entidadResetearClave.repetirClave != this.entidadResetearClave.nuevaClave) {
      this.funciones.mensaje("warning", "Deben de coincidir las contraseñas");
      return;
    } else {
      this.usuarioService.actualizarContrasenia(this.entidadResetearClave.id_usuario, this.entidadResetearClave.nuevaClave, "false").subscribe(
        data => {
          let respuesta = data as any;
          if (respuesta != null) {
            if (respuesta.id_usuario > 0) {
              this.funciones.mensaje("success","El cambio de contraseña fue realizado satisfactoriamente.");
              this.modalRef.hide();
              this.router.navigate([this.redirigirPagina]);
            }
          }
        }
      );
    }
  }
  LimpiarMensaje(event:KeyboardEvent){
    this.htmlStr="";
  }
}


//   inicio : FormGroup;
//   htmlStr: string = '';
//   redirigirPagina: string = "";
//   constructor(private router:Router, private loginService:LoginService
//     ,private authService: AuthService
//     ) { }

//   ngOnInit() {
//     sessionStorage.clear();
//     this.inicio=new FormGroup({
//       UserName: new FormControl(''),
//       Password:  new FormControl('')
//     });
//     let video:HTMLVideoElement=document.getElementById("app-video") as HTMLVideoElement;
//     video.autoplay=true;
//     video.loop=true;
//     video.muted=true;
//   }
//   validarIngresoDatosLogin(): any {
//     this.htmlStr="";
//     let user: any = Object.assign({}, this.inicio.value);
//     if (user.UserName == "" || user.Password == "") {

//       let control: string = (user.UserName == "" ? "txtUsuario" : "txtPassword");
//       this.htmlStr = (user.UserName == "" ? "Para iniciar sesión ingrese su Usuario" : "Para iniciar sesión ingrese su Contraseña");

//       document.getElementById(control).focus();
//       document.getElementById('botonLogin').style.display = 'block;'
//       return null;
//     }

//     return user;
//   }
//   onSubmit() {
//     let user: any = this.validarIngresoDatosLogin();

//     if (user != null) {
//       document.getElementById('botonLogin').style.display = 'none;'
//       this.htmlStr = 'Verificando ...';

//       //this.router.navigate(['/mantenimiento/usuarios']);

//       this.loginService.IniciarSesion(user.UserName, user.Password).subscribe(
//         (userInfo: IUserInfo) => {
//           new Observable(this.authService.SessionStorageUserInfo(userInfo)).subscribe(
//             (esPrimerIngreso : any) => {
//               this.htmlStr = "";
//               sessionStorage.setItem("token", userInfo.access_token);
//               if (userInfo.menus != null) {
//                 sessionStorage.setItem("Menus", JSON.stringify(userInfo.menus));
//                 if (JSON.parse(userInfo.menus.toString()).length > 0) {
//                   this.redirigirPagina = JSON.parse(userInfo.menus.toString())[0].Url;
//                 }
//               } else {
//                 sessionStorage.setItem("Menus", "[]");
//               }
//               if (userInfo.componentes != null) {
//                 sessionStorage.setItem("Componentes", JSON.stringify(userInfo.componentes));
//               }
//               else {
//                 sessionStorage.setItem("Componentes", "[]");
//               }
//               this.router.navigate(['/registro']);
//             });
//         }, () => {
//           this.htmlStr = "El usuario y/o contraseña no coinciden";
//         }
//       );
//     }
//   }
//   LimpiarMensaje(event:KeyboardEvent){
//     this.htmlStr="";
//   }
// }
