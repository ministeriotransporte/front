import { Injectable, ɵConsole } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from "rxjs";
import { AuthService } from '../guards/auth.service';
import { Router } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { Funciones } from 'src/app/modules/shared/functions/funciones';
//import 'rxjs/add/operator/do';
//import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

    constructor(private authService: AuthService, private router: Router, private funciones: Funciones) {

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = sessionStorage.getItem('token');
        let request = req;
        if (token) {
            request = req.clone({
                setHeaders: {
                    authorization: `Bearer ${token}`
                }
            });
        }
        return next.handle(request).pipe(
            catchError((err: HttpErrorResponse) => {
                if (err.status === 401) {
                    this.funciones.alertaRetorno("error", "La sesión ha caducado", "Será redireccionado al login", false, (data) => {
                        if (data.value) {
                            sessionStorage.clear();
                            this.router.navigate(['/login']);
                        }
                    });
                }
                else if (err.status === 0) {
                    this.funciones.alertaSimple("error", "El servidor no responde", "Vuelve a intertarlo más tarde", true);
                }
                else if (err.status === 500) {
                    this.funciones.alertaSimple("error", "El servidor no responde", "Vuelve a intertarlo más tarde", true);
                }
                return throwError(err);
            })
        );
    }
}