import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {

  constructor(private http:HttpClient) { }

  listarPrincipal(ipInput:any){
    return this.http.get(environment.host+'api/Principal/listarPrincipal?ipInput='+JSON.stringify(ipInput));
  }

  listarCombos(){
    return this.http.get(environment.host+'api/Principal/listarCombos');
  }

  listarAccidenteTransitoExcel(ipInput:any){
    return this.http.get(environment.host+'api/AccidenteTransito/listarAccidenteTransitoExcel?ipInput='+JSON.stringify(ipInput));
  }

  listarAccidenteTransitoFicha(ipInput:any){
    return this.http.get(environment.host+'api/AccidenteTransito/listarAccidenteTransitoFicha?ipInput='+JSON.stringify(ipInput));
  }

  listarAccidenteTransitoResumen(ipInput:any){
    return this.http.get(environment.host+'api/AccidenteTransito/listarAccidenteTransitoResumen?ipInput='+JSON.stringify(ipInput));
  }


   listarAccidenteTransitoRepExcel(ipInput:any){
     return this.http.get(environment.host+'api/AccidenteTransito/listarAccidenteTransitoRepExcel?ipInput='+JSON.stringify(ipInput));
   }

  


}
