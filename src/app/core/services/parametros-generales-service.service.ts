import { Injectable } from '@angular/core';
import * as CryptoJS from "crypto-js";

@Injectable({
  providedIn: 'root'
})
export class ParametrosGeneralesServiceService {

  private camino: any =
    {
      accidente: {
        p_envio_codigo_accidente: "",
        p_envio_codigo_Accidente_externo : "",
        p_envio_id_accidente_transito:null,
        p_fecha_registro_accidente_transito : null,
        p_hora_registro_accidente_transito : "",
      },
    };

  private secret_key: string = "secret key 123"
  constructor() { }

  setPaso(campo: string, valor: any, pagina: string = 'accidente'):void {
    if (sessionStorage.getItem('az') != null) {
      let bytesCaminoEncriptado = CryptoJS.AES.decrypt(sessionStorage.getItem('az'), this.secret_key);
      let CaminoDescencriptado = bytesCaminoEncriptado.toString(CryptoJS.enc.Utf8);
      this.camino = JSON.parse(CaminoDescencriptado);
    }
    this.camino[pagina][campo] = valor;
    sessionStorage.setItem('az', CryptoJS.AES.encrypt(JSON.stringify(this.camino), this.secret_key).toString());
  }

  getRastro(): any {
    if (sessionStorage.getItem('az') != null) {
      let bytesCaminoEncriptado = CryptoJS.AES.decrypt(sessionStorage.getItem('az'), this.secret_key);
      let CaminoDescencriptado = bytesCaminoEncriptado.toString(CryptoJS.enc.Utf8);
      this.camino = JSON.parse(CaminoDescencriptado);
      return this.camino;
    }
    return this.camino;
  }
  clearRastroVariables(pagina :string= 'accidente'):void{

    if (sessionStorage.getItem('az') != null) {
      let bytesCaminoEncriptado = CryptoJS.AES.decrypt(sessionStorage.getItem('az'), this.secret_key);
      let CaminoDescencriptado = bytesCaminoEncriptado.toString(CryptoJS.enc.Utf8);
      this.camino = JSON.parse(CaminoDescencriptado);
    }

    if(pagina=='registro'){
      this.camino.monitoreo.p_envio_codigo_accidente= "";
      this.camino.p_envio_codigo_Accidente_externo = "";
      this.camino.monitoreo.p_envio_id_accidente_transito= null;
      this.camino.monitoreo.p_fecha_registro_accidente_transito=null;
      this.camino.monitoreo.p_hora_registro_accidente_transito="";
 
    }
    sessionStorage.setItem('az', CryptoJS.AES.encrypt(JSON.stringify(this.camino), this.secret_key).toString());
  
  }
}
