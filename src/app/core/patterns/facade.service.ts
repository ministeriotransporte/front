import { Injectable, Injector } from '@angular/core';
import { PrincipalService } from '../http/principal.service';

@Injectable({
  providedIn: 'root'
})
export class FacadeService {

  constructor(private injector: Injector) { }

  private _PrincipalService: PrincipalService;

  public get PrincipalService(): PrincipalService {
    if (!this._PrincipalService) {
        this._PrincipalService = this.injector.get(PrincipalService);
    }
    return this._PrincipalService;
}
}
