import { Injectable } from '@angular/core';
import {  CanActivate, ActivatedRouteSnapshot,Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate{

  constructor(public router: Router) { }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    if(sessionStorage.getItem("Usuario")!=null){
      if(sessionStorage.getItem("Menus")!=null){
        let menus =  JSON.parse(JSON.parse(sessionStorage.getItem("Menus")).toString());
          let url = menus.find(x => x.Url == "/"+route.routeConfig.path);

          if (url == null) {
            this.router.navigate(['/login']);
            return false; 
          }
        }
        else{
          sessionStorage.clear();
          this.router.navigate(['/login']);
          return false;
        }

    }
    else{
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
