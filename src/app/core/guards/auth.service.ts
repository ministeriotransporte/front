import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

//import { ModalCambiarClaveComponent } from '../modal-cambiar-clave/modal-cambiar-clave.component';

import { sessionStorageItems } from './sessionStorage';
import { environment } from 'src/environments/environment';
import { IUserInfo } from 'src/app/modules/shared/interfaces/IUserInfo';




@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  config;
  bsModalRef: BsModalRef;
  strlogon: string;
  strDatosLogon;
  userData;

  constructor(
    private router: Router
  ) { }

  get isLoggedIn() {
    if (sessionStorage.getItem("Logout") == "Salio") {
      sessionStorage.clear();
      // this.servicio.validarLogout().subscribe(
      //   data => {
      //     let ruta = data as string;
      //     window.location.href = ruta;
      //   }
      // );
    }
    else {
      if (this.loadSessionData() != null) {
        this.loggedIn = new BehaviorSubject<boolean>(true);
        return this.loggedIn.asObservable();
      } else {
        sessionStorage.clear();
        return this.loggedIn.asObservable();
      }
    }
  }


  islogon(): string {
    return this.strlogon = this.getParameterByName("xy");
  }

  getParameterByName(name: string) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  logout() {
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }

  /**
  * authService.SessionStorageUserInfo: Almacena la información del usuario al SessionStorage y
  * devuelve un valor Boolean indicando si el usuario esta accediendo por primera vez al Sistema.
  */
  SessionStorageUserInfo(userInfo: IUserInfo) {
    if (!userInfo) {
      throw "No se encontro informacion del usuario logueado.";
    }

    const CONST_ADMINISTRADOR: string = "ADMINISTRADOR"
    //let primeraCondicion: Boolean = (userInfo.perfil != CONST_ADMINISTRADOR && (userInfo.fecha_modificacion == ""))
    let primeraCondicion: Boolean = (userInfo.perfil != CONST_ADMINISTRADOR && (userInfo.cambio_clave=="True" ))
    let hacerCambioClave = primeraCondicion;

    if (userInfo.access_token) {
      sessionStorage.setItem(sessionStorageItems.SS_TOKEN, userInfo.access_token);
    }

    if (userInfo.usuario) {
      sessionStorage.setItem(sessionStorageItems.SS_USUARIO, userInfo.usuario);
    }

    sessionStorage.setItem(sessionStorageItems.SS_NOMBRE_PERFIL, userInfo.perfil);
    sessionStorage.setItem(sessionStorageItems.SS_ID_PERFIL, userInfo.id_perfil.toString());
    sessionStorage.setItem(sessionStorageItems.SS_COD_PERFIL, userInfo.cod_perfil.toString());

    sessionStorage.setItem(sessionStorageItems.SS_NOMBRE_USUARIO, "");
    if (userInfo.nombre) {
      sessionStorage.setItem(sessionStorageItems.SS_NOMBRE_USUARIO, userInfo.nombre);
      sessionStorage.setItem(sessionStorageItems.SS_APELLIDO_PATERNO, userInfo.apellido_paterno);
      sessionStorage.setItem(sessionStorageItems.SS_APELLIDO_MATERNO, userInfo.apellido_materno);
    }

    sessionStorage.setItem(sessionStorageItems.SS_ID_USUARIO, "");
    if (userInfo.id_usuario) {
      sessionStorage.setItem(sessionStorageItems.SS_ID_USUARIO, userInfo.id_usuario);
    }

    sessionStorage.setItem(sessionStorageItems.SS_DEPARTAMENTO,JSON.stringify(userInfo.departamento));
    sessionStorage.setItem(sessionStorageItems.SS_TIPO, "Login");

    if (hacerCambioClave) {
      //this.modalResetearClave(userInfo, userInfo.token);
      return (subscriber) => {
        subscriber.next(true);
      }
    }

    return (subscriber) => {
      subscriber.next(false);
    }
  }

  getAccessToken() {
    let token = sessionStorage.getItem("token");

    if (token != null) {
      return token;
    }
    return "";
  }

  loadSessionData() {
    var sessionStr = sessionStorage.getItem("token");
    return (sessionStr) ? sessionStr : null;
  }

  modalResetearClave(obj, token) {
    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        entidadClave: obj,
        stringToken: token
      },
      class: 'modal-resetearClave'
    };
  }



}
