import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[hasClaim]'
  })
  export class HasClaimDirective {
    @Input() set hasClaim(claimType: any) {
      let auth:any;
      if(sessionStorage.getItem("Componentes")!=null){
        auth = JSON.parse(sessionStorage.getItem("Componentes")); // Cuando devuelve el servicio []
        if (auth.length > 0) {
            auth = JSON.parse(JSON.parse(sessionStorage.getItem("Componentes")).toString());
            let infoClaim = auth.filter(c => c.Nombre_Componente.toLowerCase() == claimType.toLowerCase());
            if(infoClaim.length>0){
              this.viewContainer.createEmbeddedView(this.templateRef);
            }
            else{
              this.viewContainer.clear();
            }
        }
      } else{
        this.viewContainer.clear();
      }
    }
    
    constructor(
      private templateRef: TemplateRef<any>,
      private viewContainer: ViewContainerRef) { }
  }