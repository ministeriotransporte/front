import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from './core/guards/role.guard.service';
//import { LayoutComponent } from './core/layout/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
  },
   {
     path:'registro',
     loadChildren:() => import('./modules/principal/principal.module').then(m => m.PrincipalModule),
     canActivate:[RoleGuardService]
   },
   {
    path: 'registro/registro-accidente',
    //component: LayoutComponent,
    loadChildren: () => import('./modules/registro-accidente/registro-accidente.module').then(x => x.RegistroAccidenteModule),
    //canActivate: [RoleGuardService]
  },

  {
    path: 'mantenimiento/usuarios',
    loadChildren: () => import('./modules/mantenimiento/modules/usuarios/usuarios.module').then(m => m.UsuariosModule)
  },
  {
    path:'mantenimiento/menus',
    loadChildren:() => import('./modules/mantenimiento/modules/menus/menus.module').then(m => m.MenusModule)
  },
  {
    path:'mantenimiento/componentes',
    loadChildren:() => import('./modules/mantenimiento/modules/componentes/componentes.module').then(m => m.ComponentesModule)
  },
  {
    path:'mantenimiento/perfiles',
    loadChildren:() => import('./modules/mantenimiento/modules/perfiles/perfiles.module').then(m => m.PerfilesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
