// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  login: '/login',
  //host: 'http://172.22.9.113/SW_SSAT/',
  //host: 'http://172.22.9.108/ssat/',
  //host: 'http://172.22.9.30/DESA_SSAT/',

  //host: 'http://192.168.251.35/QA_WSSSAT/',
   // host: 'http://gispruebas.mtc.gob.pe/DESA_WSSSAT/',
    //host : 'http://192.168.251.16/ssat/',

  //host: 'http://200.60.70.72/DESA_WSSSAT/',
  host : 'http://gispruebas.mtc.gob.pe/DESA_WSSSAT/',

  //host:'http://200.60.70.72/QA_WSSSAT/',

  ItemsPage:10,
  MAX_SIZE_UPLOAD: 20971520
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
